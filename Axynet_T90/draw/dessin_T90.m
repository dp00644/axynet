clear;

name="../REST90.txt";
fd1=fopen(name,"r");
    [tmp,m2] = fscanf(fd1,'%s',1);    [nbl,m2] = fscanf(fd1,'%d',1);
    [tmp,m2] = fscanf(fd1,'%s',1);    [npr,m2] = fscanf(fd1,'%d',1);
    [tmp,m2] = fscanf(fd1,'%s',1);    [nbr,m2] = fscanf(fd1,'%d',1);
    [tmp,m2] = fscanf(fd1,'%s',1);    [m0,m2] = fscanf(fd1,'%f',1);
    [tmp,m2] = fscanf(fd1,'%s',1);    [l0,m2] = fscanf(fd1,'%f',1);
    [tmp,m2] = fscanf(fd1,'%s',1);    [kl,m2] = fscanf(fd1,'%f',1);
    [tmp,m2] = fscanf(fd1,'%s',1);    [km,m2] = fscanf(fd1,'%f',1);
    [tmp,m2] = fscanf(fd1,'%s',1);    [v,m2] = fscanf(fd1,'%f',1);
    [tmp,m2] = fscanf(fd1,'%s',1);    [ro,m2] = fscanf(fd1,'%f',1);
    [tmp,m2] = fscanf(fd1,'%s',1);    [stiff_add,m2] = fscanf(fd1,'%f',1);
    [tmp,m2] = fscanf(fd1,'%s',1);    [seuil,m2] = fscanf(fd1,'%f',1);
    [tmp,m2] = fscanf(fd1,'%s',6);    [x_react,m2] = fscanf(fd1,'%f',1);
    [tmp,m2] = fscanf(fd1,'%s',4);    [drag,m2] = fscanf(fd1,'%f',1);
    [tmp,m2] = fscanf(fd1,'%s',5);    [volume,m2] = fscanf(fd1,'%f',1);
    [tmp,m2] = fscanf(fd1,'%s',5);    [catch_radius,m2] = fscanf(fd1,'%f',1);
    [tmp,m2] = fscanf(fd1,'%s',5);    [catch_thickness,m2] = fscanf(fd1,'%f',1);
    [tmp,m2] = fscanf(fd1,'%s',5);    [catch_length,m2] = fscanf(fd1,'%f',1);
    [tmp,m2] = fscanf(fd1,'%s',5);      
    x = 0*ones(2*nbl+1,1);
    y = 0*ones(2*nbl+1,1);
    z = 0*ones(2*nbl+1,1);
    for zc = 1 : 2*nbl+1
         [x(zc),m2] = fscanf(fd1,'%f',1);
         [y(zc),m2] = fscanf(fd1,'%f',1);
         [z(zc),m2] = fscanf(fd1,'%f',1);
    end
fclose (fd1);

 
  
rayon=sqrt(y.^2+z.^2);
anpha=2*pi/nbr;
npp=2*nbl+1-2*npr;  
  
    
betapair=zeros(1,nbl);
   for k=1:nbl
       betapair(k)=atan(y(2*k)/z(2*k));
   end    
   %angle de points impairs par rapport de plan x0z
   betaimpair=zeros(1,nbl+1);
   for k=1:nbl
       betaimpair(k)=atan(y(2*k-1)/z(2*k-1)) ;   
   end 
   for k=nbl+1
       betaimpair(k)=pi/4;
   end        
   for j=1:nbr  
   %for j=1:1  
       x6=x';
       Y6=zeros(1,2*nbl+1);
       Z6=zeros(1,2*nbl+1);
       %points pairs
       for i=1:nbl
          Y6(2*i)=rayon(2*i)*sin(j*anpha-betapair(i));
          Z6(2*i)=rayon(2*i)*cos(j*anpha-betapair(i));
       end
       %créer des points impairs de la première ligne
         
       for i=0:nbl
          Y6(2*i+1)=rayon(2*i+1)*sin(j*anpha-betaimpair(i+1));
          Z6(2*i+1)=rayon(2*i+1)*cos(j*anpha-betaimpair(i+1));
       end
       %plot3(x6,Y6,Z6);
       plot(x6,Z6,'b');
       hold on; 
       %De la même facon , on peut tracer la deuziemne ligne, il faut faire attention de l'anle des points

       %X 
       x7=x6;
       %Y et Z 
       Y7=zeros(1,2*nbl+1);
       Z7=zeros(1,2*nbl+1);
       for i=1:nbl
            Y7(2*i)=rayon(2*i)*sin((j-1)*anpha+betapair(i));
            Z7(2*i)=rayon(2*i)*cos((j-1)*anpha+betapair(i));
       end
       for i=0:nbl
            Y7(2*i+1)=rayon(2*i+1)*sin((j-1)*anpha+betaimpair(i+1) );
            Z7(2*i+1)=rayon(2*i+1)*cos((j-1)*anpha+betaimpair(i+1));
       end
       %tracer la deuxième ligne  
       %plot3(x7,Y7,Z7) ;
       plot(x7,Z7,'b') ;
    
       %reliant les points pairs de la première aux ceux de même ordre de la 2eme ligne
       for i=1:nbl
         x9=[x7(2*i) x6(2*i)];
         y9=[Y7(2*i) Y6(2*i)];
         z9=[Z7(2*i) Z6(2*i)];
         %plot3(x9,y9,z9);
         plot(x9,z9,'b');
       end
    
       %creer la 3eme ligne de même facon que les precedentes
       %X 
       x8=x';
       %ABSISCE Y et Z //
       Y8=zeros(1,2*nbl+1);
       Z8=zeros(1,2*nbl+1);
       for i=0:nbl
         Y8(2*i+1)=rayon(2*i+1)*sin((j-1)*anpha-betaimpair(i+1));
         Z8(2*i+1)=rayon(2*i+1)*cos((j-1)*anpha-betaimpair(i+1));
       end
       for i=1:nbl
         Y8(2*i)=rayon(2*i)*sin((j-1)*anpha-betapair(i));
         Z8(2*i)=rayon(2*i)*cos((j-1)*anpha-betapair(i));
       end
       %tracer la troixième ligne
       %plot3(x8,Y8,Z8);
       plot(x8,Z8,'b');
 
       %reliant  des points impairs de la 2eme et 3eme ligne

 
       for i=0:nbl
             x10=[x7(2*i+1) x8(2*i+1)];
             y10=[Y7(2*i+1) Y8(2*i+1)];
             z10=[Z7(2*i+1) Z8(2*i+1)];
             %plot3(x10,y10,z10);
             plot(x10,z10,'b');
       end 
 end 

x100= [x(npp)   x(npp) x(npp)  x(npp) x(npp)];
y100= [max(rayon) max(rayon) -max(rayon) -max(rayon) max(rayon) ];
z100= [max(rayon) -max(rayon) -max(rayon) max(rayon) max(rayon) ];
%plot3(x100,y100,z100);
plot(x100,z100,'r');
title(['volume behind the front: ',num2str(volume),' m³']);
%axis([min(x6) max(x6) -max(Z6) max(Z6)], 'equal');
%axis equal;
hold off;
