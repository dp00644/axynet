#include<iostream>
#include<math.h>
#include <fstream>
using namespace std;

ifstream inputFile;


int main()
	{
	//------------------------------------------- DECLARATION DES PARAMETRES PRICIPAUX----------------------------------------------------------------------
	
	int nbl=5;				//nombre de mailles en long
	int nbr=10;				// nombre de mailles autour  
	int npr=3;				// nombre de mailles o� le poisson s'accumule
	double mo=0.05; double lo=0.05;		//longueur initiale (m)
	double kl=200000; double km=200000;	// propri�t� du mat�riau (N)
	double ro=0.2;				// rayon de l'entr�e (m)
	double v=1.5;				//la vitesse de l'eau (m/s)

	double stiff_add = 1000.0;		//additional stiffness (N)
	double seuil=10000000.01;		// convergence limit (N)

	/*
	//
	//
	//                  
	//            \    /\    /
	//             \  /mo\mo/
	//              \/    \/
	//               |     |
	//               |lo   |lo   ->Current
	//               |     |
	//              /\    /\
	//             /  \mo/  \
	//            /    \/    \
	//                     
	//
	*/


	char chr = ' ';
	inputFile.open("A90.dat"); //ios::in

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> nbl;   cout << "nbl:" << nbl << endl;

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> npr;   cout << "npr:" << npr << endl;

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> nbr;   cout << "nbr:" << nbr << endl;

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> mo;   cout << "mo:" << mo << endl;

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> lo;   cout << "lo:" << lo << endl;

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> kl;   cout << "kl:" << kl << endl;

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> km;   cout << "km:" << km << endl;

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> v;   cout << "v:" << v << endl;

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> ro;   cout << "ro:" << ro << endl;
	
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> stiff_add;   cout << "stiff_add:" << stiff_add << endl;

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> seuil;   cout << "seuil:" << seuil << endl;
	
	inputFile.close();
	            
	const double pi=3.141592654;		// Des variales sont d�duits des parametres principales
	double  anpha=2*pi/nbr;			//  angle de chaque maille autour de l'axe X
	int npp=2*nbl+1-2*npr; 			//le num�ro du point � partir duquel le poisson s'accumule 
	double  P= 0.5 *1.4* v*v  *1025;	// La pression du poisson 
	int taille =6*nbl+3;			// taille du vecteur X, F
	int i,j,k;
	double m,l;

	            
// d�claration des tableaux (position, force, matrice de  raideur)
double *X ;   X= new double [taille];// vecteur de position
double *F1 ; F1= new double [taille];// vecteur de la tension tangent
double *F2 ; F2= new double [taille];// vecteur de tension avant
double *F3 ; F3= new double [taille];// vecteur de tension apr�s
double *F4 ; F4= new double [taille];// pression avant
double *F5 ; F5= new double [taille];// pression apres
double *F ; F= new double [taille];// vecteur de force totale
for (i=0;i<taille;i++)
{
F[i]=0;X[i]=0;F1[i]=0;F2[i]=0;F3[i]=0;F4[i]=0;F5[i]=0;
}
double ** dF1dX=new double* [taille];
double ** dF2dX=new double* [taille];
double ** dF3dX=new double* [taille];
double ** dF4dX=new double* [taille];
double ** dF5dX=new double* [taille];
double ** dFdX=new double*  [taille];
for (i=0;i<taille;i++)
{

dF1dX[i] = new double[taille ];dF2dX[i] = new double[taille ];dF3dX[i] = new double[taille ];dF4dX[i] = new double[taille ];dF5dX[i] = new double[taille ];dFdX[i] = new double[taille ];
for ( j=0; j < taille; j++)
{
dF1dX[i][j]=0;dF2dX[i][j]=0;dF3dX[i][j]=0;dF4dX[i][j]=0;dF5dX[i][j]=0;dFdX[i][j]=0;
}


}

double ** a  = new double* [taille];
for ( i=0; i < taille; i++)  
{a[i] = new double[taille ];}
double * b;b=new double[taille];
double ** A  = new double* [taille];
for ( i=0; i < taille; i++)  
{A[i] = new double[taille +1];}
double *h;
h=new double[taille];
// ---------------------------------------------------------------------Position initiale---------------------------------------------------------------------------------  
X[0]=0;
X[1]=lo/2;
X[2]=sqrt(ro*ro-pow((lo/2),2));

//rayon initial
double *rayon ; rayon= new double [taille/3];
double *x ; x= new double [taille/3];
double *Y ; Y= new double [taille/3];
double *Z ; Z= new double [taille/3];
double mean_side;
for (i=0;i<taille/3-1;i++) rayon[i]=ro;
rayon[taille/3-1]=0;
mean_side = (mo+lo)/2;
for (i=3;i<taille-2;i=i+3)	X[i]=i*mean_side/3;// x
for( i=1;i<=nbl;i++)
	{
	X[6*i-2]=rayon[2*i-1]*cos(pi/9);//y
	X[6*i-1]=rayon[2*i-1]*sin(pi/9);//z
	}

for (i=1;i<=(nbl-1);i++)
	{
	X[6*i+1]=rayon[2*i]*cos(pi/18);//y
	X[6*i+2]=rayon[2*i]*sin(pi/18);//x
	}
double residuF=1;double s,w,tmp;

// ------------------------------------------------------------------- proc�dure de calcul---------------------------------------------------------------------------------
residuF = 2*seuil;
while(residuF>seuil)
{      

///////////////MODIFICATION ADDED STIFFNESS/////////////////////////

	inputFile.open("A90.dat"); //ios::in

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> tmp;   //cout << "tmp:" << tmp << endl;

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> tmp;   //cout << "tmp:" << tmp << endl;

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> tmp;   //cout << "tmp:" << tmp << endl;

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> tmp;   

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> tmp;   

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> kl;   

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> km;   

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> v;   

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> tmp;   
	
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> stiff_add;  

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> seuil;  // cout << "mo:" << mo << " lo:" << lo << " kl:" << kl << " km:" << km << " v:" << v << " ro:" << ro << " stiff_add:" << stiff_add << " seuil:" << seuil << endl;
	
	inputFile.close();


////////////////////////////////////////




for (i=0;i<taille;i++) 
{
if (X[i]<0)X[i]=0;

}


for( k=1;k<=taille/3;k++)
{
rayon[k-1]=sqrt(pow(X[3*k-2],2)+pow(X[3*k-1],2));
x[k-1]=X[3*k-3];//X
Y[k-1]=X[3*k-2];//Y
Z[k-1]=X[3*k-1];//Z
}
// les coordon�es des points


//-------------------------------------------------------------------- tension dans les fils tangents-------------------------------------------------------------------------------

for( i=0;i<=nbl;i++)

{l=2 *Y[2*i] ;//cout<<l<<endl;
if ((l)<lo)
{ 
F1[6*i+1] =0;
dF1dX[6*i+1][6*i+1]=0; 
}
else
{ 

F1[6*i+1] = - (fabs(l)-lo)/lo*kl;
dF1dX[6*i+1][6*i+1]=-2/lo*kl;
}
}

for (i=1;i<=nbl ;i++)
{
l=sqrt(pow((sin(anpha)*Z[2*i-1]-cos(anpha)*Y[2*i-1]-Y[2*i-1] ),2)+pow((cos(anpha)*Z[2*i-1]+sin(anpha)*Y[2*i-1]-Z[2*i-1] ),2));

if( l<lo)
{ F1[6*i-2] =0;
F1[6*i-1] =0;
dF1dX[6*i-2][6*i-2]=0;
dF1dX[6*i-2][6*i-1]=0;
dF1dX[6*i-1][6*i-2]=0;
dF1dX[6*i-1][6*i-1]=0;
}
else
{
F1[6*i-2] =( l-lo)/lo*kl/l*(sin(anpha)*Z[2*i-1]-cos(anpha)*Y[2*i-1]-Y[2*i-1] );
F1[6*i-1] =( l-lo)/lo*kl/l*(cos(anpha)*Z[2*i-1]+sin(anpha)*Y[2*i-1]-Z[2*i-1] );

dF1dX[6*i-2][6*i-2]=((-cos(anpha)-1)*kl*(l-lo))/(lo*l)-(0.5*kl*(-cos(anpha)*Y[2*i-1]-Y[2*i-1]+sin(anpha)*Z[2*i-1])*(2*sin(anpha)*(sin(anpha)*Y[2*i-1]+cos(anpha)*Z[2*i-1]-Z[2*i-1])+2*(-cos(anpha)-1)*(-cos(anpha)*Y[2*i-1]-Y[2*i-1]+sin(anpha)*Z[2*i-1]))*(l-lo))/(lo*pow(l,3))+(0.5*kl*(-cos(anpha)*Y[2*i-1]-Y[2*i-1]+sin(anpha)*Z[2*i-1])*(2*sin(anpha)*(sin(anpha)*Y[2*i-1]+cos(anpha)*Z[2*i-1]-Z[2*i-1])+2*(-cos(anpha)-1)*(-cos(anpha)*Y[2*i-1]-Y[2*i-1]+sin(anpha)*Z[2*i-1])))/(lo*pow(l,2));
dF1dX[6*i-2][6*i-1]=(sin(anpha)*kl*(l-lo))/(lo*l)-(0.5*kl*(-cos(anpha)*Y[2*i-1]-Y[2*i-1]+sin(anpha)*Z[2*i-1])*(2*(cos(anpha)-1)*(sin(anpha)*Y[2*i-1]+cos(anpha)*Z[2*i-1]-Z[2*i-1])+2*sin(anpha)*(-cos(anpha)*Y[2*i-1]-Y[2*i-1]+sin(anpha)*Z[2*i-1]))*(l-lo))/(lo*pow(l,3))+(0.5*kl*(-cos(anpha)*Y[2*i-1]-Y[2*i-1]+sin(anpha)*Z[2*i-1])*(2*(cos(anpha)-1)*(sin(anpha)*Y[2*i-1]+cos(anpha)*Z[2*i-1]-Z[2*i-1])+2*sin(anpha)*(-cos(anpha)*Y[2*i-1]-Y[2*i-1]+sin(anpha)*Z[2*i-1])))/(lo*pow(l,2));

dF1dX[6*i-1][6*i-2]=(sin(anpha)*kl*(l-lo))/(lo*l)-(0.5*kl*(sin(anpha)*Y[2*i-1]+cos(anpha)*Z[2*i-1]-Z[2*i-1])*(2*sin(anpha)*(sin(anpha)*Y[2*i-1]+cos(anpha)*Z[2*i-1]-Z[2*i-1])+2*(-cos(anpha)-1)*(-cos(anpha)*Y[2*i-1]-Y[2*i-1]+sin(anpha)*Z[2*i-1]))*(l-lo))/(lo*pow(l,3))+(0.5*kl*(sin(anpha)*Y[2*i-1]+cos(anpha)*Z[2*i-1]-Z[2*i-1])*(2*sin(anpha)*(sin(anpha)*Y[2*i-1]+cos(anpha)*Z[2*i-1]-Z[2*i-1])+2*(-cos(anpha)-1)*(-cos(anpha)*Y[2*i-1]-Y[2*i-1]+sin(anpha)*Z[2*i-1])))/(lo*pow(l,2));
dF1dX[6*i-1][6*i-1] =((cos(anpha)-1)*kl*(l-lo))/(lo*l)-(0.5*kl*(sin(anpha)*Y[2*i-1]+cos(anpha)*Z[2*i-1]-Z[2*i-1])*(2*(cos(anpha)-1)*(sin(anpha)*Y[2*i-1]+cos(anpha)*Z[2*i-1]-Z[2*i-1])+2*sin(anpha)*(-cos(anpha)*Y[2*i-1]-Y[2*i-1]+sin(anpha)*Z[2*i-1]))*(l-lo))/(lo*pow(l,3))+(0.5*kl*(sin(anpha)*Y[2*i-1]+cos(anpha)*Z[2*i-1]-Z[2*i-1])*(2*(cos(anpha)-1)*(sin(anpha)*Y[2*i-1]+cos(anpha)*Z[2*i-1]-Z[2*i-1])+2*sin(anpha)*(-cos(anpha)*Y[2*i-1]-Y[2*i-1]+sin(anpha)*Z[2*i-1])))/(lo*pow(l,2));
}

}




//-------------------------------------------------------------------- tension dans les fils avant-------------------------------------------------------------------------------
for( i=1;i<=(nbl);i++)

{ m=sqrt(pow((x[2*i-1]-x[2*i-2]),2)+pow((Y[2*i-1]-Y[2*i-2]),2)+pow((Z[2*i-1]-Z[2*i-2]),2));
if (m<mo)
{
F2[6*i-3] =0;
F2[6*i-2] =0;
F2[6*i-1] =0;
dF2dX[6*i-3][6*i-6]=0;dF2dX[6*i-3][6*i-5]=0;dF2dX[6*i-3][6*i-4]=0;dF2dX[6*i-3][6*i-3]=0;dF2dX[6*i-3][6*i-2]=0;dF2dX[6*i-3][6*i-1]=0;
dF2dX[6*i-2][6*i-6]=0;dF2dX[6*i-2][6*i-5]=0;dF2dX[6*i-2][6*i-4]=0;dF2dX[6*i-2][6*i-3]=0;dF2dX[6*i-2][6*i-2]=0;dF2dX[6*i-2][6*i-1]=0;
dF2dX[6*i-1][6*i-6]=0;dF2dX[6*i-1][6*i-5]=0;dF2dX[6*i-1][6*i-4]=0;dF2dX[6*i-1][6*i-3]=0;dF2dX[6*i-1][6*i-2]=0;dF2dX[6*i-1][6*i-1]=0;

}
else
{
F2[6*i-3] =- ( m-mo)/mo*km*(x[2*i-1]-x[2*i-2])/m;
F2[6*i-2] =- ( m-mo)/mo*km*(Y[2*i-1]-Y[2*i-2])/m;
F2[6*i-1] =- ( m-mo)/mo*km*(Z[2*i-1]-Z[2*i-2])/m;

dF2dX[6*i-3][6*i-6]=-(-km*(m-mo)/mo/m+km*pow((x[2*i-1]-x[2*i-2]),2)*(m-mo)/mo/pow(m,3)-km*pow(((x[2*i-1]-x[2*i-2])),2)/mo/pow(m,2));                  dF2dX[6*i-3][6*i-5]= -(km*(x[2*i-1]-x[2*i-2])*(Y[2*i-1]-Y[2*i-2])*(m-mo)/mo/pow(m,3)-km*(x[2*i-1]-x[2*i-2])*(Y[2*i-1]-Y[2*i-2])/mo/pow(m,2)) ;     dF2dX[6*i-3][6*i-4]= -  (km*(x[2*i-1]-x[2*i-2])*(Z[2*i-1]-Z[2*i-2])*(m-mo)/mo/pow(m,3)-km*(x[2*i-1]-x[2*i-2])*(Z[2*i-1]-Z[2*i-2])/mo/pow(m,2));         dF2dX[6*i-3][6*i-3]= (-km*(m-mo)/mo/m+km*pow(((x[2*i-1]-x[2*i-2])),2)*(m-mo)/mo/pow(m,3)-km*pow(((x[2*i-1]-x[2*i-2])),2)/mo/pow(m,2)) ;        dF2dX[6*i-3][6*i-2]= (km*(x[2*i-1]-x[2*i-2])*(Y[2*i-1]-Y[2*i-2])*(m-mo)/mo/pow(m,3)-km*(x[2*i-1]-x[2*i-2])*(Y[2*i-1]-Y[2*i-2])/mo/pow(m,2)) ;       dF2dX[6*i-3][6*i-1]=-(  -(km*(x[2*i-1]-x[2*i-2])*(Z[2*i-1]-Z[2*i-2])*(m-mo)/mo/pow(m,3)-km*(x[2*i-1]-x[2*i-2])*(Z[2*i-1]-Z[2*i-2])/mo/pow(m,2)) ); 
dF2dX[6*i-2][6*i-6]=-(-(km*(x[2*i-2]-x[2*i-1])*(Y[2*i-1]-Y[2*i-2])*(m-mo)/mo/pow(m,3)-km*(x[2*i-2]-x[2*i-1])*(Y[2*i-1]-Y[2*i-2])/mo/pow(m,2)));       dF2dX[6*i-2][6*i-5]=-  (-km*(m-mo)/mo/m+km*pow(((Y[2*i-1]-Y[2*i-2])),2)*(m-mo)/mo/pow(m,3)-km*pow(((Y[2*i-1]-Y[2*i-2])),2)/mo/pow(m,2));           dF2dX[6*i-2][6*i-4]=-  (km*((Y[2*i-1]-Y[2*i-2]))*(Z[2*i-1]-Z[2*i-2])*(m-mo)/mo/pow(m,3)-km*((Y[2*i-1]-Y[2*i-2]))*(Z[2*i-1]-Z[2*i-2])/mo/pow(m,2)) ;     dF2dX[6*i-2][6*i-3]=- (km*(x[2*i-2]-x[2*i-1])*(Y[2*i-1]-Y[2*i-2])*(m-mo)/mo/pow(m,3)-km*(x[2*i-2]-x[2*i-1])*(Y[2*i-1]-Y[2*i-2])/mo/pow(m,2)) ;  dF2dX[6*i-2][6*i-2]= (-km*(m-mo)/mo/m+km*pow(((Y[2*i-1]-Y[2*i-2])),2)*(m-mo)/mo/pow(m,3)-km*pow(((Y[2*i-1]-Y[2*i-2])),2)/mo/pow(m,2)) ;             dF2dX[6*i-2][6*i-1]= (km*((Y[2*i-1]-Y[2*i-2]))*(Z[2*i-1]-Z[2*i-2])*(m-mo)/mo/pow(m,3)-km*((Y[2*i-1]-Y[2*i-2]))*(Z[2*i-1]-Z[2*i-2])/mo/pow(m,2));
dF2dX[6*i-1][6*i-6]=(km*(x[2*i-2]-x[2*i-1])*(Z[2*i-1]-Z[2*i-2])*(m-mo)/mo/pow(m,3)-km*(x[2*i-2]-x[2*i-1])*(Z[2*i-1]-Z[2*i-2])/mo/pow(m,2));           dF2dX[6*i-1][6*i-5]=-(km*((Y[2*i-1]-Y[2*i-2]))*(Z[2*i-1]-Z[2*i-2])*(m-mo)/mo/pow(m,3)-km*((Y[2*i-1]-Y[2*i-2]))*(Z[2*i-1]-Z[2*i-2])/mo/pow(m,2));   dF2dX[6*i-1][6*i-4]=-(-km*(m-mo)/mo/m+km*pow(((Z[2*i-1]-Z[2*i-2])),2)*(m-mo)/mo/pow(m,3)-km*pow(((Z[2*i-1]-Z[2*i-2])),2)/mo/pow(m,2));                  dF2dX[6*i-1][6*i-3]=-(km*(x[2*i-2]-x[2*i-1])*(Z[2*i-1]-Z[2*i-2])*(m-mo)/mo/pow(m,3)-km*(x[2*i-2]-x[2*i-1])*(Z[2*i-1]-Z[2*i-2])/mo/pow(m,2));    dF2dX[6*i-1][6*i-2]=(km*((Y[2*i-1]-Y[2*i-2]))*(Z[2*i-1]-Z[2*i-2])*(m-mo)/mo/pow(m,3)-km*((Y[2*i-1]-Y[2*i-2]))*(Z[2*i-1]-Z[2*i-2])/mo/pow(m,2));      dF2dX[6*i-1][6*i-1]=(-km*(m-mo)/mo/m+km*pow(((Z[2*i-1]-Z[2*i-2])),2)*(m-mo)/mo/pow(m,3)-km*pow(((Z[2*i-1]-Z[2*i-2])),2)/mo/pow(m,2)) ;
}
}
for( i=1;i<=(nbl);i++)
{
m=sqrt(pow((x[2*i]-x[2*i-1]),2)+pow((Y[2*i]-Y[2*i-1]),2)+pow((Z[2*i]-Z[2*i-1]),2));
if (m<mo)

{ F2[6*i+0]  =0;
F2[6*i+1]  =0;
F2[6*i+2]  =0;
dF2dX[6*i+0][6*i-3]=0;  dF2dX[6*i+0][6*i-2]=0;  dF2dX[6*i+0][6*i-1]=0;  dF2dX[6*i+0][6*i-0]=0;   dF2dX[6*i+0][6*i+1]=0;    dF2dX[6*i+0][6*i+2]=0;
dF2dX[6*i+1][6*i-3]=0;  dF2dX[6*i+1][6*i-2]=0;  dF2dX[6*i+1][6*i-1]=0;  dF2dX[6*i+1][6*i-0]=0;   dF2dX[6*i+1][6*i+1]=0;    dF2dX[6*i+1][6*i+2]=0;
dF2dX[6*i+2][6*i-3]=0;  dF2dX[6*i+2][6*i-2]=0;  dF2dX[6*i+2][6*i-1]=0;  dF2dX[6*i+2][6*i-0]=0;   dF2dX[6*i+2][6*i+1]=0;    dF2dX[6*i+2][6*i+2]=0;
}

else 
{
F2[6*i+0]  =-( m-mo)/mo*km*(x[2*i]-x[2*i-1])/m;
F2[6*i+1] = -( m-mo)/mo*km*(Y[2*i]-Y[2*i-1])/m;
F2[6*i+2] = -( m-mo)/mo*km*(Z[2*i]-Z[2*i-1])/m;
dF2dX[6*i+0][6*i-3]=-(-km*(m-mo)/mo/m+km*pow((x[2*i]-x[2*i-1]),2)*(m-mo)/mo/pow(m,3)-km*pow((x[2*i]-x[2*i-1]),2)/mo/pow(m,2));         dF2dX[6*i+0][6*i-2]=-(km*(x[2*i]-x[2*i-1])*(Y[2*i]-Y[2*i-1])*(m-mo)/mo/pow(m,3)-km*(x[2*i]-x[2*i-1])*(Y[2*i]-Y[2*i-1])/mo/pow(m,2));      dF2dX[6*i+0][6*i-1]=-(km*(x[2*i]-x[2*i-1])*(Z[2*i]-Z[2*i-1])*(m-mo)/mo/pow(m,3)-km*(x[2*i]-x[2*i-1])*(Z[2*i]-Z[2*i-1])/mo/pow(m,2));      dF2dX[6*i+0][6*i-0]=(-km*(m-mo)/mo/m+km*pow((x[2*i]-x[2*i-1]),2)*(m-mo)/mo/pow(m,3)-km*pow((x[2*i]-x[2*i-1]),2)/mo/pow(m,2));         dF2dX[6*i+0][6*i+1]=(km*(x[2*i]-x[2*i-1])*(Y[2*i]-Y[2*i-1])*(m-mo)/mo/pow(m,3)-km*(x[2*i]-x[2*i-1])*(Y[2*i]-Y[2*i-1])/mo/pow(m,2));        dF2dX[6*i+0][6*i+2]=(km*(x[2*i]-x[2*i-1])*(Z[2*i]-Z[2*i-1])*(m-mo)/mo/pow(m,3)-km*(x[2*i]-x[2*i-1])*(Z[2*i]-Z[2*i-1])/mo/pow(m,2));
dF2dX[6*i+1][6*i-3]=-(km*(x[2*i]-x[2*i-1])*(Y[2*i]-Y[2*i-1])*(m-mo)/mo/pow(m,3)-km*(x[2*i]-x[2*i-1])*(Y[2*i]-Y[2*i-1])/mo/pow(m,2)) ;  dF2dX[6*i+1][6*i-2]=-(-km*(m-mo)/mo/m+km*pow(((Y[2*i]-Y[2*i-1])),2)*(m-mo)/mo/pow(m,3)-km*pow(((Y[2*i]-Y[2*i-1])),2)/mo/pow(m,2));        dF2dX[6*i+1][6*i-1]=-(km*((Y[2*i]-Y[2*i-1]))*(Z[2*i]-Z[2*i-1])*(m-mo)/mo/pow(m,3)-km*((Y[2*i]-Y[2*i-1]))*(Z[2*i]-Z[2*i-1])/mo/pow(m,2));  dF2dX[6*i+1][6*i-0]=(km*(x[2*i]-x[2*i-1])*(Y[2*i]-Y[2*i-1])*(m-mo)/mo/pow(m,3)-km*(x[2*i]-x[2*i-1])*(Y[2*i]-Y[2*i-1])/mo/pow(m,2));   dF2dX[6*i+1][6*i+1]=(-km*(m-mo)/mo/m+km*pow(((Y[2*i]-Y[2*i-1])),2)*(m-mo)/mo/pow(m,3)-km*pow(((Y[2*i]-Y[2*i-1])),2)/mo/pow(m,2));          dF2dX[6*i+1][6*i+2]=(km*((Y[2*i]-Y[2*i-1]))*(Z[2*i]-Z[2*i-1])*(m-mo)/mo/pow(m,3)-km*((Y[2*i]-Y[2*i-1]))*(Z[2*i]-Z[2*i-1])/mo/pow(m,2));
dF2dX[6*i+2][6*i-3]=-(km*(x[2*i]-x[2*i-1])*(Z[2*i]-Z[2*i-1])*(m-mo)/mo/pow(m,3)-km*(x[2*i]-x[2*i-1])*(Z[2*i]-Z[2*i-1])/mo/pow(m,2)) ;  dF2dX[6*i+2][6*i-2]=-(km*((Y[2*i]-Y[2*i-1]))*(Z[2*i]-Z[2*i-1])*(m-mo)/mo/pow(m,3)-km*((Y[2*i]-Y[2*i-1]))*(Z[2*i]-Z[2*i-1])/mo/pow(m,2));  dF2dX[6*i+2][6*i-1]=-(-km*(m-mo)/mo/m+km*pow(((Z[2*i]-Z[2*i-1])),2)*(m-mo)/mo/pow(m,3)-km*pow(((Z[2*i]-Z[2*i-1])),2)/mo/pow(m,2));        dF2dX[6*i+2][6*i-0]=(km*(x[2*i]-x[2*i-1])*(Z[2*i]-Z[2*i-1])*(m-mo)/mo/pow(m,3)-km*(x[2*i]-x[2*i-1])*(Z[2*i]-Z[2*i-1])/mo/pow(m,2));   dF2dX[6*i+2][6*i+1]=(km*((Y[2*i]-Y[2*i-1]))*(Z[2*i]-Z[2*i-1])*(m-mo)/mo/pow(m,3)-km*((Y[2*i]-Y[2*i-1]))*(Z[2*i]-Z[2*i-1])/mo/pow(m,2));    dF2dX[6*i+2][6*i+2]=(-km*(m-mo)/mo/m+km*pow(((Z[2*i]-Z[2*i-1])),2)*(m-mo)/mo/pow(m,3)-km*pow(((Z[2*i]-Z[2*i-1])),2)/mo/pow(m,2));

}
}


//-------------------------------------------------------------------- tension dans les fils apr�s-------------------------------------------------------------------------------


for (i=1;i<=(nbl);i++)
{
m=sqrt(pow((x[2*i]-x[2*i-1]),2)+pow((-Y[2*i-1]+Y[2*i]),2)+pow((-Z[2*i-1]+Z[2*i]),2));

if( m<mo)
{
F3[6*i-3] =0;
F3[6*i-2] =0;
F3[6*i-1] =0;
dF3dX[6*i-3][6*i-3]=0;           dF3dX[6*i-3][6*i-2]=0;              dF3dX[6*i-3][6*i-1]=0;              dF3dX[6*i-3][6*i-0]=0;           dF3dX[6*i-3][6*i+1]=0;           dF3dX[6*i-3][6*i+2]=0;
dF3dX[6*i-2][6*i-3]=0;           dF3dX[6*i-2][6*i-2]=0;              dF3dX[6*i-2][6*i-1]=0;              dF3dX[6*i-2][6*i-0]=0;           dF3dX[6*i-2][6*i+1]=0;           dF3dX[6*i-2][6*i+2]=0;
dF3dX[6*i-1][6*i-3]=0;           dF3dX[6*i-1][6*i-2]=0;              dF3dX[6*i-1][6*i-1]=0;              dF3dX[6*i-1][6*i-0]=0;           dF3dX[6*i-1][6*i+1]=0;           dF3dX[6*i-1][6*i+2]=0;
}
else  
{
F3[6*i-3] =( m-mo)/mo*km*(x[2*i]-x[2*i-1])/m;
F3[6*i-2] =( m-mo)/mo*km*(-Y[2*i-1]+Y[2*i])/m;
F3[6*i-1] =( m-mo)/mo*km*(-Z[2*i-1]+Z[2*i])/m;
dF3dX[6*i-3][6*i-3]=(-km*(m-mo)/mo/m+km*pow((x[2*i]-x[2*i-1]),2)*(m-mo)/mo/pow(m,3)-km*pow((x[2*i]-x[2*i-1]),2)/mo/pow(m,2));                  dF3dX[6*i-3][6*i-2]=(km*(x[2*i]-x[2*i-1])*(Y[2*i]-Y[2*i-1])*(m-mo)/mo/pow(m,3)-km*(x[2*i]-x[2*i-1])*(Y[2*i]-Y[2*i-1])/mo/pow(m,2));                  dF3dX[6*i-3][6*i-1]=(km*(x[2*i]-x[2*i-1])*(Z[2*i]-Z[2*i-1])*(m-mo)/mo/pow(m,3)-km*(x[2*i]-x[2*i-1])*(Z[2*i]-Z[2*i-1])/mo/pow(m,2));                  dF3dX[6*i-3][6*i-0]=-(-km*(m-mo)/mo/m+km*pow((x[2*i]-x[2*i-1]),2)*(m-mo)/mo/pow(m,3)-km*pow((x[2*i]-x[2*i-1]),2)/mo/pow(m,2));                           dF3dX[6*i-3][6*i+1]=-(km*(x[2*i]-x[2*i-1])*(Y[2*i]-Y[2*i-1])*(m-mo)/mo/pow(m,3)-km*(x[2*i]-x[2*i-1])*(Y[2*i]-Y[2*i-1])/mo/pow(m,2));                         dF3dX[6*i-3][6*i+2]=-(km*(x[2*i]-x[2*i-1])*(Z[2*i]-Z[2*i-1])*(m-mo)/mo/pow(m,3)-km*(x[2*i]-x[2*i-1])*(Z[2*i]-Z[2*i-1])/mo/pow(m,2));
dF3dX[6*i-2][6*i-3]=-(km*(x[2*i-1]-x[2*i])*(Y[2*i]-Y[2*i-1])*(m-mo)/mo/pow(m,3)-km*(x[2*i-1]-x[2*i])*(Y[2*i]-Y[2*i-1])/mo/pow(m,2));           dF3dX[6*i-2][6*i-2]=(-km*(m-mo)/mo/m+km*pow(((Y[2*i]-Y[2*i-1])),2)*(m-mo)/mo/pow(m,3)-km*pow(((Y[2*i]-Y[2*i-1])),2)/mo/pow(m,2));                    dF3dX[6*i-2][6*i-1]=(km*((Y[2*i]-Y[2*i-1]))*(Z[2*i]-Z[2*i-1])*(m-mo)/mo/pow(m,3)-km*((Y[2*i]-Y[2*i-1]))*(Z[2*i]-Z[2*i-1])/mo/pow(m,2));              dF3dX[6*i-2][6*i-0]=(km*(x[2*i-1]-x[2*i])*(Y[2*i]-Y[2*i-1])*(m-mo)/mo/pow(m,3)-km*(x[2*i-1]-x[2*i])*(Y[2*i]-Y[2*i-1])/mo/pow(m,2));                      dF3dX[6*i-2][6*i+1]=-(-km*(m-mo)/mo/m+km*pow(((Y[2*i]-Y[2*i-1])),2)*(m-mo)/mo/pow(m,3)-km*pow(((Y[2*i]-Y[2*i-1])),2)/mo/pow(m,2));                           dF3dX[6*i-2][6*i+2]=-(km*((Y[2*i]-Y[2*i-1]))*(Z[2*i]-Z[2*i-1])*(m-mo)/mo/pow(m,3)-km*((Y[2*i]-Y[2*i-1]))*(Z[2*i]-Z[2*i-1])/mo/pow(m,2));
dF3dX[6*i-1][6*i-3]=-(km*(x[2*i-1]-x[2*i])*(Z[2*i]-Z[2*i-1])*(m-mo)/mo/pow(m,3)-km*(x[2*i-1]-x[2*i])*(Z[2*i]-Z[2*i-1])/mo/pow(m,2));           dF3dX[6*i-1][6*i-2]=(km*((Y[2*i]-Y[2*i-1]))*(Z[2*i]-Z[2*i-1])*(m-mo)/mo/pow(m,3)-km*((Y[2*i]-Y[2*i-1]))*(Z[2*i]-Z[2*i-1])/mo/pow(m,2));              dF3dX[6*i-1][6*i-1]=(-km*(m-mo)/mo/m+km*pow(((Z[2*i]-Z[2*i-1])),2)*(m-mo)/mo/pow(m,3)-km*pow(((Z[2*i]-Z[2*i-1])),2)/mo/pow(m,2));                              dF3dX[6*i-1][6*i-0]=(km*(x[2*i-1]-x[2*i])*(Z[2*i]-Z[2*i-1])*(m-mo)/mo/pow(m,3)-km*(x[2*i-1]-x[2*i])*(Z[2*i]-Z[2*i-1])/mo/pow(m,2)) ;           dF3dX[6*i-1][6*i+1]=-(km*((Y[2*i]-Y[2*i-1]))*(Z[2*i]-Z[2*i-1])*(m-mo)/mo/pow(m,3)-km*((Y[2*i]-Y[2*i-1]))*(Z[2*i]-Z[2*i-1])/mo/pow(m,2));           dF3dX[6*i-1][6*i+2]=-(-km*(m-mo)/mo/m+km*pow(((Z[2*i]-Z[2*i-1])),2)*(m-mo)/mo/pow(m,3)-km*pow(((Z[2*i]-Z[2*i-1])),2)/mo/pow(m,2));

}
}
for( i=0;i<=(nbl-1);i++)
{
m=sqrt(pow(x[2*i+1]-x[2*i],2)+pow((Y[2*i+1]-Y[2*i]),2)+pow((Z[2*i+1]-Z[2*i]),2));
if ( m<mo)
{
F3[6*i+0] =0;
F3[6*i+1] =0;
F3[6*i+2] =0;
dF3dX[6*i+0][6*i+0]=0;   dF3dX[6*i+0][6*i+1]=0;    dF3dX[6*i+0][6*i+2]=0;    dF3dX[6*i+0][6*i+3]=0;     dF3dX[6*i+0][6*i+4]=0;      dF3dX[6*i+0][6*i+5]=0;
dF3dX[6*i+1][6*i+0]=0;   dF3dX[6*i+1][6*i+1]=0;    dF3dX[6*i+1][6*i+2]=0;    dF3dX[6*i+1][6*i+3]=0;     dF3dX[6*i+1][6*i+4]=0;      dF3dX[6*i+1][6*i+5]=0;
dF3dX[6*i+2][6*i+0]=0;   dF3dX[6*i+2][6*i+1]=0;    dF3dX[6*i+2][6*i+2]=0;    dF3dX[6*i+2][6*i+3]=0;     dF3dX[6*i+2][6*i+4]=0;      dF3dX[6*i+2][6*i+5]=0;
}
else
{
F3[6*i+0] = ( m-mo)/mo*km*(x[2*i+1]-x[2*i])/m;
F3[6*i+1] = ( m-mo)/mo*km*(Y[2*i+1]-Y[2*i])/m;
F3[6*i+2] = ( m-mo)/mo*km*(Z[2*i+1]-Z[2*i])/m;
dF3dX[6*i+0][6*i+0]=(-km*(m-mo)/mo/m+km*pow(x[2*i+1]-x[2*i],2)*(m-mo)/mo/pow(m,3)-km*(pow(x[2*i+1]-x[2*i],2)/mo/pow(m,2)));           dF3dX[6*i+0][6*i+1]=(km*(x[2*i+1]-x[2*i])*(Y[2*i+1]-Y[2*i])*(m-mo)/mo/pow(m,3)-km*(x[2*i+1]-x[2*i])*(Y[2*i+1]-Y[2*i])/mo/pow(m,2));        dF3dX[6*i+0][6*i+2]=(km*(x[2*i+1]-x[2*i])*(Z[2*i+1]-Z[2*i])*(m-mo)/mo/pow(m,3)-km*(x[2*i+1]-x[2*i])*(Z[2*i+1]-Z[2*i])/mo/pow(m,2)) ;         dF3dX[6*i+0][6*i+3]=-(-km*(m-mo)/mo/m+km*pow(x[2*i+1]-x[2*i],2)*(m-mo)/mo/pow(m,3)-km*pow(x[2*i+1]-x[2*i],2)/mo/pow(m,2));               dF3dX[6*i+0][6*i+4]=-(km*(x[2*i+1]-x[2*i])*(Y[2*i+1]-Y[2*i])*(m-mo)/mo/pow(m,3)-km*(x[2*i+1]-x[2*i])*(Y[2*i+1]-Y[2*i])/mo/pow(m,2)) ;          dF3dX[6*i+0][6*i+5]= -(km*(x[2*i+1]-x[2*i])*(Z[2*i+1]-Z[2*i])*(m-mo)/mo/pow(m,3)-km*(x[2*i+1]-x[2*i])*(Z[2*i+1]-Z[2*i])/mo/pow(m,2));
dF3dX[6*i+1][6*i+0]=(km*(x[2*i+1]-x[2*i])*(Y[2*i+1]-Y[2*i])*(m-mo)/mo/pow(m,3)-km*(x[2*i+1]-x[2*i])*(Y[2*i+1]-Y[2*i])/mo/pow(m,2));   dF3dX[6*i+1][6*i+1]=(-km*(m-mo)/mo/m+km*pow(((Y[2*i+1]-Y[2*i])),2)*(m-mo)/mo/pow(m,3)-km*pow(((Y[2*i+1]-Y[2*i])),2)/mo/pow(m,2));          dF3dX[6*i+1][6*i+2]=(km*((Y[2*i+1]-Y[2*i]))*(Z[2*i+1]-Z[2*i])*(m-mo)/mo/pow(m,3)-km*((Y[2*i+1]-Y[2*i]))*(Z[2*i+1]-Z[2*i])/mo/pow(m,2));      dF3dX[6*i+1][6*i+3]=-(km*(x[2*i+1]-x[2*i])*(Y[2*i+1]-Y[2*i])*(m-mo)/mo/pow(m,3)-km*(x[2*i+1]-x[2*i])*(Y[2*i+1]-Y[2*i])/mo/pow(m,2));     dF3dX[6*i+1][6*i+4]=-(-km*(m-mo)/mo/m+km*pow(((Y[2*i+1]-Y[2*i])),2)*(m-mo)/mo/pow(m,3)-km*pow(((Y[2*i+1]-Y[2*i])),2)/mo/pow(m,2)) ;            dF3dX[6*i+1][6*i+5]= -(km*((Y[2*i+1]-Y[2*i]))*(Z[2*i+1]-Z[2*i])*(m-mo)/mo/pow(m,3)-km*((Y[2*i+1]-Y[2*i]))*(Z[2*i+1]-Z[2*i])/mo/pow(m,2));
dF3dX[6*i+2][6*i+0]=(km*(x[2*i+1]-x[2*i])*(Z[2*i+1]-Z[2*i])*(m-mo)/mo/pow(m,3)-km*(x[2*i+1]-x[2*i])*(Z[2*i+1]-Z[2*i])/mo/pow(m,2));   dF3dX[6*i+2][6*i+1]=(km*((Y[2*i+1]-Y[2*i]))*(Z[2*i+1]-Z[2*i])*(m-mo)/mo/pow(m,3)-km*((Y[2*i+1]-Y[2*i]))*(Z[2*i+1]-Z[2*i])/mo/pow(m,2));    dF3dX[6*i+2][6*i+2]=(-km*(m-mo)/mo/m+km*pow(((Z[2*i+1]-Z[2*i])),2)*(m-mo)/mo/pow(m,3)-km*pow(((Z[2*i+1]-Z[2*i])),2)/mo/pow(m,2));                      dF3dX[6*i+2][6*i+3]=-(km*(x[2*i+1]-x[2*i])*(Z[2*i+1]-Z[2*i])*(m-mo)/mo/pow(m,3)-km*(x[2*i+1]-x[2*i])*(Z[2*i+1]-Z[2*i])/mo/pow(m,2));     dF3dX[6*i+2][6*i+4]=-(km*((Y[2*i+1]-Y[2*i]))*(Z[2*i+1]-Z[2*i])*(m-mo)/mo/pow(m,3)-km*((Y[2*i+1]-Y[2*i]))*(Z[2*i+1]-Z[2*i])/mo/pow(m,2));       dF3dX[6*i+2][6*i+5]=-(-km*(m-mo)/mo/m+km*pow(((Z[2*i+1]-Z[2*i])),2)*(m-mo)/mo/pow(m,3)-km*pow(((Z[2*i+1]-Z[2*i])),2)/mo/pow(m,2));
}
}




// ------------------------------------------------------------------------------------------Pression avant-------------------------------------------------------------------------------------------------

for (i=(npp+1);i<=(2*nbl) ;i++) 
{                            //la pression du poisson avant � partir du point  P npp+1
F4[3*i-3] =  pi*(pow((rayon[i-2]),2)- pow((rayon[i-1]),2))*0.25*P/nbr;
F4[3*i-2] = pi*(rayon[i-2]+rayon[i-1])*0.25*P/nbr*Y[i-1]/rayon[i-1]*(x[i-1]-x[i-2]);
F4[3*i-1] = pi*(rayon[i-2]+ rayon[i-1])*0.25*P/nbr*Z[i-1]/rayon[i-1]*(x[i-1]-x[i-2]);
dF4dX[3*i-3][3*i-6]=0;                                                              dF4dX[3*i-3][3*i-5]=pi*0.25*P/nbr*2*Y[i-2];                                                 dF4dX[3*i-3][3*i-4]=pi*0.25*P/nbr*2*Z[i-2];                                                 dF4dX[3*i-3][3*i-3]=0;                                                                                                 dF4dX[3*i-3][3*i-2]=pi*0.25*P/nbr*(-2*Y[i-1]);                                                                                                                                                                                       dF4dX[3*i-3][3*i-1]=pi*0.25*P/nbr*(-2*Z[i-1]);
dF4dX[3*i-2][3*i-6]=pi*0.25*P/nbr*(-Y[i-1]*(rayon[i-2]+rayon[i-1])/rayon[i-1]);     dF4dX[3*i-2][3*i-5]=pi*0.25*P/nbr*((x[i-1]-x[i-2])*Y[i-1]*Y[i-2]/rayon[i-1]/rayon[i-2]);    dF4dX[3*i-2][3*i-4]=pi*0.25*P/nbr*((x[i-1]-x[i-2])*Y[i-1]*Z[i-2]/rayon[i-1]/rayon[i-2]);    dF4dX[3*i-2][3*i-3]=pi*0.25*P/nbr*(Y[i-1]*(rayon[i-2]+rayon[i-1])/rayon[i-1]);                                         dF4dX[3*i-2][3*i-2]=pi*0.25*P/nbr*((x[i-1]-x[i-2])*(rayon[i-2]+rayon[i-1])/rayon[i-1]-(x[i-1]-x[i-2])*pow(Y[i-1],2)*(rayon[i-2]+rayon[i-1])/pow(rayon[i-1],3)+(x[i-1]-x[i-2])*pow(Y[i-1],2)/pow(rayon[i-1],2));                      dF4dX[3*i-2][3*i-1]=pi*0.25*P/nbr*((x[i-1]-x[i-2])*Y[i-1]*Z[i-1]/pow(rayon[i-1],2)-(x[i-1]-x[i-2])*Y[i-1]*Z[i-1]*(rayon[i-2]+rayon[i-1])/pow(rayon[i-1],3));
dF4dX[3*i-1][3*i-6]=pi*0.25*P/nbr*(-Z[i-1]*(rayon[i-2]+rayon[i-1])/rayon[i-1]);     dF4dX[3*i-1][3*i-5]=pi*0.25*P/nbr*((x[i-1]-x[i-2])*Z[i-1]*Y[i-2]/rayon[i-1]/rayon[i-2]);    dF4dX[3*i-1][3*i-4]=pi*0.25*P/nbr*((x[i-1]-x[i-2])*Z[i-1]*Z[i-2]/rayon[i-1]/rayon[i-2]);    dF4dX[3*i-1][3*i-3]=pi*0.25*P/nbr*(Z[i-1]*(rayon[i-2]+rayon[i-1])/rayon[i-1]);                                         dF4dX[3*i-1][3*i-2]=pi*0.25*P/nbr*((x[i-1]-x[i-2])*Y[i-1]*Z[i-1]/pow(rayon[i-1],2)-(x[i-1]-x[i-2])*Y[i-1]*Z[i-1]*(rayon[i-2]+rayon[i-1])/pow(rayon[i-1],3));                                                                         dF4dX[3*i-1][3*i-1]=pi*0.25*P/nbr*((x[i-1]-x[i-2])*(rayon[i-2]+rayon[i-1])/rayon[i-1]-(x[i-1]-x[i-2])*pow(Z[i-1],2)*(rayon[i-2]+rayon[i-1])/pow(rayon[i-1],3)+(x[i-1]-x[i-2])*pow(Z[i-1],2)/pow(rayon[i-1],2));

}



i=(2*nbl+1) ;   //le sommet de poche de chalut
F4[3*i-3]  = - pi*(pow((rayon[i-1]),2)- pow((rayon[i-2]),2))*0.25*P/nbr;
F4[3*i-2] =    pi*(rayon[i-2]+rayon[i-1])*0.25*P/nbr*(x[i-1]-x[i-2])/ pow(2,0.5);
F4[3*i-1] =    pi*(rayon[i-2]+ rayon[i-1])*0.25*P/nbr*(x[i-1]-x[i-2])/pow(2,0.5);
dF4dX[3*i-3][3*i-6]=0;                                                              dF4dX[3*i-3][3*i-5]=pi*0.25*P/nbr*2*Y[i-2];                                                 dF4dX[3*i-3][3*i-4]=pi*0.25*P/nbr*2*Z[i-2];                                                 dF4dX[3*i-3][3*i-3]=0;                                                                                                 dF4dX[3*i-3][3*i-2]=pi*0.25*P/nbr*(-2*Y[i-1]);                                                                                                                                                                                       dF4dX[3*i-3][3*i-1]=pi*0.25*P/nbr*(-2*Z[i-1]);
dF4dX[3*i-2][3*i-6]=1/pow(2,0.5)*pi*0.25*P/nbr*(-(rayon[i-2]+rayon[i-1]));          dF4dX[3*i-2][3*i-5]=1/pow(2,0.5)*pi*0.25*P/nbr*((x[i-1]-x[i-2])*Y[i-2]/rayon[i-2]);         dF4dX[3*i-2][3*i-4]=1/pow(2,0.5)*pi*0.25*P/nbr*((x[i-1]-x[i-2])*Z[i-2]/rayon[i-2]);         dF4dX[3*i-2][3*i-3]=1/pow(2,0.5)*pi*0.25*P/nbr*(rayon[i-1]+rayon[i-2]);                                                dF4dX[3*i-2][3*i-2]=0;                                                                                                                                                                                                               dF4dX[3*i-2][3*i-1]=0;
dF4dX[3*i-1][3*i-6]=1/pow(2,0.5)*pi*0.25*P/nbr*(-(rayon[i-2]+rayon[i-1]) );         dF4dX[3*i-1][3*i-5]=1/pow(2,0.5)*pi*0.25*P/nbr*((x[i-1]-x[i-2])*Y[i-2]/rayon[i-2]);         dF4dX[3*i-1][3*i-4]=1/pow(2,0.5)*pi*0.25*P/nbr*((x[i-1]-x[i-2])*Z[i-2]/rayon[i-2]);         dF4dX[3*i-1][3*i-3]=1/pow(2,0.5)*pi*0.25*P/nbr*(rayon[i-1]+rayon[i-2]);                                                dF4dX[3*i-1][3*i-2]=0;                                                                                                                                                                                                               dF4dX[3*i-1][3*i-1]=0;



// ------------------------------------------------------------------------------------------Pression apres-------------------------------------------------------------------------------------------------

for (i=npp;i<=(2*nbl-1);i++)
{

F5[3*i-3] = pi*(pow((rayon[i-1]),2)- pow((rayon[i]),2))*0.25*P/nbr;
F5[3*i-2] = pi*(rayon[i-1]+rayon[i])*0.25*P/nbr*Y[i-1]/rayon[i-1]*(x[i]-x[i-1]);
F5[3*i-1] = pi*(rayon[i-1]+ rayon[i])*0.25*P/nbr*Z[i-1]/rayon[i-1]* (x[i]-x[i-1]);
dF5dX[3*i-3][3*i-3]=pi*0.25*P/nbr*(0);                                           dF5dX[3*i-3][3*i-2]=pi*0.25*P/nbr*(2*Y[i-1]);                                                                                                                                                                  dF5dX[3*i-3][3*i-1]=pi*0.25*P/nbr*(2*Z[i-1]);                                                                                                                                                                 dF5dX[3*i-3][3*i-0]=pi*0.25*P/nbr*(0);                                           dF5dX[3*i-3][3*i+1]=pi*0.25*P/nbr*(-2*Y[i]);                                          dF5dX[3*i-3][3*i+2]= pi*0.25*P/nbr* (-2*Z[i]);
dF5dX[3*i-2][3*i-3]=pi*0.25*P/nbr*(-Y[i-1]*(rayon[i-1]+rayon[i])/rayon[i-1]);    dF5dX[3*i-2][3*i-2]=pi*0.25*P/nbr*((x[i]-x[i-1])*(rayon[i]+rayon[i-1])/rayon[i-1]-(x[i]-x[i-1])*pow(Y[i-1],2)*(rayon[i]+rayon[i-1])/pow((rayon[i-1]),3)+(x[i]-x[i-1])*pow(Y[i-1],2)/pow((rayon[i-1]),2));      dF5dX[3*i-2][3*i-1]=pi*0.25*P/nbr*((x[i]-x[i-1])*Y[i-1]*Z[i-1]/pow((rayon[i-1]),2)-(x[i]-x[i-1])*Y[i-1]*Z[i-1]*(rayon[i]+rayon[i-1])/pow((rayon[i-1]),3));                                                    dF5dX[3*i-2][3*i-0]=pi*0.25*P/nbr*(Y[i-1]*(rayon[i-1]+rayon[i])/rayon[i-1]);     dF5dX[3*i-2][3*i+1]=pi*0.25*P/nbr*((x[i]-x[i-1])*Y[i]*Y[i-1]/rayon[i]/rayon[i-1]);    dF5dX[3*i-2][3*i+2]= pi*0.25*P/nbr* ((x[i]-x[i-1])*Y[i-1]*Z[i]/rayon[i]/rayon[i-1]);
dF5dX[3*i-1][3*i-3]=pi*0.25*P/nbr*(-Z[i-1]*(rayon[i-1]+rayon[i])/rayon[i-1]);    dF5dX[3*i-1][3*i-2]=pi*0.25*P/nbr*((x[i]-x[i-1])*Y[i-1]*Z[i-1]/pow((rayon[i-1]),2)-(x[i]-x[i-1])*Y[i-1]*Z[i-1]*(rayon[i]+rayon[i-1])/pow((rayon[i-1]),3));                                                     dF5dX[3*i-1][3*i-1]=pi*0.25*P/nbr*((x[i]-x[i-1])*(rayon[i]+rayon[i-1])/rayon[i-1]-(x[i]-x[i-1])*pow(Z[i-1],2)*(rayon[i]+rayon[i-1])/pow((rayon[i-1]),3)+(x[i]-x[i-1])*pow(Z[i-1],2)/pow((rayon[i-1]),2));     dF5dX[3*i-1][3*i-0]=pi*0.25*P/nbr*(Z[i-1]*(rayon[i-1]+rayon[i])/rayon[i-1]);     dF5dX[3*i-1][3*i+1]=pi*0.25*P/nbr*((x[i]-x[i-1])*Y[i]*Z[i-1]/rayon[i]/rayon[i-1]);    dF5dX[3*i-1][3*i+2]= pi*0.25*P/nbr* ( (x[i]-x[i-1])*Z[i-1]*Z[i]/rayon[i]/rayon[i-1]);

}


i=(2*nbl);
F5[3*i-3] = pi*(pow((rayon[i-1]),2)- pow((rayon[i]),2))*0.25*P/nbr;
F5[3*i-2] = pi*(rayon[i-1]+rayon[i])*0.25*P/nbr*Y[i-1]/rayon[i-1]*(x[i]-x[i-1]);
F5[3*i-1] = pi*(rayon[i-1]+ rayon[i])*0.25*P/nbr*Z[i-1]/rayon[i-1]*(x[i]-x[i-1]);

dF5dX[3*i-3][3*i-3]=pi*0.25*P/nbr*(0);                                           dF5dX[3*i-3][3*i-2]=pi*0.25*P/nbr*(2*Y[i-1]);                                                                                                                                                              dF5dX[3*i-3][3*i-1]=pi*0.25*P/nbr*(2*Z[i-1]);                                                                                                                                                                 dF5dX[3*i-3][3*i-0]=pi*0.25*P/nbr*(0);                                            dF5dX[3*i-3][3*i+1]=pi*0.25*P/nbr*(-2*Y[i]);                                                           dF5dX[3*i-3][3*i+2]= pi*0.25*P/nbr* (-2*Z[i]);
dF5dX[3*i-2][3*i-3]=pi*0.25*P/nbr*(-Y[i-1]*(rayon[i-1]+rayon[i])/rayon[i-1]);    dF5dX[3*i-2][3*i-2]=pi*0.25*P/nbr*((x[i]-x[i-1])*(rayon[i]+rayon[i-1])/rayon[i-1]-(x[i]-x[i-1])*pow(Y[i-1],2)*(rayon[i]+rayon[i-1])/pow(rayon[i-1],3)+(x[i]-x[i-1])*pow(Y[i-1],2)/pow(rayon[i-1],2));      dF5dX[3*i-2][3*i-1]=pi*0.25*P/nbr*((x[i]-x[i-1])*Y[i-1]*Z[i-1]/pow(rayon[i-1],2)-(x[i]-x[i-1])*Y[i-1]*Z[i-1]*(rayon[i]+rayon[i-1])/pow(rayon[i-1],3) );                                                       dF5dX[3*i-2][3*i-0]=pi*0.25*P/nbr*(Y[i-1]*(rayon[i-1]+rayon[i])/rayon[i-1]);      dF5dX[3*i-2][3*i+1]=pi*0.25*P/nbr*((x[i]-x[i-1])*Y[i-1]/rayon[i-1]);                                   dF5dX[3*i-2][3*i+2]=  pi*0.25*P/nbr*((x[i]-x[i-1])*Y[i-1]/rayon[i-1]);
dF5dX[3*i-1][3*i-3]=pi*0.25*P/nbr*(-Z[i-1]*(rayon[i-1]+rayon[i])/rayon[i-1]);    dF5dX[3*i-1][3*i-2]=pi*0.25*P/nbr*((x[i]-x[i-1])*Y[i-1]*Z[i-1]/pow(rayon[i-1],2)-(x[i]-x[i-1])*Y[i-1]*Z[i-1]*(rayon[i]+rayon[i-1])/pow(rayon[i-1],3));                                                     dF5dX[3*i-1][3*i-1]=pi*0.25*P/nbr*((x[i]-x[i-1])*(rayon[i]+rayon[i-1])/rayon[i-1]-(x[i]-x[i-1])*pow(Z[i-1],2)*(rayon[i]+rayon[i-1])/pow(rayon[i-1],3)+(x[i]-x[i-1])*pow(Z[i-1],2)/pow(rayon[i-1],2));         dF5dX[3*i-1][3*i-0]=pi*0.25*P/nbr*( Z[i-1]*(rayon[i-1]+rayon[i])/rayon[i-1]);     dF5dX[3*i-1][3*i+1]=pi*0.25*P/nbr*( (x[i]-x[i-1])*Z[i-1]/rayon[i-1]);                                  dF5dX[3*i-1][3*i+2]=  pi*0.25*P/nbr* ((x[i]-x[i-1])*Z[i-1]/rayon[i-1]);

//VECTEUR DE FORCE
// cout<<"vecteur F" <<endl;
for(i=0;i<taille;i++)F[i]=F1[i]+F2[i]+F3[i]+F4[i]+F5[i];//cout<<F2[i]<<endl;}

//MATRICE DE RAIDEUR

for(i=0;i<taille;i++)
for(j=0;j<taille;j++)dFdX[i][j]=dF1dX[i][j]+dF2dX[i][j]+dF3dX[i][j]+dF4dX[i][j]+dF5dX[i][j];



//-------------------------------------------------------------------------METHODE DE NEWTON-RAPHSON---------------------------------------------

for ( i=3; i < taille-2; i++)

for ( j=3; j < taille-2; j++)

{a[i][j]=-dFdX[i][j];}

// resolution d'un systeme d'equations % -dFdX.h=F  %

for ( i=3; i < taille-2; i++) b[i]=F[i];

for ( i=3; i < taille-2; i++)
{
for ( j=3; j < taille-2; j++)
{
A[i][j]=a[i][j];//cout<<A[i][j];
}
A[i][taille-2]=b[i];
}

for (i=3;i<taille-2;i++) A[i][i]=A[i][i]+ stiff_add;

//% �chonl� 


for (i=3;i<taille-3;i++)
	for (j=i+1;j<taille-2;j++)
		{ 
		w=A[j][i]/A[i][i] ;
		for ( k=i; k<taille-1;k++)	A[j][k]= A[j][k]-w*A[i][k];
		}



for (i=taille-3;i>=3; i--)  
{ 
s=0;
for (j=i+1; j<taille-2;j++)
s=s+A[i][j]*h[j];
h[i]=(A[i][taille-2]-s)/A[i][i];
}

// nouvelle position saucisse
for (i=3; i<taille-2; i++) X[i]=X[i]+h[i];

// residuF

residuF=0;
for (i=3; i<taille-2; i++) residuF= residuF+(pow(F[i],2));
residuF=sqrt(residuF)/(taille-5);
//cout<<residuF<<endl; 
cout << " kl:" << kl << " km:" << km << " v:" << v << " stiff_add:" << stiff_add << " seuil:" << seuil  << " residu:" << residuF << endl;
	

}



//VECTEUR DE X FINAL
cout<<"Position x y z (m):" <<endl;
//for (i=0; i<taille; i++)cout<<X[i]<<endl;
for (i=1; i<=taille/3; i++)cout<<X[3*i-3]<<"  " <<X[3*i-2]<<"  " <<X[3*i-1]<<endl;
cout<<"X reaction at the entry (N):"<<2*nbr*F[0] <<endl;
double drag;
drag = pi*rayon[npp-1]*rayon[npp-1]*P;
cout<<"Drag on catch (N): "<<drag <<endl;
cout<<"Radius at catch limit (m): "<<rayon[npp-1] <<endl;
cout<<"Thickness of the catch (m): "<<X[taille-3]-X[3*npp-3] <<endl;
//cout<<"position of the limit of the catch (m): "<<X[3*npp-3] <<endl;
cout<<"Length of the codend (m): "<<X[taille-3] <<endl;


    double Volume=0;
    for (j=npp;j< 2*nbl+1; j++) 
	Volume=Volume+(pow(X[3*j+1],2)+pow(X[3*j+2],2)+pow((pow(X[3*j+1],2)+pow(X[3*j+2],2))*(pow(X[3*j-2],2)+pow(X[3*j-1],2)),0.5)+pow(X[3*j-2],2)+pow(X[3*j-1],2))*(X[3*j]-X[3*j-3])*pi*1/3 ;
		cout<<"volume behind the front (m^3):  "<<Volume <<endl;
    double surface=0;
	for (i=npp;i<= taille/3-1; i++){ surface=surface+pi*(pow(pow(X[3*i-2],2)+pow(X[3*i-1],2),0.5)+pow(pow(X[3*i+1],2)+pow(X[3*i+2],2),0.5))*pow(pow(pow(pow(X[3*i+1],2)+pow(X[3*i+2],2),0.5)-pow(pow(X[3*i-2],2)+pow(X[3*i-1],2),0.5),2)+pow(X[3*i]-X[3*i-3],2),0.5) ; }
		// i: num�ro de point.
  
    cout<<"revolution surface (m^2):  "<<surface<<endl;
	


ofstream f ("REST90.txt");
if (!f.is_open())
	cout << "Impossible to open the file in writing" << endl;
else
	{
	f << "nbl:\t\t\t\t" << nbl << endl;
	f << "npr:\t\t\t\t" << npr << endl;
	f << "nbr:\t\t\t\t" << nbr << endl;
	f << "m0:\t\t\t\t" << mo << endl;
	f << "l0:\t\t\t\t" << lo << endl;
	f << "kl:\t\t\t\t" << kl << endl;
	f << "km:\t\t\t\t" << km << endl;
	f << "v:\t\t\t\t" << v << endl;
	f << "ro:\t\t\t\t" << ro << endl;
	f << "stiff_add:\t\t\t" << stiff_add << endl;
	f << "seuil:\t\t\t\t" << seuil << endl;
	f << " " <<endl;
	f<<"X reaction at the entry (N):\t"<<2*nbr*F[0] <<endl;
	f<<"Drag on catch (N): \t\t"<<drag <<endl;
	f<<"volume behind the front (m^3): \t\t"<<Volume <<endl;
	f<<"Radius at catch limit (m): \t"<<rayon[npp-1] <<endl;
	f<<"Thickness of the catch (m): \t"<<X[taille-3]-X[3*npp-3] <<endl;
	f<<"Length of the codend (m): \t"<<X[taille-3] <<endl;
	f << "Position x y z (m):\t" <<endl;
	for (i=1; i<=taille/3; i++) 	f<<X[3*i-3]<<"\t" <<X[3*i-2]<<"\t" <<X[3*i-1]<<endl;
	}
f.close();


delete a;
delete b;
delete A;
delete h;
delete X;
delete F1;
delete F2;
delete F3;
delete F4;
delete F5;
delete dF1dX;
delete dF2dX;
delete dF3dX;
delete dF4dX;
delete dF5dX;
delete dFdX;
delete rayon;
//getchar();
return 0;


}


