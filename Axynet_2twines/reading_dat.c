#define PRINCIPAL 0
/*reading_dat.c
it reads data in file *.dat*/
#include "AXYnet.h"

/* reading of file .dat */

int reading_dat(char *nomfichier)
	{
  	char c,tonom[100];
    	int tmpi;
  	FILE *f1;

  	strcpy(tonom,nomfichier);

  	strcat(tonom,".dat");

  	printf(" \n");
  	printf("%s %s \n","File ",tonom);

  	f1 = fopen(tonom,"r");
  	if (f1 == NULL) 
  		{
    		printf(" \n");
    		printf("%s %s %s \n","file ",tonom," don't exist");
    		//system("PAUSE");
            	exit(0);
  		} 
  
  	/* type of cod-end ***********************************************************/
  	do 
		{
  		c=fgetc(f1);
		/*printf("%c",c);*/
		}
	while (c !=':'); 
  	fscanf(f1,"%d\n",&Codend.type);
  	tmpi = Codend.type;
  	printf("type    = %10d\t ",tmpi);
  	if (Codend.type == 1)          printf("Full diamond mesh cod-end\n");
  	if (Codend.type == 2)          printf("Full square mesh cod-end\n");
  	if (Codend.type == 3)          printf("Full rectangle mesh cod-end\n");
  	if (Codend.type == 4)          printf("Full kite mesh cod-end\n");
  	if (Codend.type == 5)          printf("Full hexa1 mesh cod-end\n");
  	if (Codend.type == 6)          printf("Full hexa2 mesh cod-end\n");
  	
  	reading_case_2(f1);

  	fclose(f1);
  	return 0;
	}	


int reading_case_2(FILE *f1)
	{
  	char c;
    	int tmpi;
    	double tmpd;
    	
  	/* radius of the entry ***********************************************************/
	/*Codend.radius = 0.25;*/
	
  	/* nb of mesh around ***********************************************************/
  	do 		c=fgetc(f1);	while (c !=':'); 
  	fscanf(f1,"%d\n",&Codend.nbr);
  	tmpi = Codend.nbr;
  	printf("nbr 	= %10d \t meshes (bars) around for diamond (square)\n",tmpi);

  	/* nb of mesh along ***********************************************************/
  	do 		c=fgetc(f1);	while (c !=':'); 
  	fscanf(f1,"%d\n",&Codend.nbl);
  	tmpi = Codend.nbl;
  	if (Codend.type == 2) Codend.nbl = (int) tmpi/2;
  	if (Codend.type == 3) Codend.nbl = (int) tmpi/2;
  	printf("nbl 	= %10d \t meshes (bars) along for diamond (square)\n",tmpi);

  	if (Codend.type < 3)
      {
  	  /* full mesh size ***********************************************************/
  	  do 		c=fgetc(f1);	while (c !=':'); 
  	  fscanf(f1,"%lf\n",&Codend.mf);
  	  tmpd = Codend.mf;
  	  printf("mf 	= %10.3lf \t full mesh size (m)\n",tmpd);
     }

  	if (Codend.type == 3)
      {
  	  /* full mesh size ***********************************************************/
  	  do 		c=fgetc(f1);	while (c !=':'); 
  	  fscanf(f1,"%lf\n",&Codend.mfc);
  	  tmpd = Codend.mfc;
  	  printf("mfc 	= %10.3lf \t full mesh size around (m)\n",tmpd);
  	  do 		c=fgetc(f1);	while (c !=':'); 
  	  fscanf(f1,"%lf\n",&Codend.mfm);
  	  tmpd = Codend.mfm;
  	  printf("mfm 	= %10.3lf \t full mesh size along (m)\n",tmpd);
      }
  	
  	if (Codend.type == 4)
      {
  	  /* full mesh size ***********************************************************/
  	  do 		c=fgetc(f1);	while (c !=':'); 
  	  fscanf(f1,"%lf\n",&Codend.mfc);
  	  tmpd = Codend.mfc;
  	  printf("mfc 	= %10.3lf \t full mesh size forward (m)\n",tmpd);
  	  do 		c=fgetc(f1);	while (c !=':'); 
  	  fscanf(f1,"%lf\n",&Codend.mfm);
  	  tmpd = Codend.mfm;
  	  printf("mfm 	= %10.3lf \t full mesh size backward (m)\n",tmpd);
      }
  	
  	/* internal mesh **********************************************************
  	do      c=fgetc(f1);	while (c !=':'); 
  	fscanf(f1,"%lf\n",&Codend.mi);
  	tmpd = Codend.mi;
  	printf("%s %lf %s \n","mi 	= ",tmpd,"\t\t (m) internal mesh");*/

  	/* twine stiffness ***********************************************************/
  	do 		c=fgetc(f1);	while (c !=':'); 
  	fscanf(f1,"%lf\n",&Codend.stf);
  	tmpd = Codend.stf;
  	printf("stf 	= %10.0lf \t stiffness per twine (N)\n",tmpd);

  	/* twine nb **********************************************************
  	do 		c=fgetc(f1);	while (c !=':'); 
  	fscanf(f1,"%d\n",&Codend.nbt);
  	tmpi = Codend.nbt;
  	printf("%s %d %s \n","nbt	= ",tmpi,"\t\t\t nb of twines per side");*/

  	/* twine diameter **********************************************************
  	do 		c=fgetc(f1);	while (c !=':'); 
  	fscanf(f1,"%lf\n",&Codend.diam);
  	tmpd = Codend.diam;
  	printf("%s %lf %s \n","diam 	= ",tmpd,"\t\t (m) one twine diameter");*/

  	/* twine nb in selvedge **********************************************************
  	do 		c=fgetc(f1);	while (c !=':'); 
  	fscanf(f1,"%d\n",&Codend.sm);
  	tmpi = Codend.sm;
  	printf("%s %d %s \n","sm 	= ",tmpi,"\t\t\t nb of twine in the selvedge");*/

  	/* limit of the catch from the extremity (m) ***********************************************************/
  	do 		c=fgetc(f1);	while (c !=':'); 
  	fscanf(f1,"%lf\n",&Prise.epaisseur);
  	tmpd = Prise.epaisseur;
  	printf("lim 	= %10.3lf \t catch length (m)\n",tmpd);

  	/* EI of the netting (?) **********************************************************
  	do 		c=fgetc(f1);	while (c !=':'); 
  	fscanf(f1,"%lf\n",&Codend.EI);
  	tmpd = Codend.EI;
  	printf("%s %lf %s \n","EI 	= ",tmpd,"\t\t (?)");*/

  	/* radius of the entry (m) ***********************************************************/
  	do 		c=fgetc(f1);	while (c !=':'); 
  	fscanf(f1,"%lf\n",&Codend.radius);
  	tmpd = Codend.radius;
  	printf("radius 	= %10.3lf \t radius of the entry (m)\n",tmpd);

  	/* towing speed (m/s) ***********************************************************/
  	do 		c=fgetc(f1);	while (c !=':'); 
  	fscanf(f1,"%lf\n",&Courant.vitesse);
  	tmpd = Courant.vitesse;
  	printf("speed 	= %10.2lf \t towing speed (m/s)\n",tmpd);

  	return 1;
	}	


