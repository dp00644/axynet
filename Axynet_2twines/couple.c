#define PRINCIPAL 0
#include "AXYnet.h"
 #include <stdio.h>
 #include <stdlib.h>
 #include <string.h>
 #include <math.h>

/* calculate the effect of a stiffness couple on each knot*/

int couple()
	{
	int no;
	double hi,hj,ri,rj,xi,xj,li,alphai,alphaj,Nbr,ang,alpha_0,hh,EI;
	double couplei,couplej;
  	int zk,zl;
    	
	/*PA		EI =  2.5 * PI * diam * diam;*/
	/*PE		EI = 25.0 * PI * diam * diam;*/
    	EI = Codend.EI;				/*flexion stiffness in ?*/
    	alpha_0 = 0.0;				/*half neutral angle between twines*/
    	hh = 6.0 * EI / (Codend.mf / 2.0);	/*couple stiffness in N.m/Rad*/
    	Nbr = Codend.nbr;
    	ang = Codend.angle;
	for(no=1; no<=2*Codend.nbl;no++)
		{
    		/*hh = 2.0 * sqrt(EI * fabs(Codend.T[no]));	*****************************************/
		ri = Codend.r[no];			rj = Codend.r[no+1];		/*rayon*/
		xi = Codend.x[no];			xj = Codend.x[no+1];		/*x*/
		hi = 2.0 * PI * ri / Nbr;		hj = 2.0 * PI * rj / Nbr;	/*1/2 ouverture de la maille*/
		li = sqrt((xi-xj)*(xi-xj) + ri*ri + rj*rj - 2 * ri*rj*cos(ang));	/*longueur de la maille*/
		alphai = atan(hj/li);			alphaj = atan(hi/li);		/*1/2 angle d ouverture de la maille*/
		couplei = hh * (alphai-alpha_0);	couplej = hh * (alphaj-alpha_0);/*couple entre le fils et le noeud*/
		/*couplei = sin(alphai)/alphai * hh * (alphai-alpha_0);	
		couplei = sin(alphaj)/alphaj * hh * (alphaj-alpha_0);*/	
		/*printf("couples %lf  %lf\n",couplei,couplej);*/
		/*upstream: ie en i*/
		Codend.fr[no]   = Codend.fr[no]   - couplei * (   0.0*li - hj*(ri-rj*cos(ang))/li) / (li*li+hj*hj);
		Codend.fr[no+1] = Codend.fr[no+1] - couplei * (PI/Nbr*li - hj*(rj-ri*cos(ang))/li) / (li*li+hj*hj);
		Codend.fx[no]   = Codend.fx[no]   - couplei * (   0.0*li - hj*(xi-xj         )/li) / (li*li+hj*hj);
		Codend.fx[no+1] = Codend.fx[no+1] - couplei * (   0.0*li - hj*(xj-xi         )/li) / (li*li+hj*hj);		
		/*downstream: ie en j*/
		Codend.fr[no]   = Codend.fr[no]   - couplej * (PI/Nbr*li - hi*(ri-rj*cos(ang))/li) / (li*li+hi*hi);
		Codend.fr[no+1] = Codend.fr[no+1] - couplej * (   0.0*li - hi*(rj-ri*cos(ang))/li) / (li*li+hi*hi);
		Codend.fx[no]   = Codend.fx[no]   - couplej * (   0.0*li - hi*(xi-xj         )/li) / (li*li+hi*hi);
		Codend.fx[no+1] = Codend.fx[no+1] - couplej * (   0.0*li - hi*(xj-xi         )/li) / (li*li+hi*hi);		
		}
	
	/*affectation effort tension*/
  	for (zl=1;zl<=n_ligne;zl++)
  		{
  		zk = zl-Codend.nbpt;
  		if (zl>Codend.nbpt)	ff[zl] = Codend.fr[zk];
  		else			ff[zl] = Codend.fx[zl];
  		}
  		
	return 0;
	}	


