#include <stdio.h>
#include <stdlib.h>
#define PRINCIPAL 1
#include "AXYnet.h"
#include <string.h>
#include <math.h>

int main(int argc, char *argv[])
{
    char nomfichier[100];
void kite();
void rectangle();
void square();
 void losange();
   
    printf("This tool allows to calculate the shape of cod-end.\n");
    printf("The data are in the .dat file.\n");
    printf("The results will be in the .sha file after the iterative process.\n");
    printf("The value of param.txt can be modified to improve the convergence speed.\n");
    /*****************************************************************************************************/
	if (argc == 1)
		{
		printf("Enter the .dat file without the extension:\n");
		scanf("%s", nomfichier);
  		}
	if ((argc == 2) || (argc == 3))
		{
		strcpy(nomfichier,argv[1]);
    		printf("%s 				%s\n","file ",nomfichier);
  		}
  	/*****************************************************************************************/
    reading_dat(nomfichier);                /*reading data in *.dat*/
    test_dat();                  			/*test of data in *.dat*/
    switch(Codend.type)
    {
     case 1: //diamond
          printf("mesh parameters => DIAMOND (push a key to proceed)\n");
          //system("PAUSE");
          losange();
          break;
     case 2: //square
          printf("mesh parameters => SQUARE (push a key to proceed)\n");
          //system("PAUSE");
          square();
          break;
     case 3: //rectangle
          printf("mesh parameters => RECTANGLE (push a key to proceed)\n");
          //system("PAUSE");
          rectangle();
          break;
     case 4: //kite
          printf("mesh parameters => KITE (push a key to proceed)\n");
          //system("PAUSE");
          kite();
          break;
     case 5: //hexa1
          printf("mesh parameters => HEXAGONAL AXIAL (push a key to proceed)\n");
          //system("PAUSE");
          //hexa1();
          break;
     case 6: //hexa2
          printf("mesh parameters => HEXAGONAL NORMAL (push a key to proceed)\n");
          //system("PAUSE");
          //hexa2();
          break;
    }
    writing_don(nomfichier);                /*write *.sha*/
    printf("End of the application\n");
    //system("PAUSE"); //attente d'une touche pour passer � la suite
  return 1;
}
