/*prototypes de alloc.c*/
double *Malloc_REAL(int nb);
double *Malloc_double(int nb);
float *Malloc_float(int nb);
int *Malloc_int(int nb);

/*prototypes de catch_1.c*/
int catch_1();

/*prototypes de coordonnees.c*/
int coordonnees();

/*prototypes de initial.c*/
int initial();

/*prototypes de lectParam.c*/
double lectParam();

/*prototypes de limites.c*/
int limites();

/*prototypes de miseazero.c*/
int miseazero();

/*prototypes de reading_dat.c*/
int reading_dat(char *nomfichier);
int reading_case_2(FILE *f1);

/*prototypes de solver.c*/
int solver(int nb_row, double **stiff, double *contr, double *result);

/*prototypes de tension_circular.c*/
int tension_circular();

/*prototypes de tension_meridian.c*/
int tension_meridian();

/*prototypes de tension.c*/
int tension();

/*prototypes de test_dat.c*/
int test_dat();

/*prototypes de writing_don.c*/
int writing_don(char *nomfichier);

