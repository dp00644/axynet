#define PRINCIPAL 0
#include "AXYnet.h"

/* test of file .dat */

int test_dat()
	{
	/*test of type*/
	if (Codend.type < 1)
		{
    		printf("Codend.type must > 1 in *.dat file\n");
    		//system("PAUSE");
    		exit(0);		
		}
	if (Codend.type > 4)
		{
    		printf("Codend.type must < 5 in *.dat file\n");
    		//system("PAUSE");
    		exit(0);		
		}/**/
	/*test of nbr*/
	if (Codend.nbr < 0.0)
		{
    		printf("Codend.nbr must be > 0 in *.dat file\n");
    		//system("PAUSE");
    		exit(0);		
		}
	/*test of nbl*/
	if (Codend.nbl < 0.0)
		{
    		printf("Codend.nbl must be > 0 in *.dat file\n");
    		//system("PAUSE");
    		exit(0);		
		}
	/*test of mi*/
	if (Codend.mf < Codend.mi)
		{
    		printf("Codend.mf must be > Codend.mi in *.dat file\n");
    		//system("PAUSE");
    		exit(0);		
		}
	if (Codend.mi < 0.0)
		{
    		printf("Codend.mi must be > 0.0 in *.dat file\n");
    		//system("PAUSE");
    		exit(0);		
		}
	/*test of stf*/
	if (Codend.stf < 0.0)
		{
    		printf("Codend.stf must be > 0.0 in *.dat file\n");
    		//system("PAUSE");
    		exit(0);		
		}
	/*test of nbt*/
	if (Codend.nbt < 0)
		{
    		printf("Codend.nbt must be > 0.0 in *.dat file\n");
    		//system("PAUSE");
    		exit(0);		
		}
	/*test of diam*/
	if (Codend.diam < 0)
		{
    		printf("Codend.diam must be > 0.0 in *.dat file\n");
    		//system("PAUSE");
    		exit(0);		
		}
	if (Codend.diam > Codend.mi)
		{
    		printf("Codend.diam must be < Codend.mi in *.dat file\n");
    		//system("PAUSE");
    		exit(0);		
		}
	
  	return 0;
	}	


