#define PRINCIPAL 0
#include "AXYnet.h"

/* catch on the cod-end */

int catch_square()
	{
	int no,np;
	double max_x,Pres,ratio;
  	int zk,zl;
  	
  	/*pressure in Pa*/
  	Pres = 1.4 * 0.5 * 1025.0 * Courant.vitesse * Courant.vitesse;    			
      					
      	/*TEST_debut
	int zu,zm,zv;
	double delta_x, FF[100], MAT[100][100];
	delta_x = 0.000001;
	for( zu = 0 ; zu <= 2*Codend.nbpt+1 ; zu++ )
      		{
      		if ((zu > 0) && (zu < 2*Codend.nbpt+1))
      			{
			if (zu > Codend.nbpt) 	
				{
 				printf("zu >  Codend.nbpt zu : %8d   \n",zu);
 				zm = zu-Codend.nbpt;
				Codend.r[zm] = Codend.r[zm] + delta_x;
				}
			else 			
				{
 				printf("zu <= Codend.nbpt zu : %8d   \n",zu);
				Codend.x[zu] = Codend.x[zu] + delta_x;
				}
			}
  		for (zl=1;zl<=Codend.nbpt;zl++)
  			{
  			Codend.fx[zl] = 0.0;
  			Codend.fr[zl] = 0.0;
  			}
  		for (zl=1;zl<=2*Codend.nbpt;zl++)
  			{
  			for (zm=1;zm<=2*Codend.nbpt;zm++)
  				{
				mat[zl][zm] = 0.0;
				}		
			}
      		TEST_fin*/    
      					
	/*max of x*/
	max_x = Codend.x[1];
	for(no=1; no<=Codend.nbpt;no++)
		{
		if (Codend.x[no] > max_x)
			{
			max_x = Codend.x[no];
			}
 		}    
	/*printf("max_x %lf  \n",max_x);*/
	Prise.front = max_x - Prise.epaisseur;
      				
	/*pressure on nodes*/
	/*be carefull : angle is pi/nbr*/
	for(no=1; no<=2*Codend.nbl;no++)
		{
		if (Codend.x[no] > Prise.front)
			{
 			Codend.fx[no]   = Codend.fx[no]   - Codend.angle*Pres/2.0 * (Codend.r[no+1]*Codend.r[no+1] - Codend.r[no]*Codend.r[no]);
 			Codend.fx[no+1] = Codend.fx[no+1] - Codend.angle*Pres/2.0 * (Codend.r[no+1]*Codend.r[no+1] - Codend.r[no]*Codend.r[no]);
 			Codend.fr[no]   = Codend.fr[no]   - Codend.angle*Pres/2.0 * ((Codend.r[no]+Codend.r[no+1]) * (Codend.x[no]-Codend.x[no+1]));
 			Codend.fr[no+1] = Codend.fr[no+1] - Codend.angle*Pres/2.0 * ((Codend.r[no]+Codend.r[no+1]) * (Codend.x[no]-Codend.x[no+1]));
  			}
		if ((Codend.x[no] <= Prise.front) && (Codend.x[no+1] > Prise.front))
			{
			ratio = (Codend.x[no+1]-Prise.front) / (Codend.x[no+1]-Codend.x[no]);
 			Codend.fx[no]   = Codend.fx[no]   - ratio*Codend.angle*Pres/2.0 * (Codend.r[no+1]*Codend.r[no+1] - Codend.r[no]*Codend.r[no]);
 			Codend.fx[no+1] = Codend.fx[no+1] - ratio*Codend.angle*Pres/2.0 * (Codend.r[no+1]*Codend.r[no+1] - Codend.r[no]*Codend.r[no]);
 			Codend.fr[no]   = Codend.fr[no]   - ratio*Codend.angle*Pres/2.0 * ((Codend.r[no]+Codend.r[no+1]) * (Codend.x[no]-Codend.x[no+1]));
 			Codend.fr[no+1] = Codend.fr[no+1] - ratio*Codend.angle*Pres/2.0 * ((Codend.r[no]+Codend.r[no+1]) * (Codend.x[no]-Codend.x[no+1]));
  			}
		}

	/*affectation effort tension*/
  	for (zl=1;zl<=n_ligne;zl++)
  		{
  		zk = zl-Codend.nbpt;
  		if (zl>Codend.nbpt)	ff[zl] = Codend.fr[zk];
  		else			ff[zl] = Codend.fx[zl];
  		}
  		
  	/*derivee de ff*/
	for(no=1; no<=2*Codend.nbl;no++)
		{
		if (Codend.x[no] > Prise.front)
			{
			np = no+Codend.nbpt;
  			mat[no  ][no  ] = mat[no  ][no  ] + 0.0;
  			mat[no  ][no+1] = mat[no  ][no+1] + 0.0;
  			mat[no  ][np  ] = mat[no  ][np  ] - Codend.angle*Pres/1.0 * Codend.r[no  ];
  			mat[no  ][np+1] = mat[no  ][np+1] + Codend.angle*Pres/1.0 * Codend.r[no+1];
  			mat[no+1][no  ] = mat[no+1][no  ] + 0.0;
  			mat[no+1][no+1] = mat[no+1][no+1] + 0.0;
  			mat[no+1][np  ] = mat[no+1][np  ] - Codend.angle*Pres/1.0 * Codend.r[no  ];
  			mat[no+1][np+1] = mat[no+1][np+1] + Codend.angle*Pres/1.0 * Codend.r[no+1];
  			mat[np  ][no  ] = mat[np  ][no  ] + Codend.angle*Pres/2.0 * (Codend.r[no  ]+Codend.r[no+1]);
  			mat[np  ][no+1] = mat[np  ][no+1] - Codend.angle*Pres/2.0 * (Codend.r[no  ]+Codend.r[no+1]);
  			mat[np  ][np  ] = mat[np  ][np  ] - Codend.angle*Pres/2.0 * (Codend.x[no+1]-Codend.x[no  ]);
  			mat[np  ][np+1] = mat[np  ][np+1] - Codend.angle*Pres/2.0 * (Codend.x[no+1]-Codend.x[no  ]);
  			mat[np+1][no  ] = mat[np+1][no  ] + Codend.angle*Pres/2.0 * (Codend.r[no  ]+Codend.r[no+1]);
  			mat[np+1][no+1] = mat[np+1][no+1] - Codend.angle*Pres/2.0 * (Codend.r[no  ]+Codend.r[no+1]);
  			mat[np+1][np  ] = mat[np+1][np  ] - Codend.angle*Pres/2.0 * (Codend.x[no+1]-Codend.x[no  ]);
  			mat[np+1][np+1] = mat[np+1][np+1] - Codend.angle*Pres/2.0 * (Codend.x[no+1]-Codend.x[no  ]);
  			}
		if ((Codend.x[no] <= Prise.front) && (Codend.x[no+1] > Prise.front))
			{
			ratio = (Codend.x[no+1]-Prise.front) / (Codend.x[no+1]-Codend.x[no]);
			np = no+Codend.nbpt;
  			mat[no  ][no  ] = mat[no  ][no  ] + ratio*0.0;
  			mat[no  ][no+1] = mat[no  ][no+1] + ratio*0.0;
  			mat[no  ][np  ] = mat[no  ][np  ] - ratio*Codend.angle*Pres/1.0 * Codend.r[no  ];
  			mat[no  ][np+1] = mat[no  ][np+1] + ratio*Codend.angle*Pres/1.0 * Codend.r[no+1];
  			mat[no+1][no  ] = mat[no+1][no  ] + ratio*0.0;
  			mat[no+1][no+1] = mat[no+1][no+1] + ratio*0.0;
  			mat[no+1][np  ] = mat[no+1][np  ] - ratio*Codend.angle*Pres/1.0 * Codend.r[no  ];
  			mat[no+1][np+1] = mat[no+1][np+1] + ratio*Codend.angle*Pres/1.0 * Codend.r[no+1];
  			mat[np  ][no  ] = mat[np  ][no  ] + ratio*Codend.angle*Pres/2.0 * (Codend.r[no  ]+Codend.r[no+1]);
  			mat[np  ][no+1] = mat[np  ][no+1] - ratio*Codend.angle*Pres/2.0 * (Codend.r[no  ]+Codend.r[no+1]);
  			mat[np  ][np  ] = mat[np  ][np  ] - ratio*Codend.angle*Pres/2.0 * (Codend.x[no+1]-Codend.x[no  ]);
  			mat[np  ][np+1] = mat[np  ][np+1] - ratio*Codend.angle*Pres/2.0 * (Codend.x[no+1]-Codend.x[no  ]);
  			mat[np+1][no  ] = mat[np+1][no  ] + ratio*Codend.angle*Pres/2.0 * (Codend.r[no  ]+Codend.r[no+1]);
  			mat[np+1][no+1] = mat[np+1][no+1] - ratio*Codend.angle*Pres/2.0 * (Codend.r[no  ]+Codend.r[no+1]);
  			mat[np+1][np  ] = mat[np+1][np  ] - ratio*Codend.angle*Pres/2.0 * (Codend.x[no+1]-Codend.x[no  ]);
  			mat[np+1][np+1] = mat[np+1][np+1] - ratio*Codend.angle*Pres/2.0 * (Codend.x[no+1]-Codend.x[no  ]);
  			}
  		}
      	/*TEST_debut
		printf("zu %5d  \n",zu);
      		if (zu == 0)
      			{
			for (zl = 1; zl<= 2*Codend.nbpt; zl++)	FF[zl] = ff[zl];
      			}
      		if ((zu > 0) && (zu < 2*Codend.nbpt+1))
      			{
			if (zu > Codend.nbpt) 	
				{
 				zm = zu-Codend.nbpt;
				Codend.r[zm] = Codend.r[zm] - delta_x;
				}
			else 			
				{
				Codend.x[zu] = Codend.x[zu] - delta_x;
				}
			for (zm = 1 ;zm <= 2*Codend.nbpt ;zm++ )	MAT[zm][zu] = - (ff[zm] - FF[zm]) / delta_x;
      			}
      		if (zu == 2*Codend.nbpt+1)
      			{		
			for (zm = 1 ;zm <= 2*Codend.nbpt ;zm++ )
				{
				printf("mat = ");	for (zv = 1 ;zv <= 4 ;zv++ )	printf("%10.0lf ",mat[zm][zv]);	printf(" \n");
				printf("MAT = ");	for (zv = 1 ;zv <= 4 ;zv++ )	printf("%10.0lf ",MAT[zm][zv]);	printf(" \n");
				}
      			exit(0);
      			}
      		}
      		TEST_fin*/    		
	return 0;
	}	


