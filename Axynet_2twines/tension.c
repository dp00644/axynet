#define PRINCIPAL 0
#include "AXYnet.h"

/* create the initial shape of the cod-end */

int tension()
	{
	int no,np;
	double l0;
  	int zk,zl;
  	
    /*TEST_debut
	int zu,zm,zv;
	double delta_x, FF[100], MAT[100][100];
	delta_x = 0.000001;
	for( zu = 0 ; zu <= 2*Codend.nbpt+1 ; zu++ )
      		{
      		if ((zu > 0) && (zu < 2*Codend.nbpt+1))
      			{
			if (zu > Codend.nbpt) 	
				{
 				printf("zu >  Codend.nbpt zu : %8d   \n",zu);
 				zm = zu-Codend.nbpt;
				Codend.r[zm] = Codend.r[zm] + delta_x;
				}
			else 			
				{
 				printf("zu <= Codend.nbpt zu : %8d   \n",zu);
				Codend.x[zu] = Codend.x[zu] + delta_x;
				}
			}
  		for (zl=1;zl<=Codend.nbpt;zl++)
  			{
  			Codend.fx[zl] = 0.0;
  			Codend.fr[zl] = 0.0;
  			}
      		TEST_fin*/    
      					
	/*twine tension*/
    l0 = Codend.mf / 2.0;
  	if (Codend.type == 4)
       {
       l0 = Codend.mfc / 2.0;
       }
	for(no=1; no<=2*Codend.nbl;no+=2)
		{
		Codend.l[no] = sqrt((Codend.x[no]-Codend.x[no+1])*(Codend.x[no]-Codend.x[no+1]) + 
		Codend.r[no]*Codend.r[no] + Codend.r[no+1]*Codend.r[no+1] - 2 * Codend.r[no]*Codend.r[no+1]*cos(Codend.angle));
		Codend.T[no] = Codend.stf * (Codend.l[no] - l0) / l0;
 		Codend.fr[no]   = Codend.fr[no]   -  Codend.T[no] / Codend.l[no] * (Codend.r[no] - Codend.r[no+1]*cos(Codend.angle));
 		Codend.fr[no+1] = Codend.fr[no+1] +  Codend.T[no] / Codend.l[no] * (Codend.r[no]*cos(Codend.angle) - Codend.r[no+1]);
 		Codend.fx[no]   = Codend.fx[no]   -  Codend.T[no] / Codend.l[no] * (Codend.x[no] - Codend.x[no+1]);
 		Codend.fx[no+1] = Codend.fx[no+1] +  Codend.T[no] / Codend.l[no] * (Codend.x[no] - Codend.x[no+1]);
 		
 		/*printf("l: %8.4lf   T: %8.0lf   \n",Codend.l[no],Codend.T[no]);*/
		}
		
    l0 = Codend.mf / 2.0;
  	if (Codend.type == 4)
       {
       l0 = Codend.mfm / 2.0;
       }
	for(no=2; no<=2*Codend.nbl;no+=2)
		{
		Codend.l[no] = sqrt((Codend.x[no]-Codend.x[no+1])*(Codend.x[no]-Codend.x[no+1]) + 
		Codend.r[no]*Codend.r[no] + Codend.r[no+1]*Codend.r[no+1] - 2 * Codend.r[no]*Codend.r[no+1]*cos(Codend.angle));
		Codend.T[no] = Codend.stf * (Codend.l[no] - l0) / l0;
 		Codend.fr[no]   = Codend.fr[no]   -  Codend.T[no] / Codend.l[no] * (Codend.r[no] - Codend.r[no+1]*cos(Codend.angle));
 		Codend.fr[no+1] = Codend.fr[no+1] +  Codend.T[no] / Codend.l[no] * (Codend.r[no]*cos(Codend.angle) - Codend.r[no+1]);
 		Codend.fx[no]   = Codend.fx[no]   -  Codend.T[no] / Codend.l[no] * (Codend.x[no] - Codend.x[no+1]);
 		Codend.fx[no+1] = Codend.fx[no+1] +  Codend.T[no] / Codend.l[no] * (Codend.x[no] - Codend.x[no+1]);
 		
 		/*printf("l: %8.4lf   T: %8.0lf   \n",Codend.l[no],Codend.T[no]);*/
		}
		
	/*catch drag (along the X axis) on all the cod-end*/
    Codend.drag = 2.0 * Codend.nbr * Codend.fx[1];

	/*for(no=1; no<=Codend.nbpt;no++)
		{
 		printf("x: %8.4lf  r: %8.4lf fx: %8.0lf  fr: %8.0lf \n",Codend.x[no],Codend.r[no],Codend.fx[no],Codend.fr[no]);
		}*/
  	
	/*affectation raideur tension
  	for (zl=1;zl<=n_ligne;zl++)
  		{
  		for (zk=1;zk<=n_ligne;zk++)	mat[zl][zk] = 0.0;
  		mat[zl][zl] = 1.0;
  		}*/
  		
	/*affectation effort tension*/
  	for (zl=1;zl<=n_ligne;zl++)
  		{
  		zk = zl-Codend.nbpt;
  		if (zl>Codend.nbpt)	ff[zl] = Codend.fr[zk];
  		else			    ff[zl] = Codend.fx[zl];
 		/*printf("ff: %8.4lf  \n",ff[zl]);*/
  		}
  		
  	/*derivee de ff*/
	for(no=1; no<=2*Codend.nbl;no++)
		{
		np = no+Codend.nbpt;
  		mat[no  ][no  ] = mat[no  ][no  ] + Codend.stf / Codend.l[no] / Codend.l[no] / Codend.l[no] * (Codend.x[no]-Codend.x[no+1]) * (Codend.x[no]-Codend.x[no+1]) + Codend.T[no] / Codend.l[no];
  		mat[no  ][no+1] = mat[no  ][no+1] - Codend.stf / Codend.l[no] / Codend.l[no] / Codend.l[no] * (Codend.x[no]-Codend.x[no+1]) * (Codend.x[no]-Codend.x[no+1]) - Codend.T[no] / Codend.l[no];
  		mat[no  ][np  ] = mat[no  ][np  ] + Codend.stf / Codend.l[no] / Codend.l[no] / Codend.l[no] * (Codend.x[no]-Codend.x[no+1]) * (Codend.r[no]-Codend.r[no+1]*cos(Codend.angle));
   		mat[no  ][np+1] = mat[no  ][np+1] + Codend.stf / Codend.l[no] / Codend.l[no] / Codend.l[no] * (Codend.x[no]-Codend.x[no+1]) * (Codend.r[no+1]-Codend.r[no]*cos(Codend.angle));
  		mat[no+1][no  ] = mat[no+1][no  ] - Codend.stf / Codend.l[no] / Codend.l[no] / Codend.l[no] * (Codend.x[no]-Codend.x[no+1]) * (Codend.x[no]-Codend.x[no+1]) - Codend.T[no] / Codend.l[no];
  		mat[no+1][no+1] = mat[no+1][no+1] + Codend.stf / Codend.l[no] / Codend.l[no] / Codend.l[no] * (Codend.x[no]-Codend.x[no+1]) * (Codend.x[no]-Codend.x[no+1]) + Codend.T[no] / Codend.l[no];
  		mat[no+1][np  ] = mat[no+1][np  ] - Codend.stf / Codend.l[no] / Codend.l[no] / Codend.l[no] * (Codend.x[no]-Codend.x[no+1]) * (Codend.r[no]-Codend.r[no+1]*cos(Codend.angle));
   		mat[no+1][np+1] = mat[no+1][np+1] - Codend.stf / Codend.l[no] / Codend.l[no] / Codend.l[no] * (Codend.x[no]-Codend.x[no+1]) * (Codend.r[no+1]-Codend.r[no]*cos(Codend.angle));
  		mat[np  ][no  ] = mat[np  ][no  ] + Codend.stf / Codend.l[no] / Codend.l[no] / Codend.l[no] * (Codend.x[no]-Codend.x[no+1]) * (Codend.r[no]-Codend.r[no+1]*cos(Codend.angle));
  		mat[np  ][no+1] = mat[np  ][no+1] - Codend.stf / Codend.l[no] / Codend.l[no] / Codend.l[no] * (Codend.x[no]-Codend.x[no+1]) * (Codend.r[no]-Codend.r[no+1]*cos(Codend.angle));
  		mat[np  ][np  ] = mat[np  ][np  ] + Codend.stf / Codend.l[no] / Codend.l[no] / Codend.l[no] * (Codend.r[no]-Codend.r[no+1]*cos(Codend.angle)) * (Codend.r[no]-Codend.r[no+1]*cos(Codend.angle)) + Codend.T[no] / Codend.l[no];
  		mat[np  ][np+1] = mat[np  ][np+1] + Codend.stf / Codend.l[no] / Codend.l[no] / Codend.l[no] * (Codend.r[no+1]-Codend.r[no]*cos(Codend.angle)) * (Codend.r[no]-Codend.r[no+1]*cos(Codend.angle)) - Codend.T[no] / Codend.l[no] * cos(Codend.angle);
  		mat[np+1][no  ] = mat[np+1][no  ] - Codend.stf / Codend.l[no] / Codend.l[no] / Codend.l[no] * (Codend.x[no]-Codend.x[no+1]) * (Codend.r[no]*cos(Codend.angle)-Codend.r[no+1]);
  		mat[np+1][no+1] = mat[np+1][no+1] + Codend.stf / Codend.l[no] / Codend.l[no] / Codend.l[no] * (Codend.x[no]-Codend.x[no+1]) * (Codend.r[no]*cos(Codend.angle)-Codend.r[no+1]);
  		mat[np+1][np  ] = mat[np+1][np  ] - Codend.stf / Codend.l[no] / Codend.l[no] / Codend.l[no] * (Codend.r[no]-Codend.r[no+1]*cos(Codend.angle)) * (Codend.r[no]*cos(Codend.angle)-Codend.r[no+1]) - Codend.T[no] / Codend.l[no] * cos(Codend.angle);
  		mat[np+1][np+1] = mat[np+1][np+1] + Codend.stf / Codend.l[no] / Codend.l[no] / Codend.l[no] * (Codend.r[no]*cos(Codend.angle)-Codend.r[no+1]) * (Codend.r[no]*cos(Codend.angle)-Codend.r[no+1]) + Codend.T[no] / Codend.l[no];
		/*printf("no = %d\n",no);			
		for (zm = 1 ;zm <= 2*Codend.nbpt ;zm++ )
			{
			printf("mat = ");	for (zv = 1 ;zv <= 6 ;zv++ )	printf("%10.0lf ",mat[zm][zv]);	printf(" \n");
			}*/
  		}
  		
      	/*TEST_debut
		printf("zu %5d  \n",zu);
      		if (zu == 0)
      			{
			for (zl = 1; zl<= 2*Codend.nbpt; zl++)	FF[zl] = ff[zl];
      			}
      		if ((zu > 0) && (zu < 2*Codend.nbpt+1))
      			{
			if (zu > Codend.nbpt) 	
				{
 				zm = zu-Codend.nbpt;
				Codend.r[zm] = Codend.r[zm] - delta_x;
				}
			else 			
				{
				Codend.x[zu] = Codend.x[zu] - delta_x;
				}
			for (zm = 1 ;zm <= 2*Codend.nbpt ;zm++ )	MAT[zm][zu] = - (ff[zm] - FF[zm]) / delta_x;
      			}
      		if (zu == 2*Codend.nbpt+1)
      			{		
			for (zm = 1 ;zm <= 2*Codend.nbpt ;zm++ )
				{
				printf("mat = ");	for (zv = 1 ;zv <= 4 ;zv++ )	printf("%10.0lf ",mat[zm][zv]);	printf(" \n");
				printf("MAT = ");	for (zv = 1 ;zv <= 4 ;zv++ )	printf("%10.0lf ",MAT[zm][zv]);	printf(" \n");
				}
      			exit(0);
      			}
      		}
      		TEST_fin*/    		
	return 0;
	}	


