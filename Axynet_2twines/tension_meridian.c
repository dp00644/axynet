#define PRINCIPAL 0
#include "AXYnet.h"

/* tension of twines along the meridian of the cod-end */

int tension_meridian()
	{
	int no,np;
	double l0;
  	int zk,zl;
  	double li2,lil0,l0_li,xii,rii,EA_l2,EA_l3;
  	
	if (Codend.type == 3)
       {
  	   Codend.mf = Codend.mfm;
       }
 
  	
  	/*unstretched length*/
  	l0 = Codend.mf / 2.0;
    			
      	/*TEST_debut
	int zu,zm,zv;
	double delta_x, FF[100], MAT[100][100];
	delta_x = 0.0000001;
	for( zu = 0 ; zu <= 2*Codend.nbpt+1 ; zu++ )
      		{
      		if ((zu > 0) && (zu < 2*Codend.nbpt+1))
      			{
			if (zu > Codend.nbpt) 	
				{
 				printf("zu >  Codend.nbpt zu : %8d   \n",zu);
 				zm = zu-Codend.nbpt;
				Codend.r[zm] = Codend.r[zm] + delta_x;
				}
			else 			
				{
 				printf("zu <= Codend.nbpt zu : %8d   \n",zu);
				Codend.x[zu] = Codend.x[zu] + delta_x;
				}
			}
  		for (zl=1;zl<=Codend.nbpt;zl++)
  			{
  			Codend.fx[zl] = 0.0;
  			Codend.fr[zl] = 0.0;
  			}
  		for (zl=1;zl<=2*Codend.nbpt;zl++)
  			{
  			for (zm=1;zm<=2*Codend.nbpt;zm++)
  				{
				mat[zl][zm] = 0.0;
				}		
			}
      		TEST_fin*/    
      					
	/*twine tension*/
	for(no=1; no<=2*Codend.nbl;no++)
		{
		Codend.l_meri[no] = sqrt((Codend.x[no]-Codend.x[no+1])*(Codend.x[no]-Codend.x[no+1]) + (Codend.r[no]-Codend.r[no+1])*(Codend.r[no]-Codend.r[no+1]));
		Codend.T_meri[no] = Codend.stf * (Codend.l_meri[no] - l0) / l0;
 		Codend.fr[no]   = Codend.fr[no]   +  Codend.T_meri[no] / Codend.l_meri[no] * (Codend.r[no+1] - Codend.r[no]);
 		Codend.fr[no+1] = Codend.fr[no+1] +  Codend.T_meri[no] / Codend.l_meri[no] * (Codend.r[no] - Codend.r[no+1]);
 		Codend.fx[no]   = Codend.fx[no]   +  Codend.T_meri[no] / Codend.l_meri[no] * (Codend.x[no+1] - Codend.x[no]);
 		Codend.fx[no+1] = Codend.fx[no+1] +  Codend.T_meri[no] / Codend.l_meri[no] * (Codend.x[no] - Codend.x[no+1]);
		}
		
	/*catch drag (along the X axis) on all the cod-end*/
    Codend.drag = 1.0 * Codend.nbr * Codend.fx[1];
    
	/*affectation effort tension*/
  	for (zl=1;zl<=n_ligne;zl++)
  		{
  		zk = zl-Codend.nbpt;
  		if (zl>Codend.nbpt)	ff[zl] = Codend.fr[zk];
  		else			ff[zl] = Codend.fx[zl];
  		}
  		
  	/*derivee de ff*/
	for(no=1; no<=2*Codend.nbl;no++)
		{
		li2 = Codend.l[no] * Codend.l[no];
		lil0 = Codend.l[no] * l0;
		l0_li = l0 / Codend.l[no];
		xii = Codend.x[no+1]-Codend.x[no];
		rii = Codend.r[no+1]-Codend.r[no];
		EA_l2 = Codend.stf / Codend.l[no] / Codend.l[no] / l0;
		EA_l3 = Codend.stf / Codend.l[no] / Codend.l[no] / Codend.l[no];
		np = no+Codend.nbpt;
  		mat[no  ][no  ] = mat[no  ][no  ] + EA_l2 * (li2-lil0+l0_li*xii*xii);
  		mat[no  ][no+1] = mat[no  ][no+1] - EA_l2 * (li2-lil0+l0_li*xii*xii);
  		mat[no  ][np  ] = mat[no  ][np  ] + EA_l3 * rii * xii;
   		mat[no  ][np+1] = mat[no  ][np+1] - EA_l3 * rii * xii;
  		mat[no+1][no  ] = mat[no+1][no  ] - EA_l2 * (li2-lil0+l0_li*xii*xii);
  		mat[no+1][no+1] = mat[no+1][no+1] + EA_l2 * (li2-lil0+l0_li*xii*xii);
  		mat[no+1][np  ] = mat[no+1][np  ] - EA_l3 * rii * xii;
   		mat[no+1][np+1] = mat[no+1][np+1] + EA_l3 * rii * xii;
  		mat[np  ][no  ] = mat[np  ][no  ] + EA_l3 * rii * xii;
  		mat[np  ][no+1] = mat[np  ][no+1] - EA_l3 * rii * xii;
  		mat[np  ][np  ] = mat[np  ][np  ] + EA_l2 * (li2-lil0+l0_li*rii*rii);
  		mat[np  ][np+1] = mat[np  ][np+1] - EA_l2 * (li2-lil0+l0_li*rii*rii);
  		mat[np+1][no  ] = mat[np+1][no  ] - EA_l3 * rii * xii;
  		mat[np+1][no+1] = mat[np+1][no+1] + EA_l3 * rii * xii;
  		mat[np+1][np  ] = mat[np+1][np  ] - EA_l2 * (li2-lil0+l0_li*rii*rii);
  		mat[np+1][np+1] = mat[np+1][np+1] + EA_l2 * (li2-lil0+l0_li*rii*rii);
  		}
  		
      	/*TEST_debut
		printf("zu %5d  \n",zu);
      		if (zu == 0)
      			{
			for (zl = 1; zl<= 2*Codend.nbpt; zl++)	FF[zl] = ff[zl];
      			}
      		if ((zu > 0) && (zu < 2*Codend.nbpt+1))
      			{
			if (zu > Codend.nbpt) 	
				{
 				zm = zu-Codend.nbpt;
				Codend.r[zm] = Codend.r[zm] - delta_x;
				}
			else 			
				{
				Codend.x[zu] = Codend.x[zu] - delta_x;
				}
			for (zm = 1 ;zm <= 2*Codend.nbpt ;zm++ )	MAT[zm][zu] = - (ff[zm] - FF[zm]) / delta_x;
      			}
      		if (zu == 2*Codend.nbpt+1)
      			{		
			for (zm = 1 ;zm <= 2*Codend.nbpt ;zm++ )
				{
				printf("mat = ");	for (zv = 1 ;zv <= 2*Codend.nbpt ;zv++ )	printf("%10.0lf ",mat[zm][zv]);	printf(" \n");
				printf("MAT = ");	for (zv = 1 ;zv <= 2*Codend.nbpt ;zv++ )	printf("%10.0lf ",MAT[zm][zv]);	printf(" \n");
				}
      			exit(0);
      			}
      		}
      		TEST_fin*/    		
	return 0;
	}	


