#define PRINCIPAL 0
#include "AXYnet.h"

	/*resolution of the system stiff * result = contr  with 
	stiff : squarre matrix of stiffness, 
	contr : known vector of constraint and 
	result : unknown vector of results
	solver(3, stiff, contr, result);  */

int solver(int nb_row, double **stiff, double *contr, double *result)
	{
	int zd,ze,zf;
	
 	/*printf("stiff	\n");
  	for (zd=1;zd<=nb_row;zd++)
  		{
  		for (ze=1;ze<=nb_row;ze++)	printf("%6.3lf  ",stiff[zd][ze]);
  		printf("\n");
  		}
	printf("contr	");imprvectreel(nb_row, contr);*/
	/*triangulation*/
	for (zd = 1; zd <= nb_row-1; zd++)
  		{
		for (ze = zd+1; ze <= nb_row; ze++)
  			{
			for (zf = zd+1; zf <= nb_row; zf++)
  				{
  				stiff[ze][zf] = stiff[ze][zf] - stiff[zd][zf] * stiff[ze][zd] / stiff[zd][zd];
  				}
  			contr[ze] = contr[ze] - contr[zd] * stiff[ze][zd] / stiff[zd][zd];
 			stiff[ze][zd] = 0.0;
  			}
  		}
 	/*printf("stiff	\n");
  	for (zd=1;zd<=nb_row;zd++)
  		{
  		for (ze=1;ze<=nb_row;ze++)	printf("%6.3lf  ",stiff[zd][ze]);
  		printf("\n");
  		}
	printf("contr	");imprvectreel(nb_row, contr);*/
	/*remonte*/
	for (zd = nb_row; zd >= 2; zd--)
  		{
  		result[zd] = contr[zd] / stiff[zd][zd];
		for (ze = zd-1; ze >= 1; ze--)
  			{
  			contr[ze] = contr[ze] - stiff[ze][zd] * result[zd];
  			}
  		}
  	result[1] = contr[1] / stiff[1][1];
	/*printf("result	");imprvectreel(nb_row, result);*/
	return 1;
	}

