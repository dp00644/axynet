#define PRINCIPAL 0
#include "AXYnet.h"

int writing_don(char *nomfichier)
	{
  	char   tonom[100];
  	FILE   *f1;
  	int    no;
  	
	/*

  	char   str[10],tonom[100],axe_perpendiculaire;
	int pt,elem,coordonnee_neutre,ele,nb_char,catch_name,zl,pan;
	for(no=1; no<=Codend.nbpt;no++)
		{
		zl = no+Codend.nbpt;
 		printf("ux: %8.4lf  ur: %8.4lf ",uu[no],uu[zl]);
 		printf("x: %8.4lf  r: %8.4lf fx: %8.0lf  fr: %8.0lf \n",Codend.x[no],Codend.r[no],Codend.fx[no],Codend.fr[no]);
		}*/

  	strcpy(tonom,nomfichier);
  	strcat(tonom,".sha");
  	/*printf(" \n");*/
  	printf("%s %s \n","Result file: ",tonom);
  	f1 = fopen(tonom,"w");
    	
   	fprintf(f1,"front (m): %10.3lf\n",Prise.front);
   	fprintf(f1,"drag  (N): %10.0lf\n",Codend.drag);
   	fprintf(f1,"vol.(m^3): %10.3lf\n",Prise.volume);
   	
   	fprintf(f1,"         x          r \n");
	for(no=1; no<=Codend.nbpt;no++)
		{
   		fprintf(f1,"%10.3lf %10.3lf\n",Codend.x[no],Codend.r[no]);
 		}
   	
   	fclose(f1);
	return 0;
	}
