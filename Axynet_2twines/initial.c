#define PRINCIPAL 0
#include "AXYnet.h"

/* create the initial shape of the cod-end */
/*initialisation of fx fr, condend.l, mat, uu & ff*/

int initial()
	{
	int no;
	double max_l;
  	int zk,p_colonne;
    	
	Codend.nbpt = 2*Codend.nbl + 1;	/*nb of points along the codend*/
	Codend.x = (double *) Malloc_double(1 + Codend.nbpt);
	Codend.r = (double *) Malloc_double(1 + Codend.nbpt);
	Codend.fx = (double *) Malloc_double(1 + Codend.nbpt);
	Codend.fr = (double *) Malloc_double(1 + Codend.nbpt);
	Codend.l = (double *) Malloc_double(1 + 2*Codend.nbl);
	Codend.T = (double *) Malloc_double(1 + 2*Codend.nbl);
	Codend.l_circ = (double *) Malloc_double(1 + 2*Codend.nbl);
	Codend.T_circ = (double *) Malloc_double(1 + 2*Codend.nbl);
	Codend.l_meri = (double *) Malloc_double(1 + 2*Codend.nbl);
	Codend.T_meri = (double *) Malloc_double(1 + 2*Codend.nbl);
	Codend.angle =  PI / Codend.nbr;
	Codend.angle_circ =  2.0 * PI / Codend.nbr;
		
    if (Codend.type < 3)
       {
  	   max_l = sqrt(Codend.radius * Codend.radius + (2*Codend.nbl*Codend.mf/2.0)*(2*Codend.nbl*Codend.mf/2.0));
      }
    if (Codend.type == 3)
       {
   	   max_l = sqrt(Codend.radius * Codend.radius + (2*Codend.nbl*Codend.mfm/2.0)*(2*Codend.nbl*Codend.mfm/2.0));
       }

    if (Codend.type == 4)
       {
   	   max_l = sqrt(Codend.radius * Codend.radius + (Codend.nbl*(Codend.mfm+Codend.mfc)/2.0)*(Codend.nbl*(Codend.mfm+Codend.mfc)/2.0));
       }

 	/*printf("Codend.radius: %lf      \n",Codend.radius);//
 	printf("Codend.nbl: %d      \n",Codend.nbl);
 	printf("Codend.mf: %lf      \n",Codend.mf);
 	printf("Codend.mfc: %lf      \n",Codend.mfc);
 	printf("Codend.mfm: %lf      \n",Codend.mfm);
 	printf("max_l: %lf      \n",max_l);*/
 	
	
	for(no=1; no<=Codend.nbpt;no++)
		{
		Codend.x[no] = max_l         / (Codend.nbpt - 1) * (no - 1);
		Codend.r[no] = Codend.radius / (Codend.nbpt - 1) * (Codend.nbpt - no);
		
		Codend.fx[no] = 0.0;
		Codend.fr[no] = 0.0;
		}
	no = Codend.nbpt;
	
	/*length of twines*/
	for(no=1; no<=2*Codend.nbl;no++)
		{
		Codend.l[no] = sqrt((Codend.x[no]-Codend.x[no+1])*(Codend.x[no]-Codend.x[no+1]) + 
		Codend.r[no]*Codend.r[no] + Codend.r[no+1]*Codend.r[no+1] 
		- 2 * Codend.r[no]*Codend.r[no+1]*cos(Codend.angle));
 	    /*printf("no: %d l %lf      \n",no,Codend.l[no]);*/
		}
		
 	/*resolution du systeme lineaire*/
    /*allocation de la matrice raideur mat*/
	n_ligne 	= 2 * (2*Codend.nbl+1);
    p_colonne 	= n_ligne;
       	
    xmat = (double *) malloc ((n_ligne+1) * (p_colonne+1) * sizeof(double));  	if (xmat == NULL){printf("xmat	= NULL \n" );/*system("PAUSE");*/exit(0);}
	mat = (double **) malloc ((n_ligne+1) * sizeof(double *));			if (mat	 == NULL){printf("mat	= NULL \n" );/*system("PAUSE");*/exit(0);}
  	cmat = xmat;
  	for (zk=0;zk<n_ligne+1;zk++)
  		{
  		mat[zk] = cmat;
  		cmat += p_colonne+1;
		}
  		
    /*allocation des vecteurs deplacement uu et resultat ff dans le systeme lineaure mat*uu = ff*/
    uu = (double *) malloc ((p_colonne+1) * sizeof(double));  	if (uu	== NULL){printf("uu	= NULL \n" );/*system("PAUSE");*/exit(0);}
    ff = (double *) malloc ((p_colonne+1) * sizeof(double));  	if (ff	== NULL){printf("uu	= NULL \n" );/*system("PAUSE");*/exit(0);}
	
	return 0;
	}	


