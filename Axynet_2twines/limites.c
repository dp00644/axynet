#define PRINCIPAL 0
#include "AXYnet.h"

/* create the initial shape of the cod-end */

int limites()
	{
  	int zk,zl;
    	

  	/*condition aux limites x[1] = 0.0: ligne 1*/
  	zk = 1;
  	for (zl=1;zl<=n_ligne;zl++)
  		{
  		mat[zk][zl] = 0.0;
  		mat[zl][zk] = 0.0;
  		}
  	mat[zk][zk] = 1.0;
  	ff[zk] = 0.0;
  	/*condition aux limites r[1] = radius: ligne Codend.nbpt+1*/
  	zk = Codend.nbpt+1;
  	for (zl=1;zl<=n_ligne;zl++)
  		{
  		mat[zk][zl] = 0.0;
  		mat[zl][zk] = 0.0;
  		}
  	mat[zk][zk] = 1.0;
  	ff[zk] = 0.0;
  	/*condition aux limites r[Codend.nbpt] = 0.0: ligne 2*Codend.nbpt*/
  	zk = 2*Codend.nbpt;
  	for (zl=1;zl<=n_ligne;zl++)
  		{
  		mat[zk][zl] = 0.0;
  		mat[zl][zk] = 0.0;
  		}
  	mat[zk][zk] = 1.0;
  	ff[zk] = 0.0;

	return 0;
	}	


