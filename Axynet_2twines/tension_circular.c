#define PRINCIPAL 0
#include "AXYnet.h"

/* take into account the tension du to twine around (which are circular) */

int tension_circular()
	{
	int no,np;
	double l0;
  	int zk,zl;
  	
	if (Codend.type == 3)
       {
  	   Codend.mf = Codend.mfc;
       }
 
  	
  	/*unstretched length*/
  	l0 = Codend.mf / 2.0;
    			
      	/*TEST_debut
	int zu,zm,zv;
	double delta_x, FF[100], MAT[100][100];
	delta_x = 0.0000001;
	for( zu = 0 ; zu <= 2*Codend.nbpt+1 ; zu++ )
      		{
      		if ((zu > 0) && (zu < 2*Codend.nbpt+1))
      			{
			if (zu > Codend.nbpt) 	
				{
 				printf("zu >  Codend.nbpt zu : %8d   \n",zu);
 				zm = zu-Codend.nbpt;
				Codend.r[zm] = Codend.r[zm] + delta_x;
				}
			else 			
				{
 				printf("zu <= Codend.nbpt zu : %8d   \n",zu);
				Codend.x[zu] = Codend.x[zu] + delta_x;
				}
			}
  		for (zl=1;zl<=Codend.nbpt;zl++)
  			{
  			Codend.fx[zl] = 0.0;
  			Codend.fr[zl] = 0.0;
  			}
  		for (zl=1;zl<=2*Codend.nbpt;zl++)
  			{
  			for (zm=1;zm<=2*Codend.nbpt;zm++)
  				{
				mat[zl][zm] = 0.0;
				}		
			}
      		TEST_fin*/    
      					
	/*twine tension*/
	for(no=1; no<=2*Codend.nbl;no++)
		{
		Codend.l_circ[no] = 2.0 * Codend.r[no] * sin(Codend.angle_circ/2.0);
		if (Codend.l_circ[no] > l0)
			{
			Codend.T_circ[no] = Codend.stf * (Codend.l_circ[no] - l0) / l0;
			}
		else
			{
			Codend.T_circ[no] = 0.0;
			}
		Codend.fx[no]   = Codend.fx[no]   -  0.0;
		Codend.fr[no]   = Codend.fr[no]   -  Codend.T_circ[no] * sin(Codend.angle_circ/2.0);
		}
  		
	/*affectation effort tension*/
  	for (zl=1;zl<=n_ligne;zl++)
  		{
  		zk = zl-Codend.nbpt;
  		if (zl>Codend.nbpt)	ff[zl] = Codend.fr[zk];
  		else			ff[zl] = Codend.fx[zl];
  		}
  		
  	/*derivee de ff*/
	for(no=1; no<=2*Codend.nbl;no++)
		{
		np = no+Codend.nbpt;
		if (Codend.l_circ[no] > l0)
			{
	  		mat[np  ][np  ] = mat[np  ][np  ] + 2.0 * Codend.stf * sin (Codend.angle_circ/2.0) * sin(Codend.angle_circ/2.0) / l0;
 			/*printf("mat[%3d][%3d]: %8.4lf  \n",np,np,mat[np  ][np  ]);*/
	  		}
  		}
  		
      	/*TEST_debut
		printf("zu %5d  \n",zu);
      		if (zu == 0)
      			{
			for (zl = 1; zl<= 2*Codend.nbpt; zl++)	FF[zl] = ff[zl];
      			}
      		if ((zu > 0) && (zu < 2*Codend.nbpt+1))
      			{
			if (zu > Codend.nbpt) 	
				{
 				zm = zu-Codend.nbpt;
				Codend.r[zm] = Codend.r[zm] - delta_x;
				}
			else 			
				{
				Codend.x[zu] = Codend.x[zu] - delta_x;
				}
			for (zm = 1 ;zm <= 2*Codend.nbpt ;zm++ )	MAT[zm][zu] = - (ff[zm] - FF[zm]) / delta_x;
      			}
      		if (zu == 2*Codend.nbpt+1)
      			{		
			for (zm = 1 ;zm <= 2*Codend.nbpt ;zm++ )
				{
				printf("mat = ");	for (zv = 1 ;zv <= 2*Codend.nbpt ;zv++ )	printf("%10.0lf ",mat[zm][zv]);	printf(" \n");
				printf("MAT = ");	for (zv = 1 ;zv <= 2*Codend.nbpt ;zv++ )	printf("%10.0lf ",MAT[zm][zv]);	printf(" \n");
				}
      			exit(0);
      			}
      		}
      		TEST_fin*/    		
	return 0;
	}	


