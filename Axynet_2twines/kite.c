#include "AXYnet.h"

void kite()
{
    int zi,nbiter;
    double mean,max_uu,stiff_add,objective;
    int volume();
    int catch_losange();
    
    initial();				/*type 4 : full kite mesh*/
    objective = 0.001;			//residue force threshold
    stiff_add = 1000000.0;
    nbiter = 0;
    do
    {
        nbiter = nbiter + 1;
        stiff_add = lectParam();
        /*TEST_debut
    	int zu,zm,zl,zv;
    	double delta_x,FF[100], MAT[100][100],mean,max_uu,stiff_add;
        delta_x = 0.000001;
        for( zu = 0 ; zu <= 2*Codend.nbpt+1 ; zu++ )
        {
        if ((zu > 0) && (zu < 2*Codend.nbpt+1))
        {
        if (zu > Codend.nbpt) 	
        {
        printf("zu >  Codend.nbpt zu : %8d   \n",zu);
        zm = zu-Codend.nbpt;
        Codend.r[zm] = Codend.r[zm] + delta_x;
        }
        else 			
        {
        printf("zu <= Codend.nbpt zu : %8d   \n",zu);
        Codend.x[zu] = Codend.x[zu] + delta_x;
        }
        }
        for (zl=1;zl<=Codend.nbpt;zl++)
        {
        Codend.fx[zl] = 0.0;
        Codend.fr[zl] = 0.0;
        }
        TEST_fin*/    
        miseazero();
        tension();			/*twine tension*/
        catch_losange();	/*pressure of the catch*/
        /*couple();			couple on knots*/
  	    /*for (zl=1;zl<=n_ligne;zl++)
  		    {
 		    printf("ff: %8.4lf  \n",ff[zl]);
  		    }*/
  		
        /*TEST_debut
        printf("zu %5d  \n",zu);
        if (zu == 0)
        {
        for (zl = 1; zl<= 2*Codend.nbpt; zl++)	FF[zl] = ff[zl];
        }
        if ((zu > 0) && (zu < 2*Codend.nbpt+1))
        {
        if (zu > Codend.nbpt) 	
        {
        zm = zu-Codend.nbpt;
        Codend.r[zm] = Codend.r[zm] - delta_x;
        }
        else 			
        {
        Codend.x[zu] = Codend.x[zu] - delta_x;
        }
        for (zm = 1 ;zm <= 2*Codend.nbpt ;zm++ )	MAT[zm][zu] = - (ff[zm] - FF[zm]) / delta_x;
        }
        if (zu == 2*Codend.nbpt+1)
        {		
        for (zm = 1 ;zm <= 2*Codend.nbpt ;zm++ )
        {
        printf("mat = ");	for (zv = 1 ;zv <= 6 ;zv++ )	printf("%10.0lf ",mat[zm][zv]);	printf(" \n");
        printf("MAT = ");	for (zv = 1 ;zv <= 6 ;zv++ )	printf("%10.0lf ",MAT[zm][zv]);	printf(" \n");
        }
        exit(0);
        }
        }
        TEST_fin*/    
        
        for (zi=1;zi<=2*Codend.nbpt;zi++)	mat[zi][zi] = mat[zi][zi] + stiff_add;
        
        /*for (zi=1;zi<=2*Codend.nbpt;zi++)
            {
            printf("zi: %5d  ff:%10.4lf \n",zi,ff[zi]);	                             
            }*/
        limites();			/*limites conditions*/	
        solver(n_ligne, mat, ff, uu);	/*solve the system*/	
        coordonnees();
        mean = 0.0;        for (zi=1;zi<=2*Codend.nbpt;zi++)	mean = mean + fabs(ff[zi]);//abs(ff[zi]);
        mean = mean / 2 / Codend.nbpt;
        max_uu = 0.0;      for (zi=1;zi<=2*Codend.nbpt;zi++)	if (max_uu < fabs(uu[zi])) max_uu = uu[zi];
        printf("nb_iteration %5d  Force residue (N) %15.4lf objective (N) %15.4lf \n",nbiter,mean,objective);
    }
    while (mean > 0.001 && nbiter < 300000) ;
    if (nbiter >= 300000)
    	{
        printf("You have reached the maximal number of iterations: 300000\n");
    	}
    volume();
}
