#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#if PRINCIPAL>0
#define CLASS	
#else
#define CLASS extern
#endif

#include "filwin.h"
#include "protos.h"

#define G 9.81                     /* pesanteur  */
#define PI 4.0 * atan(1.0)
#define RHO 1025                   	/* masse volumique de l eau  */
#define RHO_AIR 1.208                   /* masse volumique de l air  */
#define yb(zi,zj) (int)((Element[zi].nop[(zj+2)/3])*3+zj-3*((zj+2)/3))
#define yc(zi,zj) (int)((Surface[zi].nop[(zj+2)/3])*3+zj-3*((zj+2)/3))
#define yc_hexa(zi,zj) (int)((Surf_hexa[zi].nop[(zj+2)/3])*3+zj-3*((zj+2)/3))

typedef double TCOO1[DIM1];   
typedef double TCOO2[DIM2];
typedef double TMKB[7],TMKB3[10],Tab1[4];
typedef double TMK2[DIM1][2*DIM3];
typedef int    TLARGE1[DIM1];
typedef int    TLARGE2[DIM2];
typedef double TMKK[65][65],TMKC[10][10];

CLASS int    eca,nbiter,nbpas,Nbmaxiterations,Periodeimpression,largeur_matrice;
CLASS int    miseentension,lectsta;
CLASS int    rang[5][DIM1];
CLASS int    nb_u_tension,nb_v_tension,nb_pt_aval;

CLASS double RW,MW,RSUM,MSUM,RTEN,MTEN,RMEAN,MMEAN,tt,nu,Seuilconvergence,Relaxation;
CLASS double Deplacement,Numtemps,Numtemps_relax,Pascalcul,Passtockage,Debutstockage,Finstockage,deltat;
CLASS double X1,X2,X3,Y1,Y2,Y3,alpha,beta,iterationfin,precedent;
CLASS double alpha1,beta1,alpha2,bag,frontz;
CLASS double wg[7][7],NT[7],etat_converg[1001][5];
CLASS double double_temp;

CLASS TCOO1  wa,wasurf,waemodif,waecontact,waelem,wanoeud,wf,wh,wv,mh,mvh,mgh,wf1,wf2,mvb,mgb,wfcodend;
CLASS Tab1   ne,te;
CLASS TMKB   wd;
CLASS TMKB3  wae,ET,EN,ETuv,ENuv,ETlmn,ENlmn,Pe;
CLASS TMKC   kcontact,kmodif,we;
CLASS double **ze,*xze,*cze,**ze_modif,*xze_modif,*cze_modif,**ze_contact,*xze_contact,*cze_contact;
CLASS TLARGE1 fixation,fixa,whs,wl;
CLASS FILE   *f2;
