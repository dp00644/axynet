#define PRINCIPAL 0
#include "AXYnet.h"

/* volume behind the front of the diamond cod-end */

int volume()
	{
	int no;
	double tmp1,tmp2,tmp3,tmp4;
  	
  	Prise.volume = 0.0;
      				
	/*volume for each bar*/
	for(no=2; no<=2*Codend.nbl;no++)
		{
		if (Codend.x[no] > Prise.front)
			{
            /*volume for bars entirely behind the front*/
            tmp1 = (Codend.r[no+1]-Codend.r[no])/(Codend.x[no+1]-Codend.x[no]);
            tmp2 = (Codend.r[no]*Codend.r[no]+tmp1*tmp1*Codend.x[no]*Codend.x[no]-2*tmp1*Codend.x[no]*Codend.r[no]) * (Codend.x[no+1]-Codend.x[no]);
            tmp3 = (tmp1*Codend.r[no]-tmp1*tmp1*Codend.x[no])*(Codend.x[no+1]*Codend.x[no+1]-Codend.x[no]*Codend.x[no]);
            tmp4 = tmp1*tmp1*(Codend.x[no+1]*Codend.x[no+1]*Codend.x[no+1]-Codend.x[no]*Codend.x[no]*Codend.x[no])/3.0;
            Prise.volume = Prise.volume + PI * (tmp2 + tmp3 + tmp4);
  			}
		if ((Codend.x[no] <= Prise.front) && (Codend.x[no+1] > Prise.front))
			{
            /*volume for the bar cut by the front*/
            tmp1 = (Codend.r[no+1]-Codend.r[no])/(Codend.x[no+1]-Prise.front);
            tmp2 = (Codend.r[no]*Codend.r[no]+tmp1*tmp1*Prise.front*Prise.front-2*tmp1*Prise.front*Codend.r[no]) * (Codend.x[no+1]-Prise.front);
            tmp3 = (tmp1*Codend.r[no]-tmp1*tmp1*Prise.front)*(Codend.x[no+1]*Codend.x[no+1]-Prise.front*Prise.front);
            tmp4 = tmp1*tmp1*(Codend.x[no+1]*Codend.x[no+1]*Codend.x[no+1]-Prise.front*Prise.front*Prise.front)/3.0;
            Prise.volume = Prise.volume + PI * (tmp2 + tmp3 + tmp4);
  			}
		}
		
    /*printf("Prise.volume %lf  \n",Prise.volume);*/

	return 0;
	}	


