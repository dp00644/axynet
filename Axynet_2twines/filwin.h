#if PRINCIPAL>0
#define CLASS	
#else
#define CLASS extern
#endif

#define DIM1  7633             	/* taille tableau we,ye > 3*NOMBRE_NOEUDS           */
#define DIM2  4757            	/* taille tableau yb,wr >  NOMBRE_ELEMENTS & NOMBRE_SURFACES & NOMBRE_SURF_HEXA*/
#define DIM3  570             	/* taille tableau we,ye >= eca                      */

#define NBMAXNOEUD		20000
#define NBMAXTRIANGLE		5000
#define NBMAXTRIHEXA		5000
#define NBMAXPANNEAU		200
#define NBMAXPANHEXA		200
#define NBMAXELEMENT		2000	
#define NBMAXBARRE		5000
#define NBMAXTYPEBARRE		1900
#define NBMAXNOEUDCONTOUR	100
#define NBMAXNOEUDCOTE		100
#define NBMAXNOEUDINTERIEUR	20000
#define NBMAXLIAISON		1300
#define NBMAXTYPENOEUD		50

typedef struct 	codend {
	int    	type;		 	/*type of cod-end*/
	int  	nbr;			/*nb of mesh around*/
	int  	nbl;			/*nb of mesh along*/
	double 	mf;			/*full mesh size (m)*/
	double 	mfc;			/*full mesh size (m)*/
	double 	mfm;			/*full mesh size (m)*/
	double 	mi;			/*internal mesh size (m)*/
	double 	stf;			/*one twine stiffness (N)*/
	double 	EI;			/*one twine stiffness (N)*/
	int 	nbt;			/*1 if single twine, 2 if double twine*/
	double 	diam;			/*dimaeter of 1 twine (m)*/
	int 	sm;			/*twine nb in each selvedge*/
	double  wl;			/*size of the window in nb of diamond mesh along*/
	double  wr;			/*size of the window in nb of diamond mesh around*/
	double  distance;		/*distance of the window to the codline in nb of diamond mesh*/
	double  sql;			/*size of the window in nb of squarre mesh along*/
	double  sqr;			/*size of the window in nb of squarre mesh around*/
	double 	sqmf;			/*full mesh size of the quarre mesh (m)*/
	double 	radius;			/*radius of the entry (m)*/
	double 	angle;			/*angle of half mesh (rad)*/
	double 	drag;			/*drag of the catch along X axis (N)*/
	double 	angle_circ;		/*angle of a bar (rad)*/
	int 	nbpt;			/*nb of knots along = nbl + 1*/
	double 	*x;			/*x position of the node (m)*/
	double 	*r;			/*r position of the node (m)*/
	double 	*l;			/*length of the mesh bar (m)*/
	double 	*T;			/*tension in the mesh bar (N)*/
	double 	*l_circ;		/*length of the mesh bar (m) along the circunference*/
	double 	*T_circ;		/*tension in the mesh bar (N) along the circunference*/
	double 	*l_meri;		/*length of the mesh bar (m) along the meridian*/
	double 	*T_meri;		/*tension in the mesh bar (N) along the meridian*/
	double 	*fx;			/*force along x on node (N)*/
	double 	*fr;			/*force along r on node (N)*/
	} 	CODEND;

typedef struct 	element {
	int    	type;		 	/*no du type de l element*/
	int    	extremite[3];		/*no des 2 points d extremites*/
	double  pro[3];			/*position des 2 extremites en proportion de la longueur de l element de phobos*/
	double 	diametremeca;		/*diametre mecanique m^2*/
	double 	raideur_traction;	/*raideur en traction    (N) = section * module d elasticite*/
	double 	raideur_compression;	/*raideur en compression (N) = section * module d elasticite*/
	double 	raideur_effective;	/*raideur effective = raideur en traction (compression) si element est en traction (compression)*/
	double 	wt;			/*tension dans cet element (N)*/
	double 	wtfinal;		/*tension dans cet element (N) a l avant derniere iteration */
	double 	lgrepos;		/*longueur avec tension = 0 N*/
	double 	lgtendue;		/*longueur sous tension*/
	double 	diametrehydro;		/*diametre hydrodynamique m*/
	double 	rhoelement;		/*masse volumique kg/m^3*/
	double 	cdnormal;		/*coef. de trainee normal*/
	double 	ftangent;		/*coef. de trainee tangent*/
	int    	nb_barre;		/*nb bar in the cable*/
	int    	type_noeud;		/*type of node in the cable*/
	} 	ELEMENT;

typedef struct 	coulisse {
	int    	nop[3];			/*no des 2 points d extremites*/
	int    	nb_noeud;		/*nb de noeuds internes dans la coulisse en commencant par extremite 1*/
	int    	*noeud;			/*numeros des noeuds internes dans la coulisse en commencant par extremite 1*/
	double  *longueur;		/*distance entre noeuds en commencant par extremite 1*/
	double 	raideur_traction;	/*raideur en traction    (N) = section * module d elasticite*/
	double 	raideur_compression;	/*raideur en compression (N) = section * module d elasticite*/
	double 	raideur_effective;	/*raideur effective = raideur en traction (compression) si element est en traction (compression)*/
	double 	wt;			/*tension dans cet element (N)*/
	double 	wtfinal;		/*tension dans cet element (N) a l avant derniere iteration */
	double 	lgrepos;		/*longueur avec tension = 0 N = somme des longueurs*/
	double 	lgtendue;		/*longueur sous tension*/
	double 	diametrehydro;		/*diametre hydrodynamique m^2*/
	double 	rhocoulisse;		/*masse volumique kg/m^3*/
	double 	cdnormal;		/*coef. de trainee normal*/
	double 	ftangent;		/*coef. de trainee tangent*/
	} 	COULISSE;

typedef struct 	surface {
	int    	panneau;		/*no du panneau ou est situe ce triangle*/
	int    	type;		 	/*no du type de surface*/
	int    	nop[4];			/*no des 3 points aux sommets*/
	double 	lon[7];			/*coordonnees filaires des 3 sommets dans le sens des diagonales aux mailles*/
	double 	nb_cote_u_ou_v;		/*nb de cotes de mailles u ou v dans ce triangle : nb cote u = nb cotev*/
	double 	diametremeca;		/*diametre mecanique m^2*/		
	double 	raideur_traction;	/*raideur en traction    (N) = section * module d elasticite*/
	double 	raideur_compression;	/*raideur en compression (N) = section * module d elasticite*/
	double 	raideur_ouverture;	/*raideur a l ouverture des maille em N.m/rad*/
	double 	EI_flexion;		/*raideur a la flexion des fils em N.m.m*/
	double 	tension1;		/*tension dans les fils u (N)*/
	double 	tension2;		/*tension dans les fils v (N)*/
	double 	lgrepos;		/*longueur de la maille avec tension = 0 N*/
	double 	nx;			/*composante de la longueur du cote de maille u selon l axe x*/
	double 	ny;			/*composante de la longueur du cote de maille u selon l axe y*/
	double 	nz;			/*composante de la longueur du cote de maille u selon l axe z*/
	double 	mx;			/*composante de la longueur du cote de maille v selon l axe x*/
	double 	my;			/*composante de la longueur du cote de maille v selon l axe y*/
	double 	mz;			/*composante de la longueur du cote de maille v selon l axe z*/
	double 	diametrehydro;		/*diametre hydrodynamique m*/
	double 	largeurnoeud;		/*largeur du noeud  m*/
	double 	rhosurface;		/*masse volumique kg/m^3*/
	double 	cdnormal;		/*coef. de trainee normal*/
	double 	ftangent;		/*coef. de trainee tangent*/
	} 	SURFACE;

typedef struct 	surf_hexa {
	int    	type;		 	/*no du type de surface*/
	int    	nop[4];			/*no des 3 points aux sommets*/
	double 	lon[7];			/*coordonnees filaires des 3 sommets dans le sens des diagonales aux mailles*/
	double 	nb_cote_l_m_ou_n;	/*nb de cotes de mailles l, m ou n dans ce triangle : nb cote l = nb cote m = nb cote n*/
	double 	diametremeca;		/*diametre mecanique m^2*/		
	double 	raideur_traction;	/*raideur en traction    (N) = section * module d elasticite*/
	double 	raideur_compression;	/*raideur en compression (N) = section * module d elasticite*/
	double 	raideur_traction_l,raideur_traction_m,raideur_traction_n;		/*raideur en traction des fils constituant le filet de ce panneau*/
	double 	raideur_compression_l,raideur_compression_m,raideur_compression_n;/*raideur en compression des fils constituant le filet de ce panneau*/
	double 	tension1;		/*tension dans les fils U (N)*/
	double 	tension2;		/*tension dans les fils V (N)*/
	double 	lo_repos,mo_repos,no_repos;		/*cote de maille l,m,n du filet de ce panneau*/
	double 	lx,ly,lz;			/*composante de la longueur du cote de maille l selon l axe x*/
	double 	mx,my,mz;			/*composante de la longueur du cote de maille m selon l axe x*/
	double 	nx,ny,nz;			/*composante de la longueur du cote de maille n selon l axe x*/
	double 	diam_hydro_l,diam_hydro_m,diam_hydro_n;	/*diametre hydro des fils du filet de ce panneau*/
	double 	diametrehydro;		/*diametre hydrodynamique m*/
	double 	largeurnoeud;		/*largeur du noeud  m*/
	double 	rhosurface;		/*masse volumique kg/m^3*/
	double 	cdnormal;		/*coef. de trainee normal*/
	double 	ftangent;		/*coef. de trainee tangent*/
	} 	SURFACE_HEXA;

typedef struct 	noeud {
	int    	type;		 	/*no du type de noeud*/
	int    	new_numero;		/*renvoi le nouveau   numero de point apres renumerotation*/
	int    	old_numero;		/*renvoi le precedent numero de point apres renumerotation*/
	double 	U,V;			/*coordonnees filaires(meshes)*/
	double 	x,y,z;			/*coordonnees (m)*/
	double 	r;			/*radius coordonnees (m)*/
	double 	mx,my,mz;		/*masses (kg)*/
	double 	majx,majy,majz;		/*masses ajoutees ses (kg)*/
	double 	lonx,lony,lonz;		/*longueurs du parralellepipede (m)*/
	double 	cdx,cdy,cdz;		/*coef de trainee*/
	double 	fextx,fexty,fextz;	/*Effort exterieur (N)*/
	int    	senx,seny,senz;		/*Sens de la limite des deplacement du noeud si >0 limite inferieure*/
	double 	limx,limy,limz;		/*Limite des deplacement du noeud (m)*/
	int    	fixx,fixy,fixz;		/*autorisation de deplacement 0 : libre, 1 : fixe*/
	int    	symx,symy,symz;		/*Symetrie si 1, sinon 0*/
	} 	NOEUD; 
	
typedef struct 	houle {
	double 	hauteur;	/*hauteur de la houle (m)*/
	double 	periode;	/*periode (s)*/
	double 	direction;	/*direction dans le plasn horizontal relativement a l axe X (deg.)*/
	double 	lambda;		/*longueur d onde (m)*/
	double 	omega;		/*pulsation (rad/s)*/
	double 	k;		/*nombre d onde*/
	double 	depth;		/*profondeur du site (m)*/
	} 	HOULE;	

typedef struct 	courant {
	double 	vitesse;	/*amplitude (m/s)*/
	double 	direction;	/*direction dans le plan horizontal relativement a l axe X (deg.)*/
	} 	COURANT;	

typedef struct 	vent {
	double 	vitesse;	/*amplitude (m/s)*/
	double 	direction;	/*direction dans le plan horizontal relativement a l axe X (deg.)*/
	} 	VENT;	

typedef struct 	prise {
	double 	volume;			/*volume (m3)*/
	double 	seuil;			/*seuil (m3)*/
	double 	cd;			/*coef de trainee sur la prise*/
	double 	front;			/*position du front de la prise (m)*/
	double 	diametre;		/*diametre de la prise (m)*/
	double 	epaisseur;		/*distance du front a l extremite de la prise (m)*/
	double 	radial_radius;		/*radial radius of the ellipsoide inner surface (m)*/
	double 	axial_radius;		/*radial radius of the ellipsoide inner surface (m)*/
	double 	distance_front;		/*distance between the center of the ellipsoide and the front of the cod-end (m)*/
	int	nb_point;		/*nb of points in the perimeter of the front*/
	double	*perimeter_y;		/*y position of a point of the perimeter*/
	double	*perimeter_z;		/*z position of a point of the perimeter*/
	double	*catch;			/*catches*/
	double 	max_volume;		/*maximal volume available in the cod-end*/
	double 	PR;			/*prise en kg de poisson*/
	double 	PE;			/*perimetre tendu du cod-end*/
	double 	TA;			/*taille moyenne en kg des poisson*/
	double 	RA;			/*mesh opening stiffness N.m2*/
	double 	Ox;			/*x coordinate of the ellipsoide center*/
	double 	Oy;			/*y coordinate of the ellipsoide center*/
	double 	Oz;			/*z coordinate of the ellipsoide center*/
	} 	PRISE;	

typedef struct 	bottom {
	double 	coef_frottement;	/*coef de frottement sur le fond des noeuds : effort horizontal / effort vertical*/	
	double 	raideur;		/*raideur a l enfoncement des noeuds (N/m)*/
	} 	BOTTOM;	
	
typedef struct 	commentaire {
	char texte[156];
	} 	COMMENTAIRE;	
	


typedef struct sortie_texte
	{
	int  nb_distance;			/*nb de distance entre 2 points affichees*/
	COMMENTAIRE *comment_distance;		/*commentaires associes aux distances*/
	int *noeud1_distance;			/*premiere extremite de la distance en global*/		
	int *noeud2_distance;			/*seconde extremite de la distance en global*/		
	int *decimale_distance;			/*nb de decimale pour l affichage de la distance*/
			
	int  nb_effort;				/*nb d effort selon un axe a un point affiches*/
	COMMENTAIRE *comment_effort;		/*commentaires associes aux distances*/
	int *noeud_effort;			/*premiere extremite de la distance en global*/		
	int *axe_effort;			/*seconde extremite de la distance*/		
	int *decimale_effort;			/*nb de decimale pour l affichage de la distance*/

	int  nb_tension;			/*nb de tension dans un element affichees*/
	COMMENTAIRE *comment_tension;		/*commentaires associes aux distances*/
	int *element_tension;			/*premiere extremite de la distance*/		
	int *element_extremite;			/*premiere extremite de la distance en local : 1 ou 2*/		
	int *decimale_tension;			/*nb de decimale pour l affichage de la distance*/

	int  nb_tension_coulisse;		/*nb de tension dans une coulisse affichees*/
	COMMENTAIRE *comment_tension_coulisse;	/*commentaires associes aux distances*/
	int *coulisse_tension;			/*premiere extremite de la distance*/		
	int *coulisse_extremite;		/*premiere extremite de la distance*/		
	int *decimale_coulisse;			/*nb de decimale pour l affichage de la distance*/		

	int  nb_position;			/*nb de position selon un axe a un point affichees*/
	COMMENTAIRE *comment_position;		/*commentaires associes aux distances*/
	int *noeud_position;			/*premiere extremite de la distance*/		
	int *axe_position;			/*seconde extremite de la distance*/		
	int *decimale_position;			/*nb de decimale pour l affichage de la distance*/
	
	int  nb_longueur;			/*nb de longueur de barres affichees*/
	COMMENTAIRE *comment_longueur;		/*commentaires associes aux longueurs*/
	int *element_longueur;			/*numero de l element*/		
	int *decimale_longueur;			/*nb de decimale pour l affichage de la longueur*/
	
	int  nb_longueur_cable;			/*nb de longueur de cables affichees*/
	COMMENTAIRE *comment_longueur_cable;	/*commentaires associes aux longueurs*/
	int *element_longueur_cable;		/*numero du cable*/		
	int *decimale_longueur_cable;		/*nb de decimale pour l affichage de la longueur en m*/
	
	int  nb_parametre;			/*nb de position selon un axe a un point affichees*/
	COMMENTAIRE *comment_parametre;		/*commentaires associes aux distances*/
	int *decimale_parametre;		/*nb de decimale pour l affichage de la distance*/

	int effort_structure;			/*si 1 affiche l effort sur toute la structure (N) selon les axes X Y et Z*/	
	int diametre_prise;			/*si 1 affiche diametre_prise (m)*/	
	int epaisseur_prise;			/*si 1 affiche epaisseur_prise (m)*/	
	int vitesse_courant;			/*si 1 affiche vitesse_courant (m/s)*/	
	int vitesse_vent;			/*si 1 affiche vitesse_vent (m/s)*/	
	int volume_capture;			/*si 1 affiche volume_capture (m3)*/	
	int surface_filtree;			/*si 1 affiche surface_filtree (m2)*/	
	int volume_x;				/*si 1 affiche le voulume calcule selon l axe x (m3)*/	
	int volume_y;				/*si 1 affiche le voulume calcule selon l axe y (m3)*/	
	int volume_z;				/*si 1 affiche le voulume calcule selon l axe z (m3)*/	
	int volume_0;				/*si 1 affiche le voulume calcule selon le courant (m3)*/	
	
	} SORTIE_TEXTE;

typedef struct panneau
	{
	int nb_noeud_contour; 				/*nb de noeuds du contour du panneau*/
	int nb_noeud_interieur; 			/*nb de noeuds interieur au panneau*/
	int nb_noeud_cote; 				/*nb de noeuds des cotes du panneau*/
	int *noeud_contour;				/*liste ordonnee des indices des noeuds du contour*/
	int *suivant_contour;				/*noeuds de cote qui suivent les noeuds du contour*/
	int *type_suivant_contour;			/*type des noeuds de cote qui suivent les noeuds du contour*/
	int *noeud_cote;				/*liste des indices des noeuds des cotes*/
	float *prop_cote;				/*liste des positions relatives des noeuds des cotes / longueur du cote*/
	int *noeud_interieur;				/*liste des indices des noeuds interieurs*/
	int *numero_triangle;				/*liste des indices des noeuds interieurs*/
	int nb_triangle_interieur; 			/*nb de triangles interieurs*/
	int triangle_interieur[NBMAXNOEUDINTERIEUR][4];	/*sommets des triangles interieurs dans la numerotation totale*/
	int nb_triangle_contour; 			/*nb de triangles poses sur le contour*/
	int triangle_contour[NBMAXNOEUDCONTOUR][4];	/*sommets des triangles poses sur le contour dans la numerotation locale*/
	float pas_maillage;				/*pas du maillage des noeuds interieurs*/
	int type_maillage;				/*type du maillage*/
	int flag_maillage;				/*1 si maille 0 si pas maille*/
	int flag_filet_contour;				/*1 si la triangulation sur le contour    est calculee 0 sinon*/
	int flag_filet;					/*1 si la triangulation sur ts les points est calculee 0 sinon*/
	float raideur_traction;				/*raideur en traction des fils constituant le filet de ce panneau*/
	float raideur_compression;			/*raideur en compression des fils constituant le filet de ce panneau*/
	float raideur_ouverture;			/*raideur a l ouverture des mailles du filet de ce panneau*/
	float EI_flexion;				/*raideur a la flexion des fils em N.m.m*/
	float longueur_repos;				/*cote de maille du filet de ce panneau*/
	float diam_hydro;				/*diametre hydro des fils du filet de ce panneau*/
	float largeurnoeud;				/*largeur des noeuds du filet de ce panneau*/
	float rho;					/*masse volumique des fils constituant le filet de ce panneau*/
	float cdnormal;					/*coef de trainee normale des fils constituant le filet de ce panneau*/
	float ftangent;					/*coef de trainee tangentielle des fils constituant le filet de ce panneau*/
	float surf_fils;				/*surface de fils de ce panneau*/
	int type_noeud;					/*type des noeud a l interieur du panneau*/
	} PANNEAU;
	
typedef struct lien
	{
	int  nb_liaison;		/*nb de noeud utilisant cette liaison*/
	char *type; 			/*types des structures liees*/
	int  *structure; 		/*indice des structures liees*/
	int  *extremite; 		/*indice des noeuds lies (numerotation locale)*/
	int  *noeud; 			/*indice des coins lies (numerotation totale)*/
	} LIEN;

typedef struct chalut
	{
	int nb_ordre_objet;             /*nombre d'objets(panneau,element,coulisse,pan_hexa)*/
	int nb_fils; 			/*nombre de fils dans les triangles contour constituants le chalut*/
	int nb_panneau; 		/*nombre de panneaux a maille losange constituants le chalut*/
	int nb_pan_hexa; 		/*nombre de panneaux a maille hexagonale constituants le chalut*/
	int nb_element; 		/*nombre de elements constituants le chalut*/
	int nb_coulisse; 		/*nombre de coulisses constituants le chalut*/
	int nb_liaison; 		/*nombre de liaisons dans le chalut*/
	int nb_lien; 			/*nombre de liens dans le chalut >=  liaisons*/
	int nb_total;			/*nombre de noeuds dans la numerotation totale dans le chalut*/
	int nb_global;			/*nombre de noeuds dans la numerotation globale dans le chalut apres renumerotation*/
	int nb_type_noeud;		/*nombre de type de noeud dans le chalut*/
	int nb_barre;			/*nombre de barre dans le chalut*/
	int nb_surface;			/*nombre de surface dans le chalut*/
	int nb_surf_hexa;		/*nombre de surface a maille hexagonale dans le chalut*/
	float surface_fils;		/*surface de fils du chalut tel que maille*/
	int orientation;		/*orientation du dessin 1 (2,3) perpendiculaire a l axe x (y,z)*/
	int nb_catches;			/*nb of catches calculated*/
	} CHALUT;
	
typedef struct filiere
   	{
   	int       nombrePoints;
   	int       nombreNoeuds;
   	int	  nombreElements;
   	int	  nombreCoulisses;
   	int	  nombreSurfaces;
   	int	  nombreSurfHexa;
   	int	  NbTypeNoeud;
   	int	  NbTypeelem;
   	int	  NbTypesurf;
   	int	  NbTypesurf_hexa;
   	double    diviseur;
   	int       lmn_calcules;			/*= 1 si l m n des mailles hexagonales ont ete calcules sinon 0*/
   	} 	FILIERE;


typedef struct ordre
	{
	char *type;
	int  *indice;
	} ORDRE;
	
typedef struct numerique {
	float DIVISEUR;			/*raideur additionnelle en N*/
	int Nbmaxiterations;		/*nb max d iterations realisees dans unix*/
	float Seuilconvergence;		/*seuil de convergence en N*/
	float Deplacement;		/*deplacement maximal autorise en m*/
	float Pascalcul;		/*pas de calcul en dynamique en s*/
	float Passtockage;		/*pas de stockage en dynamique en s*/
	float Debutstockage;		/*debut du stockage en dynamique en s*/
	float Finstockage;		/*fin du stockage et du calculen dynamique en s*/
	} NUMERIQUE;
	
double **mat,*xmat,*cmat,*ff,*uu;
int n_ligne;
	
/**** variables globales ****/

CLASS FILIERE   		Filiere; 
CLASS NOEUD     		Noeud[DIM1],TypeNoeud[DIM1];
CLASS ELEMENT   		Element[DIM2],TypeElement[DIM2]; 
CLASS COULISSE  		Coulisse[DIM2];
CLASS SURFACE   		Surface[DIM2],TypeSurface[DIM2]; 
CLASS SURFACE_HEXA   		Surf_hexa[DIM2],TypeSurf_hexa[DIM2]; 
CLASS HOULE     		Houle;
CLASS COURANT   		Courant;
CLASS VENT   			Vent;
CLASS PRISE     		Prise;
CLASS BOTTOM	  		Bottom;
CLASS SORTIE_TEXTE	  	Sortie_texte;
CLASS CODEND			Codend;
CLASS CHALUT			Chalut;
CLASS PANNEAU   		Panneau[NBMAXPANNEAU]; 
CLASS LIEN	   		Lien[NBMAXLIAISON]; 
CLASS ORDRE			Ordre;
CLASS NUMERIQUE			Numerique;

/* "define" 
#define NOMBRE_NOEUDS   	Filiere.nombreNoeuds
#define NOMBRE_ELEMENTS 	Filiere.nombreElements
#define NOMBRE_COULISSES 	Filiere.nombreCoulisses
#define NOMBRE_SURFACES 	Filiere.nombreSurfaces
#define NOMBRE_SURF_HEXA 	Filiere.nombreSurfHexa*/

