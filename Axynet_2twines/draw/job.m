clear

direct1 = ['../../Axynet_2twines/']; %path of *.dat and *.sha files
nom1 =  ['busan_1'];              %name of the dat and sha files
nom1 =  ['square_2'];              %name of the dat and sha files
nom1 =  ['rect_3'];              %name of the dat and sha files
nom1 =  ['kite_4'];              %name of the dat and sha files
nom1 =  ['busan_1'];              %name of the dat and sha files
nom1 =  ['square_2'];              %name of the dat and sha files
nom1 =  ['rect_3'];              %name of the dat and sha files
nom1 =  ['square_9'];              %name of the dat and sha files
lectdat;                          %reading of the characteristric of the cod-end
lectsha;                          %reading of the shape of the cod-end

plot(x,r,'b');
hold on;
plot(x,-r,'b');
fr_x = [ front;   front;];
fr_r = [max(r); -max(r);];
plot(fr_x,fr_r,'r');
%axis ("equal");
%hold off;

if typ == 1
  %diamond meshes
  nb_p = max(size(x)); %nb of points
  teta = 2 * pi / nbr / 2; %angle for 1/2 mesh around
  %front
  xf = [ front  front   front   front   front];
  yf = [ max(r) max(r) -max(r) -max(r)  max(r)];
  plot(xf,yf,'r');
  hold on
  for nn = 1:2:nb_p,
      gamma(nn) = 0;
  endfor
  for nn = 2:2:nb_p,
      gamma(nn) = teta;
  endfor
  y = sin(gamma).*r;
  z = cos(gamma).*r;
  for nn = 1:nbr,
      beta = (nn-1)*2*teta;
      xa = x;
      ya = sin(beta+gamma).*r;
      plot(xa,ya,'b');
      xa = x;
      ya = sin(beta-gamma).*r;
      plot(xa,ya,'b');
  endfor
  plot(fr_x,fr_r,'r');
  hold off;
endif

if typ == 2
  %square meshes
  nb_p = max(size(x)); %nb of points
  teta = 2 * pi / nbr / 2; %angle for 1/2 mesh around
  %front
  xf = [ front  front   front   front   front];
  yf = [ max(r) max(r) -max(r) -max(r)  max(r)];
  plot(xf,yf,'r');
  hold on
  for nn = 1:nbr,
      beta = (nn-1)*2*teta;
      xa = x;
      ya = sin(beta).*r;
      plot(xa,ya,'b');
  endfor
  for nn = 1 : nb_p
    xa = [ x(nn) x(nn)];
    ya = [ -r(nn) r(nn)];
    plot(xa,ya,'b');
  endfor
  plot(fr_x,fr_r,'r');
  hold off;
endif

if typ == 3
  %rectangle meshes
  nb_p = max(size(x)); %nb of points
  teta = 2 * pi / nbr / 2; %angle for 1/2 mesh around
  %front
  xf = [ front  front   front   front   front];
  yf = [ max(r) max(r) -max(r) -max(r)  max(r)];
  plot(xf,yf,'r');
  hold on
  for nn = 1:nbr,
      beta = (nn-1)*2*teta;
      xa = x;
      ya = sin(beta).*r;
      plot(xa,ya,'b');
  endfor
  for nn = 1 : nb_p
    xa = [ x(nn) x(nn)];
    ya = [ -r(nn) r(nn)];
    plot(xa,ya,'b');
  endfor
  plot(fr_x,fr_r,'r');
  hold off;
endif

if typ == 4
  %kite meshes
  nb_p = max(size(x)); %nb of points
  teta = 2 * pi / nbr / 2; %angle for 1/2 mesh around
  %front
  xf = [ front  front   front   front   front];
  yf = [ max(r) max(r) -max(r) -max(r)  max(r)];
  plot(xf,yf,'r');
  hold on
  for nn = 1:2:nb_p,
      gamma(nn) = 0;
  endfor
  for nn = 2:2:nb_p,
      gamma(nn) = teta;
  endfor
  y = sin(gamma).*r;
  z = cos(gamma).*r;
  for nn = 1:nbr,
      beta = (nn-1)*2*teta;
      xa = x;
      ya = sin(beta+gamma).*r;
      plot(xa,ya,'b');
      xa = x;
      ya = sin(beta-gamma).*r;
      plot(xa,ya,'b');
  endfor
  plot(fr_x,fr_r,'r');
  hold off;
endif

%print -dtiff busan_1




