fich = [direct1, nom1 ,'.dat'];

fid = fopen(fich,'r');
  [tmp,m2] = fscanf(fid,'%s',1);
  [typ,m2] = fscanf(fid,'%d',1);
  if typ == 1
    %diamond meshes
    [tmp,m2] = fscanf(fid,'%s',1);    [nbr,m2] = fscanf(fid,'%d',1);
    [tmp,m2] = fscanf(fid,'%s',1);    [nbl,m2] = fscanf(fid,'%d',1);
    [tmp,m2] = fscanf(fid,'%s',1);    [mf,m2] = fscanf(fid,'%f',1);
    [tmp,m2] = fscanf(fid,'%s',1);    [stf,m2] = fscanf(fid,'%f',1);
    [tmp,m2] = fscanf(fid,'%s',1);    [lim,m2] = fscanf(fid,'%f',1);
    [tmp,m2] = fscanf(fid,'%s',1);    [radius,m2] = fscanf(fid,'%f',1);
    [tmp,m2] = fscanf(fid,'%s',1);    [speed,m2] = fscanf(fid,'%f',1);
  endif
  if typ == 2
    %square meshes
    [tmp,m2] = fscanf(fid,'%s',1);    [nbr,m2] = fscanf(fid,'%d',1);
    [tmp,m2] = fscanf(fid,'%s',1);    [nbl,m2] = fscanf(fid,'%d',1);
    [tmp,m2] = fscanf(fid,'%s',1);    [mf,m2] = fscanf(fid,'%f',1);
    [tmp,m2] = fscanf(fid,'%s',1);    [stf,m2] = fscanf(fid,'%f',1);
    [tmp,m2] = fscanf(fid,'%s',1);    [lim,m2] = fscanf(fid,'%f',1);
    [tmp,m2] = fscanf(fid,'%s',1);    [radius,m2] = fscanf(fid,'%f',1);
    [tmp,m2] = fscanf(fid,'%s',1);    [speed,m2] = fscanf(fid,'%f',1);
  endif
  if typ == 3
    %rectangle meshes
    [tmp,m2] = fscanf(fid,'%s',1);    [nbr,m2] = fscanf(fid,'%d',1);
    [tmp,m2] = fscanf(fid,'%s',1);    [nbl,m2] = fscanf(fid,'%d',1);
    [tmp,m2] = fscanf(fid,'%s',1);    [mfc,m2] = fscanf(fid,'%f',1);
    [tmp,m2] = fscanf(fid,'%s',1);    [mfm,m2] = fscanf(fid,'%f',1);
    [tmp,m2] = fscanf(fid,'%s',1);    [stf,m2] = fscanf(fid,'%f',1);
    [tmp,m2] = fscanf(fid,'%s',1);    [lim,m2] = fscanf(fid,'%f',1);
    [tmp,m2] = fscanf(fid,'%s',1);    [radius,m2] = fscanf(fid,'%f',1);
    [tmp,m2] = fscanf(fid,'%s',1);    [speed,m2] = fscanf(fid,'%f',1);
  endif
  if typ == 4
    %kite meshes
    [tmp,m2] = fscanf(fid,'%s',1);    [nbr,m2] = fscanf(fid,'%d',1);
    [tmp,m2] = fscanf(fid,'%s',1);    [nbl,m2] = fscanf(fid,'%d',1);
    [tmp,m2] = fscanf(fid,'%s',1);    [mfc,m2] = fscanf(fid,'%f',1);
    [tmp,m2] = fscanf(fid,'%s',1);    [mfm,m2] = fscanf(fid,'%f',1);
    [tmp,m2] = fscanf(fid,'%s',1);    [stf,m2] = fscanf(fid,'%f',1);
    [tmp,m2] = fscanf(fid,'%s',1);    [lim,m2] = fscanf(fid,'%f',1);
    [tmp,m2] = fscanf(fid,'%s',1);    [radius,m2] = fscanf(fid,'%f',1);
    [tmp,m2] = fscanf(fid,'%s',1);    [speed,m2] = fscanf(fid,'%f',1);
  endif
fclose(fid);
