clear;

name="../REST0.txt";
fd1=fopen(name,"r");
    [tmp,m2] = fscanf(fd1,'%s',1);    [nbl,m2] = fscanf(fd1,'%d',1);
    [tmp,m2] = fscanf(fd1,'%s',1);    [npr,m2] = fscanf(fd1,'%d',1);
    [tmp,m2] = fscanf(fd1,'%s',1);    [nbr,m2] = fscanf(fd1,'%d',1);
    [tmp,m2] = fscanf(fd1,'%s',1);    [p0,m2] = fscanf(fd1,'%f',1);
    [tmp,m2] = fscanf(fd1,'%s',1);    [q0,m2] = fscanf(fd1,'%f',1);
    [tmp,m2] = fscanf(fd1,'%s',1);    [r0,m2] = fscanf(fd1,'%f',1);
    [tmp,m2] = fscanf(fd1,'%s',1);    [s0,m2] = fscanf(fd1,'%f',1);
    [tmp,m2] = fscanf(fd1,'%s',1);    [kl,m2] = fscanf(fd1,'%f',1);
    [tmp,m2] = fscanf(fd1,'%s',1);    [km,m2] = fscanf(fd1,'%f',1);
    [tmp,m2] = fscanf(fd1,'%s',1);    [v,m2] = fscanf(fd1,'%f',1);
    [tmp,m2] = fscanf(fd1,'%s',1);    [ro,m2] = fscanf(fd1,'%f',1);
    [tmp,m2] = fscanf(fd1,'%s',1);    [stiff_add,m2] = fscanf(fd1,'%f',1);
    [tmp,m2] = fscanf(fd1,'%s',1);    [seuil,m2] = fscanf(fd1,'%f',1);
    [tmp,m2] = fscanf(fd1,'%s',6);    [x_react,m2] = fscanf(fd1,'%f',1);
    [tmp,m2] = fscanf(fd1,'%s',4);    [drag,m2] = fscanf(fd1,'%f',1);
    [tmp,m2] = fscanf(fd1,'%s',5);    [volume,m2] = fscanf(fd1,'%f',1);
    [tmp,m2] = fscanf(fd1,'%s',5);    [catch_radius,m2] = fscanf(fd1,'%f',1);
    [tmp,m2] = fscanf(fd1,'%s',5);    [catch_thickness,m2] = fscanf(fd1,'%f',1);
    [tmp,m2] = fscanf(fd1,'%s',5);    [catch_length,m2] = fscanf(fd1,'%f',1);
    [tmp,m2] = fscanf(fd1,'%s',4);      
    x = 0*ones(4*nbl+1,1);
    r = 0*ones(4*nbl+1,1);
    for zc = 1:4*nbl+1,
         [x(zc),m2] = fscanf(fd1,'%f',1);
         [r(zc),m2] = fscanf(fd1,'%f',1);
    end
fclose (fd1);

npp=4*nbl+1-4*npr;
teta=pi/nbr;

xx=[x(npp) x(npp) x(npp) x(npp) x(npp)];
yy=[max(r) max(r) -max(r) -max(r) max(r) ];
zz=[max(r) -max(r) -max(r) max(r) max(r) ];
%plot3(xx,yy,zz); %% limit of the catch
plot(xx,zz,'r'); %% limit of the catch
%axis equal;
hold on


for j=0:2*nbr
    %y
    y=zeros(4*nbl+1,1);
    y(1)=ro*sin((2*j)*teta);
    for i=1:nbl
       y(4*i-2)=r(4*i-2)*sin((2*j)*teta);
       y(4*i-1)=r(4*i-1)*sin((2*j+1)*teta);
       y(4*i)=r(4*i)*sin((2*j+1)*teta);
       y(4*i+1)=r(4*i+1)*sin((2*j)*teta);;
    end
    %z
    z=zeros(4*nbl+1,1);
    z(1)=ro*cos((2*j)*teta);
    for i=1:nbl
       z(4*i-2)= r(4*i-2)*cos((2*j)*teta);;
        z(4*i-1)=r(4*i-1)*cos((2*j+1)*teta);
       z(4*i)=r(4*i)*cos((2*j+1)*teta);
       z(4*i+1)=r(4*i+1)*cos((2*j)*teta);
    end
    %plot3(x,y,z) %% 1st axial line for the rotation
    plot(x,z,'b') %% 1st axial line for the rotation
    hold on
end


for j=1:2*nbr
    %y1
    y1=zeros(4*nbl+1,1);
    y1(1)=ro*sin((2*j)*teta);
    for i=1:nbl
       y1(4*i-2)=r(4*i-2)*sin((2*j)*teta);
       y1(4*i-1)=r(4*i-1)*sin((2*j-1)*teta);
       y1(4*i)=r(4*i)*sin((2*j-1)*teta);
       y1(4*i+1)=r(4*i+1)*sin((2*j)*teta);
    end
    %z1
    z1=zeros(4*nbl+1,1);
    z1(1)=ro*cos((2*j)*teta);
    for i=1:nbl
       z1(4*i-2)= r(4*i-2)*cos((2*j)*teta);;
        z1(4*i-1)=r(4*i-1)*cos((2*j-1)*teta);
       z1(4*i)=r(4*i)*cos((2*j-1)*teta);
       z1(4*i+1)=r(4*i+1)*cos((2*j)*teta);
    end
    %plot3(x,y1,z1); % 2d axial line for the rotation
    plot(x,z1,'b'); % 2d axial line for the rotation
    hold on;
end

title(['volume behind the front: ',num2str(volume),' m³']);
%axis([min(x) max(x) -max(r) max(r)], 'equal');
%axis equal;
hold off;
