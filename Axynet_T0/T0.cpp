#include<iostream>
#include<math.h>
#include <fstream>
using namespace std;

ifstream inputFile;


  
int main()
	{
	int nbl=8;							// nombre de mailles en long   
	int npr=7;							// nombre de mailles en long contenant du poisson
	int nbr=100;							// nombre de mailles autour
	double p0=0.05;double q0=0.05;double r0=0.05;double s0=0.05;	// les longueurs au repos
	double kl=200000;double km=200000;				// raideur du fils
	double v=1.9;							// la vitesse de l'eau (m/s)
	double  ro=0.2;							// rayon de l'entr�e (m)

	double stiff_add = 100;						// additional stiffness (N)
	double  seuil = 100000000.00001;				// convergence limit (N)


	/*
	//
	//
	//                  _______
	//                 /  q0   \
	//              p0/         \r0
	//       ________/           \________     -> current
	//               \           /    s0
	//                \         /
	//                 \_______/
	//
	//
	*/

	char chr = ' ';
	inputFile.open("A0.dat"); //ios::in

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> nbl;   //cout << "nbl:" << nbl << endl;

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> npr;   //cout << "npr:" << npr << endl;

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> nbr;   //cout << "nbr:" << nbr << endl;

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> p0;   //cout << "p0:" << p0 << endl;

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> q0;   //cout << "q0:" << q0 << endl;

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> r0;   //cout << "r0:" << r0 << endl;

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> s0;   //cout << "s0:" << s0 << endl;

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> kl;   cout << "kl:" << kl << endl;

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> km;   cout << "km:" << km << endl;

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> v;   cout << "v:" << v << endl;

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> ro;   cout << "ro:" << ro << endl;
	
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> stiff_add;   cout << "stiff_add:" << stiff_add << endl;

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> seuil;   cout << "seuil:" << seuil << endl;
	
	inputFile.close();
	            
	const double pi =3.14159265359;					// nombre Pi  
	double  teta=pi/nbr;						//angle entre 2 plans radiaux
	double  co=cos(teta);
	double  si=sin(teta);
	int npp=4*nbl+1-4*npr;						// num�ro du point indiquant la limite de la prise de poisson
	double P= 0.5 *1.4* v*v  *1025.0;				// La pression du poisson 


	// ------ ----------------- POSITION INITIALE------------------------------------------------------------- 
	double *X;
	int taille=8*nbl+2;     //la taille du vecteur X
	X = new double[taille];
	int i,j;
	double mean_side;
	mean_side = (p0+q0+r0+s0)/4;
	for ( i = 0; i < taille/2; i++)
		{   
		X[2*i]=mean_side*(double)i;
		X[2*i+1]=ro;    
		}
	X[taille-1]=0;
	int compteur_aff=0;
	// d�claration des vecteurs de force
	double *FT1, *FT2, *FT3, *FT4, *FP, *h;
	double *F;
	FT1 = new double[taille];
	FT2 = new double[taille];
	FT3 = new double[taille];
	FT4 = new double[taille];
	FP  = new double[taille];
	F   = new double[taille];
	h   = new double[taille];
	for ( i=0; i < taille; i++)
		{
		FT1[i]=0;
		FT2[i]=0;
		FT3[i]=0;
		FT4[i]=0;
		FP[i]=0;
		F[i]=0;
		h[i]=0;
		}

	// d�claration de la matrice de raideur 
	double **dFTdX;double **dFPdX;double **dFdX;
	dFTdX = new double* [taille ];//pour les  tensions
	dFPdX = new double* [taille ];// pour la pression du poisson
	dFdX  = new double* [taille ];// matrice de raideur totale
	for ( i=0; i < taille; i++)  
		{
		dFPdX[i] = new double[taille ];
		dFTdX[i] = new double[taille ];
		dFdX[i] = new double[taille ];
		}
	for ( i=0; i < taille; i++)
		for ( j=0; j < taille; j++)
			{
			dFTdX[i][j]=0;
			dFPdX[i][j]=0;
			dFdX[i][j]=0;
			}
	// les variables utiles en simplification 
	double a1,a2,a3,a4,a5,a6,l10,l11;
	double b1,b2,b3,b4,b5,b6,l21,l22;
	double c1,c2,c3,c4,c5,c6,l33;
	double d1,d2,d3,d4,d5,d6,l44;
	double tmp;

	double ** a  = new double* [taille];
	for ( i=0; i < taille; i++)  
		{
		a[i] = new double[taille ];
		}
	double * b;b=new double[taille];
	double ** A  = new double* [taille];
	for ( i=0; i < taille; i++)  
		{
		A[i] = new double[taille +1];
		}

	// La proc�dure de calcul
	double ref=2*seuil;// residu de F
	while (ref>seuil)
		{
		
///////////////MODIFICATION ADDED STIFFNESS/////////////////////////

	inputFile.open("A0.dat"); //ios::in

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> tmp;   //cout << "nbl:" << nbl << endl;

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> tmp;   //cout << "npr:" << npr << endl;

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> tmp;   //cout << "nbr:" << nbr << endl;

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> tmp;   //cout << "p0:" << p0 << endl;

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> tmp;   //cout << "q0:" << q0 << endl;

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> tmp;   //cout << "r0:" << r0 << endl;

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> tmp;   //cout << "s0:" << s0 << endl;

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> kl;   //cout << "kl:" << kl << endl;

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> km;   //cout << "km:" << km << endl;

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> v;   //cout << "v:" << v << endl;

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> tmp;   //cout << "ro:" << ro << endl;
	
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> stiff_add;   //cout << "stiff_add:" << stiff_add << endl;

	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> chr;    //cout << "chr:" << chr << endl;
	inputFile >> seuil;   //cout << "seuil:" << seuil << endl;
	
	inputFile.close();
///////////////MODIFICATION ADDED STIFFNESS/////////////////////////



		// ------ ----------------- la tension s'appliquant sur point i_1-------------------------------------------------------------



		for ( i=0;i<nbl;i++)
		{
		a1=X[8*i+0];
		a2=X[8*i+1];
		a3=X[8*i+2];
		a4=X[8*i+3];
		a5=X[8*i+4];
		a6=X[8*i+5];
		l10=sqrt( pow((a3-a1),2)+pow((a4-a2),2));
		if (l10<s0)
		l10=s0;
		FT1[8*i+2]=-km*((l10-s0))/s0/l10*(a3-a1);
		FT1[8*i+3]=-km*((l10-s0))/s0/l10*(a4-a2); 
		l11=sqrt(pow((a5-a3),2)+pow((a6*si),2)+pow((a6*co-a4),2));
		if (l11<p0)
		l11=p0;
		FT1[8*i+2]=FT1[8*i+2]+2*kl*(l11-p0)/p0/l11*(a5-a3);// ajout de  la composante axiale de la tension dans le fils apr�s point i_1
		FT1[8*i+3]=FT1[8*i+3]+2*kl*(l11-p0)/p0/l11*(a6*co-a4);//%+ la composante axiale de la tension dans le fils apr�s point i_1

		//Matrice de raideur sur point i_1

		dFTdX[8*i+2][8*i+0]=(km*(sqrt(pow((a4-a2),2)+pow((a3-a1),2))-s0))/(sqrt(pow((a4-a2),2)+pow((a3-a1),2))*s0)-(pow((a3-a1),2)*km*(sqrt(pow((a4-a2),2)+pow((a3-a1),2))-s0))/(pow(l10,3)*s0)+(pow((a3-a1),2)*km)/(pow(l10,2)*s0);
		dFTdX[8*i+2][8*i+1]=(km*(a4-a2)*(a3-a1))/(s0*(pow((a3-a1),2)+pow((a4-a2),2)))-(km*(a4-a2)*(l10-s0)*(a3-a1))/(s0*((pow(l10,3))));
		dFTdX[8*i+2][8*i+2]=-(km*pow((a3-a1),2))/(s0*(pow((a3-a1),2)+pow((a4-a2),2)))+(km*(l10-s0)*pow((a3-a1),2))/(s0*((pow(l10,3))))-(km*(l10-s0))/(s0*l10)-(2*kl*pow((a5-a3),2))/(p0*pow(l11,2))+(2*kl*(l11-p0)*pow((a5-a3),2))/(p0*pow(l11,3))-(2*kl*(l11-p0))/(p0*l11);
		dFTdX[8*i+2][8*i+3]= -(km*(a4-a2)*(a3-a1))/(s0*(pow((a3-a1),2)+pow((a4-a2),2)))+(km*(a4-a2)*(l10-s0)*(a3-a1))/(s0*((pow(l10,3))))-(2*kl*(co*a6-a4)*(a5-a3))/(p0*pow(l11,2))+(2*kl*(co*a6-a4)*(l11-p0)*(a5-a3))/(p0*pow(l11,3));
		dFTdX[8*i+2][8*i+4]=(2*kl*pow((a5-a3),2))/(p0*pow(l11,2))-(2*kl*(l11-p0)*pow((a5-a3),2))/(p0*pow(l11,3))+(2*kl*(l11-p0))/(p0*l11);
		dFTdX[8*i+2][8*i+5]= (kl*(2*co*(co*a6-a4)+2*pow(si,2)*a6)*(a5-a3))/(p0*pow(l11,2))-(kl*(2*co*(co*a6-a4)+2*pow(si,2)*a6)*(l11-p0)*(a5-a3))/(p0*pow(l11,3));

		dFTdX[8*i+3][8*i+0]=(km*(a4-a2)*(a3-a1))/(s0*(pow((a3-a1),2)+pow((a4-a2),2)))-(km*(a4-a2)*(l10-s0)*(a3-a1))/(s0*((pow(l10,3))));
		dFTdX[8*i+3][8*i+1]=(km*(l10-s0))/(s0*l10)+(km*pow((a4-a2),2))/(s0*(pow((a3-a1),2)+pow((a4-a2),2)))-(km*pow((a4-a2),2)*(l10-s0))/(s0*((pow(l10,3))));
		dFTdX[8*i+3][8*i+2]=-(km*(a4-a2)*(a3-a1))/(s0*(pow((a3-a1),2)+pow((a4-a2),2)))+(km*(a4-a2)*(l10-s0)*(a3-a1))/(s0*((pow(l10,3))))-(2*kl*(co*a6-a4)*(a5-a3))/(p0*pow(l11,2))+(2*kl*(co*a6-a4)*(l11-p0)*(a5-a3))/(p0*pow(l11,3));
		dFTdX[8*i+3][8*i+3]=-(km*(l10-s0))/(s0*l10)-(km*pow((a4-a2),2))/(s0*(pow((a3-a1),2)+pow((a4-a2),2)))+(km*pow((a4-a2),2)*(l10-s0))/(s0*((pow(l10,3))))-(2*kl*(l11-p0))/(p0*l11)-(2*kl*pow((co*a6-a4),2))/(p0*pow(l11,2))+(2*kl*pow((co*a6-a4),2)*(l11-p0))/(p0*pow(l11,3));
		dFTdX[8*i+3][8*i+4]=(2*kl*(co*a6-a4)*(a5-a3))/(p0*pow(l11,2))-(2*kl*(co*a6-a4)*(l11-p0)*(a5-a3))/(p0*pow(l11,3));
		dFTdX[8*i+3][8*i+5]=(2*kl*co*(l11-p0))/(p0*l11)+(kl*(2*co*(co*a6-a4)+2*pow(si,2)*a6)*(co*a6-a4))/(p0*pow(l11,2))-(kl*(2*co*(co*a6-a4)+2*pow(si,2)*a6)*(co*a6-a4)*(l11-p0))/(p0*pow(l11,3));

		}




		// ------ ----------------- la tension s'appliquant sur point i_2-------------------------------------------------------------



		for (i=0;i<nbl;i++)
		{
		b1=X[8*i+2];
		b2=X[8*i+3];
		b3=X[8*i+4];
		b4=X[8*i+5];
		b5=X[8*i+6];
		b6=X[8*i+7];

		l21=sqrt(pow((b3-b1),2)+pow((b4*si),2)+pow((b4*co-b2),2));
		if (l21<p0)
		l21=p0;
		FT2[8*i+4]=-kl*(l21-p0)/p0/l21*(b3-b1);
		FT2[8*i+5]=-kl*(l21-p0)/p0/l21*(b4-b2*co);

		l22=sqrt(pow((b5-b3),2)+pow((b6*si-b4*si),2)+pow((b6*co-b4*co),2));
		if (l22<q0)
		l22=q0;
		FT2[8*i+4]=FT2[8*i+4]+km*(l22-q0)/q0/l22*(b5-b3);
		FT2[8*i+5]=FT2[8*i+5]+km*(l22-q0)/q0/l22*(b6-b4);

		FT2[8*i+4]=FT2[8*i+4]+kl*(l21-p0)/p0/l21*(b1-b3);
		FT2[8*i+5]=FT2[8*i+5]+kl*(l21-p0)/p0/l21*(b2*cos(3*teta)-b4);


		dFTdX[8*i+4][8*i+2]=-(kl*(b3-b1)*(b1-b3))/(p0*pow(l21,2))+(kl*(l21-p0)*(b3-b1)*(b1-b3))/(p0*pow(l21,3))+(kl*pow((b3-b1),2))/(p0*pow(l21,2))-(kl*(l21-p0)*pow((b3-b1),2))/(p0*pow(l21,3))+(2*kl*(l21-p0))/(p0*l21);
		dFTdX[8*i+4][8*i+3]=-(kl*(co*b4-b2)*(b1-b3))/(p0*pow(l21,2))+(kl*(co*b4-b2)*(l21-p0)*(b1-b3))/(p0*pow(l21,3))+(kl*(co*b4-b2)*(b3-b1))/(p0*pow(l21,2))-(kl*(co*b4-b2)*(l21-p0)*(b3-b1))/(p0*pow(l21,3));
		dFTdX[8*i+4][8*i+4]=(kl*(b3-b1)*(b1-b3))/(p0*pow(l21,2))-(kl*(l21-p0)*(b3-b1)*(b1-b3))/(p0*pow(l21,3))-(kl*pow((b3-b1),2))/(p0*pow(l21,2))+(kl*(l21-p0)*pow((b3-b1),2))/(p0*pow(l21,3))-(2*kl*(l21-p0))/(p0*l21)-(km*pow((b5-b3),2))/(q0*pow(l22,2))+(km*(l22-q0)*pow((b5-b3),2))/(q0*pow(l22,3))-(km*(l22-q0))/(q0*l22);
		dFTdX[8*i+4][8*i+5]=(kl*(2*co*(co*b4-b2)+2*pow(si,2)*b4)*(b1-b3))/(2*p0*pow(l21,2))-(kl*(2*co*(co*b4-b2)+2*pow(si,2)*b4)*(l21-p0)*(b1-b3))/(2*p0*pow(l21,3))-(kl*(2*co*(co*b4-b2)+2*pow(si,2)*b4)*(b3-b1))/(2*p0*pow(l21,2))+(kl*(2*co*(co*b4-b2)+2*pow(si,2)*b4)*(l21-p0)*(b3-b1))/(2*p0*pow(l21,3))+(km*(-2*si*(si*b6-si*b4)-2*co*(co*b6-co*b4))*(b5-b3))/(2*q0*pow(l22,2))-(km*(-2*si*(si*b6-si*b4)-2*co*(co*b6-co*b4))*(l22-q0)*(b5-b3))/(2*q0*pow(l22,3));
		dFTdX[8*i+4][8*i+6]=(km*pow((b5-b3),2))/(q0*pow(l22,2))-(km*(l22-q0)*pow((b5-b3),2))/(q0*pow(l22,3))+(km*(l22-q0))/(q0*l22);
		dFTdX[8*i+4][8*i+7]=(km*(2*si*(si*b6-si*b4)+2*co*(co*b6-co*b4))*(b5-b3))/(2*q0*pow(l22,2))-(km*(2*si*(si*b6-si*b4)+2*co*(co*b6-co*b4))*(l22-q0)*(b5-b3))/(2*q0*pow(l22,3));
		dFTdX[8*i+5][8*i+2]=-(kl*(cos(3*teta)*b2-b4)*(b3-b1))/(p0*pow(l21,2))+(kl*(b4-co*b2)*(b3-b1))/(p0*pow(l21,2))+(kl*(cos(3*teta)*b2-b4)*(l21-p0)*(b3-b1))/(p0*pow(l21,3))-(kl*(b4-co*b2)*(l21-p0)*(b3-b1))/(p0*pow(l21,3));
		dFTdX[8*i+5][8*i+3]=(kl*cos(3*teta)*(l21-p0))/(p0*l21)+(kl*co*(l21-p0))/(p0*l21)-(kl*(co*b4-b2)*(cos(3*teta)*b2-b4))/(p0*pow(l21,2))+(kl*(co*b4-b2)*(b4-co*b2))/(p0*pow(l21,2))+(kl*(co*b4-b2)*(cos(3*teta)*b2-b4)*(l21-p0))/(p0*pow(l21,3))-(kl*(co*b4-b2)*(b4-co*b2)*(l21-p0))/(p0*pow(l21,3));
		dFTdX[8*i+5][8*i+4]=(kl*(cos(3*teta)*b2-b4)*(b3-b1))/(p0*pow(l21,2))-(kl*(b4-co*b2)*(b3-b1))/(p0*pow(l21,2))-(kl*(cos(3*teta)*b2-b4)*(l21-p0)*(b3-b1))/(p0*pow(l21,3))+(kl*(b4-co*b2)*(l21-p0)*(b3-b1))/(p0*pow(l21,3))-(km*(b6-b4)*(b5-b3))/(q0*pow(l22,2))+(km*(b6-b4)*(l22-q0)*(b5-b3))/(q0*pow(l22,3));
		dFTdX[8*i+5][8*i+5]=-(2*kl*(l21-p0))/(p0*l21)+(kl*(2*co*(co*b4-b2)+2*pow(si,2)*b4)*(cos(3*teta)*b2-b4))/(2*p0*pow(l21,2))-(kl*(2*co*(co*b4-b2)+2*pow(si,2)*b4)*(b4-co*b2))/(2*p0*pow(l21,2))-(kl*(2*co*(co*b4-b2)+2*pow(si,2)*b4)*(cos(3*teta)*b2-b4)*(l21-p0))/(2*p0*pow(l21,3))+(kl*(2*co*(co*b4-b2)+2*pow(si,2)*b4)*(b4-co*b2)*(l21-p0))/(2*p0*pow(l21,3))-(km*(l22-q0))/(q0*l22)+(km*(b6-b4)*(-2*si*(si*b6-si*b4)-2*co*(co*b6-co*b4)))/(2*q0*pow(l22,2))-(km*(b6-b4)*(-2*si*(si*b6-si*b4)-2*co*(co*b6-co*b4))*(l22-q0))/(2*q0*pow(l22,3));
		dFTdX[8*i+5][8*i+6]=(km*(b6-b4)*(b5-b3))/(q0*pow(l22,2))-(km*(b6-b4)*(l22-q0)*(b5-b3))/(q0*pow(l22,3));
		dFTdX[8*i+5][8*i+7]=(km*(l22-q0))/(q0*l22)+(km*(b6-b4)*(2*si*(si*b6-si*b4)+2*co*(co*b6-co*b4)))/(2*q0*pow(l22,2))-(km*(b6-b4)*(2*si*(si*b6-si*b4)+2*co*(co*b6-co*b4))*(l22-q0))/(2*q0*pow(l22,3));


		}






		// ------ ----------------- la tension s'appliquant sur point i_3-------------------------------------------------------------




		for (i=0;i<nbl;i++)
		{
		c1=X[8*i+4];
		c2=X[8*i+5];
		c3=X[8*i+6];
		c4=X[8*i+7];
		c5=X[8*i+8];
		c6=X[8*i+9];
		l33=sqrt(pow((c5-c3),2)+pow((-c4*si),2)+pow((c6-c4*co),2));
		if (l33<r0)
		l33=r0;

		//tension dans 2 fils derriere point i_3
		FT3[8*i+6]=kl*(l33-r0)/l33/r0*(c5-c3);
		FT3[8*i+7]=kl*(l33-r0)/l33/r0*(c6*co-c4);

		FT3[8*i+6]=FT3[8*i+6]+kl*(l33-r0)/l33/r0*(c5-c3);
		FT3[8*i+7]=FT3[8*i+7]+kl*(l33-r0)/l33/r0*(c6*co-c4);

		l22=sqrt(pow((c3-c1),2)+pow((c4*si-c2*si),2)+pow((c4*co-c2*co),2));
		if (l22<q0)
		l22=q0;

		//tension dans 2 fils devant point i_3
		FT3[8*i+6]=FT3[8*i+6]-km*(l22-q0)/q0/l22*(c3-c1);
		FT3[8*i+7]=FT3[8*i+7]-km*(l22-q0)/q0/l22*(c4-c2);

		dFTdX[8*i+6][8*i+4]=(km*(l22-q0))/(q0*l22)-(pow((c3-c1),2)*km*(l22-q0))/(q0*pow(l22,3))+(pow((c3-c1),2)*km)/(q0*pow(l22,2));
		dFTdX[8*i+6][8*i+5]=((c3-c1)*km*(-2*si*(c4*si-c2*si)-2*co*(c4*co-c2*co))*(l22-q0))/(2*q0*pow(l22,3))-((c3-c1)*km*(-2*si*(c4*si-c2*si)-2*co*(c4*co-c2*co)))/(2*q0*pow(l22,2));
		dFTdX[8*i+6][8*i+6]=-(km*(l22-q0))/(q0*l22)+(pow((c3-c1),2)*km*(l22-q0))/(q0*pow(l22,3))-(2*kl*(l33-r0))/(r0*l33)+(2*pow((c5-c3),2)*kl*(l33-r0))/(r0*pow(l33,3))-(pow((c3-c1),2)*km)/(q0*pow(l22,2))-(2*pow((c5-c3),2)*kl)/(r0*pow(l33,2));
		dFTdX[8*i+6][8*i+7]=((c3-c1)*km*(2*si*(c4*si-c2*si)+2*co*(c4*co-c2*co))*(l22-q0))/(2*q0*pow(l22,3))-((c5-c3)*kl*(2*c4*pow(si,2)-2*co*(c6-c4*co))*(l33-r0))/(r0*pow(l33,3))-((c3-c1)*km*(2*si*(c4*si-c2*si)+2*co*(c4*co-c2*co)))/(2*q0*pow(l22,2))+((c5-c3)*kl*(2*c4*pow(si,2)-2*co*(c6-c4*co)))/(r0*pow(l33,2));
		dFTdX[8*i+6][8*i+8]=(2*kl*(l33-r0))/(r0*l33)-(2*pow((c5-c3),2)*kl*(l33-r0))/(r0*pow(l33,3))+(2*pow((c5-c3),2)*kl)/(r0*pow(l33,2));
		dFTdX[8*i+6][8*i+9]=(2*(c5-c3)*kl*(c6-c4*co))/(r0*pow(l33,2))-(2*(c5-c3)*kl*(c6-c4*co)*(l33-r0))/(r0*pow(l33,3));

		dFTdX[8*i+7][8*i+4]=((c4-c2)*(c3-c1)*km)/(q0*pow(l22,2))-((c4-c2)*(c3-c1)*km*(l22-q0))/(q0*pow(l22,3));
		dFTdX[8*i+7][8*i+5]=(km*(l22-q0))/(q0*l22)+((c4-c2)*km*(-2*si*(c4*si-c2*si)-2*co*(c4*co-c2*co))*(l22-q0))/(2*q0*pow(l22,3))-((c4-c2)*km*(-2*si*(c4*si-c2*si)-2*co*(c4*co-c2*co)))/(2*q0*pow(l22,2));
		dFTdX[8*i+7][8*i+6]=((c4-c2)*(c3-c1)*km*(l22-q0))/(q0*pow(l22,3))+(2*(c5-c3)*kl*(c6*co-c4)*(l33-r0))/(r0*pow(l33,3))-((c4-c2)*(c3-c1)*km)/(q0*pow(l22,2))-(2*(c5-c3)*kl*(c6*co-c4))/(r0*pow(l33,2));
		dFTdX[8*i+7][8*i+7]=-(km*(l22-q0))/(q0*l22)+((c4-c2)*km*(2*si*(c4*si-c2*si)+2*co*(c4*co-c2*co))*(l22-q0))/(2*q0*pow(l22,3))-(2*kl*(l33-r0))/(r0*l33)-(kl*(c6*co-c4)*(2*c4*pow(si,2)-2*co*(c6-c4*co))*(l33-r0))/(r0*pow(l33,3))-((c4-c2)*km*(2*si*(c4*si-c2*si)+2*co*(c4*co-c2*co)))/(2*q0*pow(l22,2))+(kl*(c6*co-c4)*(2*c4*pow(si,2)-2*co*(c6-c4*co)))/(r0*pow(l33,2));
		dFTdX[8*i+7][8*i+8]=(2*(c5-c3)*kl*(c6*co-c4))/(r0*pow(l33,2))-(2*(c5-c3)*kl*(c6*co-c4)*(l33-r0))/(r0*pow(l33,3));
		dFTdX[8*i+7][8*i+9]=(2*kl*co*(l33-r0))/(r0*l33)-(2*kl*(c6-c4*co)*(c6*co-c4)*(l33-r0))/(r0*pow(l33,3))+(2*kl*(c6-c4*co)*(c6*co-c4))/(r0*pow(l33,2));


		}






		// ------ ----------------- la tension s'appliquant sur point i_4-------------------------------------------------------------



		for ( i=0;i<nbl-1;i++)

		{

		d1=X[8*i+6];
		d2=X[8*i+7];
		d3=X[8*i+8];
		d4=X[8*i+9];
		d5=X[8*i+10];
		d6=X[8*i+11];
		l44=sqrt(pow((d5-d3),2)+pow((d6-d4),2));
		if (l44<s0)
		l44=s0;

		// tension dans le fils derrier   point i_4
		FT4[8*i+8]=km*(l44-s0)/l44/s0*(d5-d3);
		FT4[8*i+9]=km*(l44-s0)/l44/s0*(d6-d4);

		l33=sqrt(pow((d3-d1),2)+pow((-d2*si),2)+pow((d4-d2*co),2));
		if (l33<r0)
		l33=r0;


		//tension dans le fils devant   point i_4
		FT4[8*i+8]=FT4[8*i+8]-kl*(l33-r0)/l33/r0*(d3-d1);
		FT4[8*i+9]=FT4[8*i+9]-kl*(l33-r0)/l33/r0*(d4-d2*co);

		FT4[8*i+8]=FT4[8*i+8]+kl*(l33-r0)/l33/r0*(d1-d3);
		FT4[8*i+9]=FT4[8*i+9]+kl*(l33-r0)/l33/r0*(d2*co-d4);

		dFTdX[8*i+8][8*i+6]=(2*kl*(l33-r0))/(r0*l33)-(pow((d3-d1),2)*kl*(l33-r0))/(r0*pow(l33,3))+((d1-d3)*(d3-d1)*kl*(l33-r0))/(r0*pow(l33,3))+(pow((d3-d1),2)*kl)/(r0*pow(l33,2))-((d1-d3)*(d3-d1)*kl)/(r0*pow(l33,2));
		dFTdX[8*i+8][8*i+7]=((d3-d1)*kl*(2*d2*pow(si,2)-2*co*(d4-d2*co))*(l33-r0))/(2*r0*pow(l33,3))-((d1-d3)*kl*(2*d2*pow(si,2)-2*co*(d4-d2*co))*(l33-r0))/(2*r0*pow(l33,3))-((d3-d1)*kl*(2*d2*pow(si,2)-2*co*(d4-d2*co)))/(2*r0*pow(l33,2))+((d1-d3)*kl*(2*d2*pow(si,2)-2*co*(d4-d2*co)))/(2*r0*pow(l33,2));
		dFTdX[8*i+8][8*i+8]=-(2*kl*(l33-r0))/(r0*l33)+(pow((d3-d1),2)*kl*(l33-r0))/(r0*pow(l33,3))-((d1-d3)*(d3-d1)*kl*(l33-r0))/(r0*pow(l33,3))-(pow((d3-d1),2)*kl)/(r0*pow(l33,2))+((d1-d3)*(d3-d1)*kl)/(r0*pow(l33,2))-(km*(l44-s0))/(l44*s0)+(pow((d5-d3),2)*km*(l44-s0))/(pow(l44,3)*s0)-(pow((d5-d3),2)*km)/(pow(l44,2)*s0);
		dFTdX[8*i+8][8*i+9]=((d3-d1)*kl*(d4-d2*co)*(l33-r0))/(r0*pow(l33,3))-((d1-d3)*kl*(d4-d2*co)*(l33-r0))/(r0*pow(l33,3))-((d3-d1)*kl*(d4-d2*co))/(r0*pow(l33,2))+((d1-d3)*kl*(d4-d2*co))/(r0*pow(l33,2))+((d5-d3)*(d6-d4)*km*(l44-s0))/(pow(l44,3)*s0)-((d5-d3)*(d6-d4)*km)/(pow(l44,2)*s0);
		dFTdX[8*i+8][8*i+10]=(km*(l44-s0))/(l44*s0)-(pow((d5-d3),2)*km*(l44-s0))/(pow(l44,3)*s0)+(pow((d5-d3),2)*km)/(pow(l44,2)*s0);
		dFTdX[8*i+8][8*i+11]=((d5-d3)*(d6-d4)*km)/(pow(l44,2)*s0)-((d5-d3)*(d6-d4)*km*(l44-s0))/(pow(l44,3)*s0);

		dFTdX[8*i+9][8*i+6]=-((d3-d1)*(d4-co*d2)*kl*(l33-r0))/(r0*pow(l33,3))+((d3-d1)*(co*d2-d4)*kl*(l33-r0))/(r0*pow(l33,3))+((d3-d1)*(d4-co*d2)*kl)/(r0*pow(l33,2))-((d3-d1)*(co*d2-d4)*kl)/(r0*pow(l33,2));
		dFTdX[8*i+9][8*i+7]=(2*co*kl*(l33-r0))/(r0*l33)+((d4-co*d2)*kl*(2*d2*pow(si,2)-2*co*(d4-co*d2))*(l33-r0))/(2*r0*pow(l33,3))-((co*d2-d4)*kl*(2*d2*pow(si,2)-2*co*(d4-co*d2))*(l33-r0))/(2*r0*pow(l33,3))-((d4-co*d2)*kl*(2*d2*pow(si,2)-2*co*(d4-co*d2)))/(2*r0*pow(l33,2))+((co*d2-d4)*kl*(2*d2*pow(si,2)-2*co*(d4-co*d2)))/(2*r0*pow(l33,2));
		dFTdX[8*i+9][8*i+8]=-((d3-d1)*kl*(d2*co-d4)*(l33-r0))/(r0*pow(l33,3))+((d3-d1)*kl*(d4-d2*co)*(l33-r0))/(r0*pow(l33,3))+((d3-d1)*kl*(d2*co-d4))/(r0*pow(l33,2))-((d3-d1)*kl*(d4-d2*co))/(r0*pow(l33,2))+((d5-d3)*(d6-d4)*km*(l44-s0))/(pow(l44,3)*s0)-((d5-d3)*(d6-d4)*km)/(pow(l44,2)*s0);
		dFTdX[8*i+9][8*i+9]=-(2*kl*(l33-r0))/(r0*l33)-(kl*(d4-d2*co)*(d2*co-d4)*(l33-r0))/(r0*pow(l33,3))+(kl*pow((d4-d2*co),2)*(l33-r0))/(r0*pow(l33,3))+(kl*(d4-d2*co)*(d2*co-d4))/(r0*pow(l33,2))-(kl*pow((d4-d2*co),2))/(r0*pow(l33,2))-(km*(l44-s0))/(l44*s0)+(pow((d6-d4),2)*km*(l44-s0))/(pow(l44,3)*s0)-(pow((d6-d4),2)*km)/(pow(l44,2)*s0);
		dFTdX[8*i+9][8*i+10]=((d5-d3)*(d6-d4)*km)/(pow(l44,2)*s0)-((d5-d3)*(d6-d4)*km*(l44-s0))/(pow(l44,3)*s0);
		dFTdX[8*i+9][8*i+11]=(km*(l44-s0))/(l44*s0)-(pow((d6-d4),2)*km*(l44-s0))/(pow(l44,3)*s0)+(pow((d6-d4),2)*km)/(pow(l44,2)*s0);


		}


		// point d�part!
		l44=sqrt(pow((X[2]-X[0]),2)+pow((X[3]-X[1]),2));
		if (l44<s0)
		l44=s0;

		FT4[0]=km*(l44-s0)/l44/s0*((X[2]-X[0]));
		FT4[1]=km*(l44-s0)/l44/s0*(X[3]-X[1]);

		dFTdX[0][0]=-(km*(l44-s0))/(l44*s0)+(pow((X[2]-X[0]),2)*km*(l44-s0))/(pow(l44,3)*s0)-(pow((X[2]-X[0]),2)*km)/(pow(l44,2)*s0);
		dFTdX[0][1]=((X[2]-X[0])*(X[3]-X[1])*km*(l44-s0))/(pow(l44,3)*s0)-((X[2]-X[0])*(X[3]-X[1])*km)/(pow(l44,2)*s0);
		dFTdX[0][2]=(km*(l44-s0))/(l44*s0)-(pow((X[2]-X[0]),2)*km*(l44-s0))/(pow(l44,3)*s0)+(pow((X[2]-X[0]),2)*km)/(pow(l44,2)*s0);
		dFTdX[0][3]=((X[2]-X[0])*(X[3]-X[1])*km)/(pow(l44,2)*s0)-((X[2]-X[0])*(X[3]-X[1])*km*(l44-s0))/(pow(l44,3)*s0);

		dFTdX[1][0]=((X[2]-X[0])*(X[3]-X[1])*km*(l44-s0))/(pow(l44,3)*s0)-((X[2]-X[0])*(X[3]-X[1])*km)/(pow(l44,2)*s0);
		dFTdX[1][1]=-(km*(l44-s0))/(l44*s0)+(pow((X[3]-X[1]),2)*km*(l44-s0))/(pow(l44,3)*s0)-(pow((X[3]-X[1]),2)*km)/(pow(l44,2)*s0);
		dFTdX[1][2]=((X[2]-X[0])*(X[3]-X[1])*km)/(pow(l44,2)*s0)-((X[2]-X[0])*(X[3]-X[1])*km*(l44-s0))/(pow(l44,3)*s0);
		dFTdX[1][3]=(km*(l44-s0))/(l44*s0)-(pow((X[3]-X[1]),2)*km*(l44-s0))/(pow(l44,3)*s0)+(pow((X[3]-X[1]),2)*km)/(pow(l44,2)*s0);

		// point arrier

		i=nbl-1;
		d1=X[8*i+6];
		d2=X[8*i+7];
		d3=X[8*i+8];
		d4=X[8*i+9];
		l33=sqrt(pow((d3-d1),2)+pow((-d2*si),2)+pow((d4-d2*co),2));
		if (l33<r0)
		l33=r0;

		FT4[8*i+8]=-kl*(l33-r0)/l33/r0*(d3-d1);
		FT4[8*i+9]=-kl*(l33-r0)/l33/r0*(d4-d2*co);

		FT4[8*i+8]=FT4[8*i+8]+kl*(l33-r0)/l33/r0*(d1-d3);
		FT4[8*i+9]=FT4[8*i+9]+kl*(l33-r0)/l33/r0*(d2*co-d4);

		dFTdX[8*i+8][8*i+6]=(2*kl*(l33-r0))/(r0*l33)-(pow((d3-d1),2)*kl*(l33-r0))/(r0*pow(l33,3))+((d1-d3)*(d3-d1)*kl*(l33-r0))/(r0*pow(l33,3))+(pow((d3-d1),2)*kl)/(r0*pow(l33,2))-((d1-d3)*(d3-d1)*kl)/(r0*pow(l33,2));
		dFTdX[8*i+8][8*i+7]=((d3-d1)*kl*(2*d2*pow(si,2)-2*co*(d4-d2*co))*(l33-r0))/(2*r0*pow(l33,3))-((d1-d3)*kl*(2*d2*pow(si,2)-2*co*(d4-d2*co))*(l33-r0))/(2*r0*pow(l33,3))-((d3-d1)*kl*(2*d2*pow(si,2)-2*co*(d4-d2*co)))/(2*r0*pow(l33,2))+((d1-d3)*kl*(2*d2*pow(si,2)-2*co*(d4-d2*co)))/(2*r0*pow(l33,2));
		dFTdX[8*i+8][8*i+8]=-(2*kl*(l33-r0))/(r0*l33)+(pow((d3-d1),2)*kl*(l33-r0))/(r0*pow(l33,3))-((d1-d3)*(d3-d1)*kl*(l33-r0))/(r0*pow(l33,3))-(pow((d3-d1),2)*kl)/(r0*pow(l33,2))+((d1-d3)*(d3-d1)*kl)/(r0*pow(l33,2));
		dFTdX[8*i+8][8*i+9]=((d3-d1)*kl*(d4-d2*co)*(l33-r0))/(r0*pow(l33,3))-((d1-d3)*kl*(d4-d2*co)*(l33-r0))/(r0*pow(l33,3))-((d3-d1)*kl*(d4-d2*co))/(r0*pow(l33,2))+((d1-d3)*kl*(d4-d2*co))/(r0*pow(l33,2));
		dFTdX[8*i+9][8*i+6]=((d3-d1)*kl*(d2*co-d4)*(l33-r0))/(r0*pow(l33,3))-((d3-d1)*kl*(d4-d2*co)*(l33-r0))/(r0*pow(l33,3))-((d3-d1)*kl*(d2*co-d4))/(r0*pow(l33,2))+((d3-d1)*kl*(d4-d2*co))/(r0*pow(l33,2));
		dFTdX[8*i+9][8*i+7]=(2*kl*co*(l33-r0))/(r0*l33)-(kl*(d2*co-d4)*(2*d2*pow(si,2)-2*co*(d4-d2*co))*(l33-r0))/(2*r0*pow(l33,3))+(kl*(d4-d2*co)*(2*d2*pow(si,2)-2*co*(d4-d2*co))*(l33-r0))/(2*r0*pow(l33,3))+(kl*(d2*co-d4)*(2*d2*pow(si,2)-2*co*(d4-d2*co)))/(2*r0*pow(l33,2))-(kl*(d4-d2*co)*(2*d2*pow(si,2)-2*co*(d4-d2*co)))/(2*r0*pow(l33,2));
		dFTdX[8*i+9][8*i+8]=-((d3-d1)*kl*(d2*co-d4)*(l33-r0))/(r0*pow(l33,3))+((d3-d1)*kl*(d4-d2*co)*(l33-r0))/(r0*pow(l33,3))+((d3-d1)*kl*(d2*co-d4))/(r0*pow(l33,2))-((d3-d1)*kl*(d4-d2*co))/(r0*pow(l33,2));
		dFTdX[8*i+9][8*i+9]=-(2*kl*(l33-r0))/(r0*l33)-(kl*(d4-d2*co)*(d2*co-d4)*(l33-r0))/(r0*pow(l33,3))+(kl*pow((d4-d2*co),2)*(l33-r0))/(r0*pow(l33,3))+(kl*(d4-d2*co)*(d2*co-d4))/(r0*pow(l33,2))-(kl*pow((d4-d2*co),2))/(r0*pow(l33,2));

		// ------ ---------------------------------------- Pression du poisson-------------------------------------------------------------


		//FP axiale
		// les points interm�diaires
		for (i=(npp+1);i<(taille/2);i++)
		{
		FP[2*i-2]=pi*(pow(X[2*i-3],2)-pow(X[2*i+1],2))*P/nbr/2;
		dFPdX[2*i-2][2*i-3]=(X[2*i-3]*pi*P)/nbr;
		dFPdX[2*i-2][2*i+1]=-(X[2*i+1]*pi*P)/nbr;
		}

		// point ou le poisson s'accumule   
		FP[2*npp-2]=pi*(pow(X[2*npp-1],2)-pow(X[2*npp+1],2))*P/nbr/2;
		dFPdX[2*npp-2][2*npp-1]= (X[2*npp-1]*pi*P)/nbr;
		dFPdX[2*npp-2][2*npp+1]=-(X[2*npp+1]*pi*P)/nbr;
		// point arriere
		FP[taille-2]=pi*(pow(X[taille-3],2)-pow(X[taille-1],2))*P/nbr/2;
		dFPdX[taille-2][taille-3]=(pi*P*X[taille-3])/nbr;
		dFPdX[taille-2][taille-1]=-(pi*P*X[taille-1])/nbr;


		//FP radiale

		// les points interm�diaires

		for (i=(npp+1);i<(taille/2);i++)
		{
		FP[2*i-1]=pi*(X[2*i-1]+X[2*i+1])*P/nbr*fabs(X[2*i]-X[2*i-2])/2+pi*(X[2*i-3]+X[2*i-1])*P/nbr*fabs(X[2*i-2]-X[2*i-4])/2;
		dFPdX[2*i-1][2*i-4]=-((X[2*i-3]+X[2*i-1])*(X[2*i-2]-X[2*i-4])*pi*P)/(2*fabs(X[2*i-2]-X[2*i-4])*nbr);
		dFPdX[2*i-1][2*i-3]=(fabs(X[2*i-2]-X[2*i-4])*pi*P)/(2*nbr);
		dFPdX[2*i-1][2*i-2]=((X[2*i-3]+X[2*i-1])*(X[2*i-2]-X[2*i-4])*pi*P)/(2*fabs(X[2*i-2]-X[2*i-4])*nbr)-((X[2*i]-X[2*i-2])*(X[2*i+1]+X[2*i-1])*pi*P)/(2*fabs(X[2*i]-X[2*i-2])*nbr);
		dFPdX[2*i-1][2*i-1]=(fabs(X[2*i]-X[2*i-2])*pi*P)/(2*nbr)+(fabs(X[2*i-2]-X[2*i-4])*pi*P)/(2*nbr);
		dFPdX[2*i-1][2*i-0]=((X[2*i]-X[2*i-2])*(X[2*i+1]+X[2*i-1])*pi*P)/(2*fabs(X[2*i]-X[2*i-2])*nbr);
		dFPdX[2*i-1][2*i+1]=(fabs(X[2*i]-X[2*i-2])*pi*P)/(2*nbr);
		}


		// point ou le poisson s'accumule  

		FP[2*npp-1]=pi*(X[2*npp-1]+X[2*npp+1])*P/nbr*fabs(X[2*npp]-X[2*npp-2])/2;
		dFPdX[2*npp-1][2*npp-2]=-((X[2*npp]-X[2*npp-2])*(X[2*npp+1]+X[2*npp-1])*pi*P)/(2*nbr*fabs(X[2*npp]-X[2*npp-2]));
		dFPdX[2*npp-1][2*npp-1]=(fabs(X[2*npp]-X[2*npp-2])*pi*P)/(2*nbr);
		dFPdX[2*npp-1][2*npp-0]=((X[2*npp]-X[2*npp-2])*(X[2*npp+1]+X[2*npp-1])*pi*P)/(2*nbr*fabs(X[2*npp]-X[2*npp-2]));
		dFPdX[2*npp-1][2*npp+1]=(fabs(X[2*npp]-X[2*npp-2])*pi*P)/(2*nbr);

		// point arriere
		FP[taille-1]=pi*(X[taille-3]+X[taille-1])*P/nbr*fabs(X[taille-2]-X[taille-4])/2;
		dFPdX[taille-1][taille-4]=-((X[taille-2]-X[taille-4])*(X[taille-1]+X[taille-3])*pi*P)/(2*fabs(X[taille-2]-X[taille-4])*nbr);
		dFPdX[taille-1][taille-3]=(fabs(X[taille-2]-X[taille-4])*pi*P)/(2*nbr);
		dFPdX[taille-1][taille-2]=((X[taille-2]-X[taille-4])*(X[taille-1]+X[taille-3])*pi*P)/(2*fabs(X[taille-2]-X[taille-4])*nbr);
		dFPdX[taille-1][taille-1]=(fabs(X[taille-2]-X[taille-4])*pi*P)/(2*nbr);




		// ------ ----------------------------------------       LE VECTEUR DE FORCE F  ------------------------------------------------------------


		for(i=0; i<taille;i++) 
			{
			F[i]=FT1[i]+FT2[i]+FT3[i]+FT4[i]+FP[i]; // somme
			//cout<<F[i]<<endl;
			}


		// ------ ----------------------------------------        MATRICE DE RAIDEUR ------------------------------------------------------------

		for ( i=0; i < taille; i++)
			{
			for ( j=0; j < taille; j++)
				{
				dFdX[i][j]=dFTdX[i][j]+dFPdX[i][j];

				}
			//cout<<endl;
			}

		// ------ ----------------------------------------        METHODE DE NEWTON RAPHSON ------------------------------------------------------------
		for ( i=2; i < taille-1; i++)

			for ( j=2; j < taille-1; j++)

				{a[i][j]=-dFdX[i][j];}

		// resolution d'un systeme d'equations % -dFdX.h=F  %

		for ( i=2; i < taille-1; i++) b[i]=F[i];

			for ( i=2; i < taille-1; i++)
				{
				for ( j=2; j < taille-1; j++)
					{
					A[i][j]=a[i][j];//cout<<A[i][j];
					}
				A[i][taille-1]=b[i];
				}

		for (i=2;i<taille-1;i++) A[i][i]=A[i][i]+ stiff_add;

		//% �chonl� 
		double w;
		int k;
		for (i=2;i<taille-1;i++)
		for (j=i+1;j<taille-1;j++)
			{ 
			w=A[j][i]/A[i][i] ;
			for ( k=i; k<taille;k++)
				A[j][k]= A[j][k]-w*A[i][k];
			}


		double s;
		for (i=taille-2;i>=2; i--)  
		{ 
		s=0;
		for (j=i+1; j<taille-1;j++)
		s=s+A[i][j]*h[j];
		h[i]=(A[i][taille-1]-s)/A[i][i];
		}

		// calcul des r�actions
		//double RX1,RR1,RRD;
		//       RX1=-F[0];// r�action axiale au point 1 
		//	RR1=-F[1];// r�action radiale au point 1
		//	RRD=-F[taille-1];// r�action radiale au denier point 

		//ajouter des r�actions au vecteur de force
		// F[0]=0;
		// F[1]=0;
		// F[taille-1]=0;
		// nouvelle position 
		for (i=2; i<taille-1; i++) X[i]=X[i]+h[i];

		ref=0;               

		for (i=2; i<taille-1; i++)
		ref=ref+pow(F[i],2);

		compteur_aff++;
		if(compteur_aff==1 || ref<=seuil) {	
		//cout<<ref<<endl;	
		cout << " kl:" << kl << " km:" << km << " v:" << v << " stiff_add:" << stiff_add << " seuil:" << seuil  << " residu:" << ref << endl;
		compteur_aff =0;
		}


		}

	cout<<"Position X R (m):" <<endl;
	//for (i=0; i<taille; i++)	cout<<X[i]<<endl;
	for (i=1; i<=taille/2; i++)	cout<<X[2*i-2]<<"  "<<X[2*i-1]<<endl;
	cout<<"X reaction at the entry (N): "<<nbr*F[0] <<endl;
	double drag;
	drag = pi*X[2*npp-1]*X[2*npp-1]*P;
	cout<<"Drag on catch (N): " <<drag<<endl;
	cout<<"Radius at catch limit (m): "<<X[2*npp-1] <<endl;
	cout<<"Thickness of the catch (m): "<<X[taille-2]-X[2*npp-2] <<endl;
	cout<<"Length of the codend (m): "<<X[taille-2] <<endl;
 // la surface de r�volution de la prise

    double surface=0;
    for (i=npp;i<= taille/2-1; i++) surface=surface+pi*(X[2*i-1]+X[2*i+1])*pow(pow(X[2*i]-X[2*i-2],2)+pow(X[2*i+1]-X[2*i-1],2),0.5) ; 
		// i: num�ro de point.
  
    cout<<"revolution surface (m^2):  "<<surface<<endl;

   // volume de la prise (m3)

    double Volume=0;
    for (j=npp;j<= taille/2-1; j++) 
		Volume=Volume+(pow(X[2*j+1],2)+X[2*j+1]*X[2*j-1]+pow(X[2*j-1],2))*pi*1/3*(X[2*j]-X[2*j-2]) ;
		// i: num�ro de point.
      
    cout<<"volume behind the front (m^3):  "<<Volume<<endl;
    

	
ofstream f ("REST0.txt");
if (!f.is_open())
	cout << "Impossible to open the file in writing" << endl;
else
	{
	f << "nbl:\t\t\t\t" << nbl << endl;
	f << "npr:\t\t\t\t" << npr << endl;
	f << "nbr:\t\t\t\t" << nbr << endl;
	f << "p0:\t\t\t\t" << p0 << endl;
	f << "q0:\t\t\t\t" << q0 << endl;
	f << "r0:\t\t\t\t" << r0 << endl;
	f << "s0:\t\t\t\t" << s0 << endl;
	f << "kl:\t\t\t\t" << kl << endl;
	f << "km:\t\t\t\t" << km << endl;
	f << "v:\t\t\t\t" << v << endl;
	f << "ro:\t\t\t\t" << ro << endl;
	f << "stiff_add:\t\t\t" << stiff_add << endl;
	f << "seuil:\t\t\t\t" << seuil << endl;
	f << " " <<endl;
	f<<"X reaction at the entry (N):\t"<<nbr*F[0] <<endl;
	f<<"Drag on catch (N): \t\t"<<drag <<endl;
	f<<"volume behind the front (m^3): \t"<<Volume <<endl;
	f<<"Radius at catch limit (m): \t"<<X[2*npp-1] <<endl;
	f<<"Thickness of the catch (m): \t"<<X[taille-2]-X[2*npp-2] <<endl;
	f<<"Length of the codend (m): \t"<<X[taille-2] <<endl;
	f << "Position x r (m):" <<endl;
	for (i=1; i<=taille/2; i++)	f<<X[2*i-2]<<"\t"<<X[2*i-1]<<endl;
	}
f.close();


	delete dFdX;
	delete dFTdX;
	delete dFPdX;
	delete F;
	delete FT1;
	delete FT2;
	delete FT3;
	delete FT4;
	delete FP;
	delete X;
	delete a;
	delete b;
	delete h;
	delete A;
	//getchar();
	return  0;




}
